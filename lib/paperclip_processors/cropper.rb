module Paperclip
  class Cropper < Thumbnail
    def transformation_command
      # crop command will only be non-nil if the current style that is being edited 
      # has crop dimensions set in the attachment instance's crop_rules hash
      if crop_command

        # by default by calling super, Paperclip will produce a IM args array of the form:
        # ["-resize", "\"x50\"", "-crop", "\"40x50+14+0\"", "+repage"]
        # The issue is that we need to enforce the condition that IM crops BEFORE it resizes the image
        #puts "richard.pry-er"

        #TODO: temp!!
        # TODO: need to figure out how to know which style we're currently editing
        #       then check if the target.style_to_crop == the current style
        #       and only then apply the custom crop_command
        #
        #
        #     @options[:geometry] looks promising => "40x50#"
        #
        #       this should prevent all of the styles getting cropped
        #if @options[:geometry] != @attachment.instance.crop_target_geometry
          #puts "@options[:geometry] #{@options[:geometry]} did not match #{@attachment.instance.crop_target_geometry}"
          #return super
        #else 
          #puts "@options[:geometry] #{@options[:geometry]} __did__ match #{@attachment.instance.crop_target_geometry}"
        #end



        puts "super: #{super}"
        # for now, we'll just build the im params array
        super_transform_with_new_crop = super
        #super_transform_with_new_crop = []
        #super_transform_with_new_crop << "-crop"
        #super_transform_with_new_crop << crop_command
        #super_transform_with_new_crop << "+repage"
        super.each_with_index do |c, i|
           if c =~ /-crop/
             super_transform_with_new_crop[i] = ''
             super_transform_with_new_crop[i+1] = ''
           end
        end
     
        #new_tc = super_transform_with_crop_removed << crop_command
        # add our custom crop command to the beginning of the IM args array
        super_transform_with_new_crop.unshift crop_command 
        super_transform_with_new_crop.unshift "-crop" 
        
        puts "new_tc #{super_transform_with_new_crop}"
        return super_transform_with_new_crop 
        #return super
        # crop_command + super.sub(/ -crop \S+/, '')
      else
        super
      end
    end
    
    def crop_command

      target = @attachment.instance
      crop_rules = target.crop_rules
      #style_to_crop = @attachment.instance.style_to_crop.try(:to_sym)

      # get the the type
      type = FileAttachment::TYPE.invert[target.attachment_type]
      styles = FileAttachment::STYLES[type] 
      if crop_rules.blank? or styles.blank?
        puts "no style found for type #{type}"
        return nil
      end

      # TODO: this should refactored to be
      # style_to_crop = FileAttachment.get_style_by_type_and_geometry(target.attachment_type, @options[:geometry])
      
      style_to_crop = styles.invert[@options[:geometry]]
      #binding.pry

      # if there are crop rules for the given style, then use them
      if style_to_crop.present? and crop_rules[style_to_crop].present?
        rules = crop_rules[style_to_crop]
        "\"#{rules[:crop_w]}x#{rules[:crop_h]}+#{rules[:crop_x]}+#{rules[:crop_y]}\""
      end
    end
  end
end
