module Mercury
  module Authentication
    def can_edit?
      user_signed_in? and current_user.is_admin?
    end
  end
end
