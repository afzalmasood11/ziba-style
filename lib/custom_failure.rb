# Override Devise FailureApp so that we can redirect to the homepage with sign-in modal, rather than the actual sign-in page 
# -- could capture and do something with the 'attempted_path', if we want. 

class CustomFailure < Devise::FailureApp
  def redirect_url
    # send(:"new_#{scope}_session_path", :format => (request.xhr? ? 'js' : nil ))
    # puts attempted_path
    flash[:alert] = nil 
    '/?need_log_in=true'
  end
  
end