ENTITY_TYPE = { '1' => 'User' , '2' => 'Product', '3' => 'Country' }

task :update_attachments => :environment do
  FileAttachment.all.each do |fa|    
    fa.update_column(:entity_type, ENTITY_TYPE[fa.entity_type.to_s]) if ENTITY_TYPE.key? fa.entity_type.to_s     
  end
end

task :update_country_flags => :environment do

  files = Dir.glob('vendor/country_flags/*')
  for file in files
    unless file.include? "_1."

      country_name = file.split("/")
      country_name = country_name.last
      country_name = country_name.split(".")
      country_name = country_name.first

      countries = Country.where{name =~ my{country_name}}

      unless countries.empty?
        country = countries.first
        if country.flag.blank? or country.flag.to_s.include? "missing.png"
          country.flag = File.open(file, "r")
          country.save
        end
      else
        puts file 
      end
    end
  end

  puts "---------------------"
  puts "DONE IMPORTING FLAGS"
  puts "---------------------"

  Country.all.each do |country|
    if country.flag.blank? or country.flag.to_s.include? "missing.png"
      puts country.name
    end
  end

  puts "Done dealing with Country flags"

end