namespace :height do
  desc "Adding Height to entities."
  task :entities => :environment do

    HomePageEntity.all.each do |obj|
      if(!obj.entity_type)
        obj.height = obj.get_height
      elsif(obj.entity_type == 'Category')
        geo = Paperclip::Geometry.from_file(obj.entity.homepage_image.to_file(:original))
        obj.height = geo.height
      else
        geo = Paperclip::Geometry.from_file(obj.entity.homepage_image.attachment.to_file(:original))
        obj.height = geo.height
      end
      obj.save
    end
    
  end #end of task

end #end of name space