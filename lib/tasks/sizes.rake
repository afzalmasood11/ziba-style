namespace :subcategories do
  desc "Adding subcategory."
  task :sizes => :environment do

    #creating category
    topLevels = %w{Apparel}

    @apparel_both = %w{}
    @apparel_men = %w{}
    @apparel_women = %w{Sarees Capes}

    puts "creating categories"
        
    topLevels.each do |cat|
      parent = Category.find_by_name(cat)

      both = {both: instance_variable_get("@#{cat.gsub("'", "_").gsub(" ", "_").downcase}_both")}
      mens = {men: instance_variable_get("@#{cat.gsub("'", "_").gsub(" ", "_").downcase}_men")}
      womens = {women: instance_variable_get("@#{cat.gsub("'", "_").gsub(" ", "_").downcase}_women")}

      both.each do |g, subcats|
        unless subcats.nil?
          subcats.each do |name|
            cat = Category.find_or_create_by_name_and_category_id({name: name, category_id: parent.id})
            subcategory_size(cat.id, 3) unless Size.where(category_id: cat.id, gender_type: 3).first
          end
        end #unless end
      end

      mens.each do |g, subcats|
        unless subcats.nil?
          subcats.each do |name|
            cat = Category.find_or_create_by_name_and_category_id({name: name, category_id: parent.id})
            subcategory_size(cat.id, 2) unless Size.where(category_id: cat.id, gender_type: 2).first
          end
        end #unless end
      end

      womens.each do |g, subcats|
        unless subcats.nil?
          subcats.each do |name|
            cat = Category.find_or_create_by_name_and_category_id({name: name, category_id: parent.id})
            subcategory_size(cat.id, 1) unless Size.where(category_id: cat.id, gender_type: 1).first
          end
        end #unless end
      end

    end

    puts "End of task."
  end #end of task

  def subcategory_size(cat_id, gen)
    puts "creating size of subcategory."
    one_size_fits_all_text = "One size"
    others = { 'EU' => one_size_fits_all_text, 'IT' => one_size_fits_all_text, 'UK/US Waist' => one_size_fits_all_text }
    Size.create us_value: one_size_fits_all_text, other_values: others, category_id: cat_id, gender_type: gen
    4.times do |i|
      i *= 2
      us_value = (i == 0) ? '00' : i
      if us_value.is_a? String and us_value=='00'
        wm_alpha = 'XS'
      elsif us_value > 4
        wm_alpha = 'L'
      elsif us_value > 2
        wm_alpha = 'M'
      elsif us_value > 0
        wm_alpha = 'S'
      end

      others = {
#        'Alpha Size' => wm_alpha,
#        'EU' => i+44,
#        'IT' => i+44,
#        'UK/US Waist' => i+28
      }
      Size.create us_value: wm_alpha, other_values: others, category_id: cat_id, gender_type: gen
    end
  end

end #end of name space