namespace :seed do
	
	task :categories => :environment do
		Category.seed
		puts 'done seeding categories!'
	end

	task :sizes => :environment do
		Size.seed
		puts 'done seeding sizes!'
	end

	task :countries => :environment do
		Country.seed
		puts 'done seeding countries!'
	end

	task :all => [:categories, :sizes, :countries, :environment] do 
		# puts 'done!!!'
	end

end