namespace :blog do
  task "refresh" => :environment do
    puts ::BLOG_FEED_URL
    feed = Feedzirra::Feed.fetch_and_parse(::BLOG_FEED_URL)
    updated_feed = Feedzirra::Feed.update(feed)
    expire_fragment "blog_page" if updated_feed.updated?
  end
end
