namespace :slugify do
  desc "Adding slug for existing country records."
  task :countries => :environment do
    Country.find_each(&:save)
  end

  desc "Adding slug for existing designer records."
  task :designer => :environment do
    User.find_each(&:save)
  end

  desc "Adding slug for existing category records."
  task :category => :environment do
    Category.find_each(&:save)
  end

  desc "Adding slug for existing product records."
  task :product => :environment do
    Product.find_each(&:save)
#    Product.all.each do |product|
#      product.send :set_slug
#      product.save(:validate => false)
#    end
  end

  desc "Adding slug for all required records."
  task :all => [:countries, :designer, :category]
end