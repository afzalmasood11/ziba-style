require 'bundler/capistrano'

set :user, 'ubuntu'
set :domain, 'ec2-23-22-208-199.compute-1.amazonaws.com'

set :staging_dir, "/home/ubuntu/staging"
set :staging_application, "ZIBA Style Staging"
set :production_dir, "/home/ubuntu/www"
set :production_application, "ZIBA Style Production"

set :scm, 'git'
set :repository, "git@github.com:yasminsaniehay/ZIBA-Style.git"
set :git_enable_submodules, 1 # if you have vendored rails
set :branch, 'master'
set :git_shallow_clone, 1
set :scm_verbose, true
set :use_sudo, false

# roles (servers)
role :web, domain
role :app, domain
role :db,  domain, :primary => true
role :db,  domain

# deploy config
set :deploy_via, :export

# additional settings
default_run_options[:pty] = true      # Forgo errors when deploying from windows
ssh_options[:username] = 'ubuntu'
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "ZIBA2.pem")]
ssh_options[:forward_agent] = true

# defining environment tasks
task :staging do
  set :applicationdir, staging_dir 
  set :application, staging_application
  set :deploy_to, staging_dir
  set :env, "staging"
end

task :production do
  set :applicationdir, production_dir 
  set :application, production_application
  set :deploy_to, production_dir
  set :env, "production"
end

# defining name
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

# Build the SASS Stylesheets
before "deploy:update_code" do
end

before "deploy:restart" do
  run "cd #{current_path} && rake db:migrate RAILS_ENV=#{env}"
  run "cd #{current_path} && rake assets:precompile RAILS_ENV=#{env}"
end
