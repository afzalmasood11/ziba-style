ZIBAStyle::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # Code is not reloaded between requests
  config.cache_classes = true

  config.action_mailer.default_url_options = { :host => 'www.ziba-style.com' }

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_assets = false

  #config.static_cache_control = "public, max-age=2592000"
  config.static_cache_control = "public, max-age=20"

  # Compress JavaScripts and CSS
  config.assets.compress = true

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = true

  # Generate digests for assets URLs
  config.assets.digest = true

  Slim::Engine.set_default_options :pretty => true

  # s3 bucket
  config.s3_bucket = 'zibastyle'
  config.s3_credentials = {
    :access_key_id => "AKIAIU3YSAQDUM7LAJWA",
    :secret_access_key => "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" 
  }

  # Gateway in test mode by default ... set to production in environments/production.rb
  config.after_initialize do
    ActiveMerchant::Billing::Base.gateway_mode = :production
    ActiveMerchant::Billing::Base.integration_mode = :production
    ::BTGATEWAY = ActiveMerchant::Billing::BraintreeGateway.new(
      :merchant_id => "cgr5kbf6wv4875dk",
      :public_key => "gp2czyxwy9bkv66z",
      :private_key => "nxj7pvfqck7sydvq"
    )
    ::PPGATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(
      :login => "ysh_api1.ziba-style.com",
      :password => "UKYW6RMN5JZHB45C",
      :signature => "AWUhtF.qs03R6aV3hUr7Id6Xi7bCA7mNASfqphc4dnuBp3xqJZaQT8Cb"
    )

  end

  ::USA_COUNTRY_ID = 2281
  ::OBJECT_D_ART_CATEGORY_ID = 581
  ::GOOGLE_ANALYTICS_ACCOUNT = "UA-29556856-1"
  ::BLOG_FEED_URL = "http://zibastyle.blogspot.com/feeds/posts/default?alt=rss"

  # to redirect user to www.ziba-style.com if they're coming from http://ziba-style.com (SSL thing)
  config.middleware.use "WwwMiddleware"

  #config.action_dispatch.rack_cache = {
    #:metastore    => Dalli::Client.new,
    #:entitystore  => 'file:tmp/cache/rack/body',
    #:allow_reload => false
  #}

  # Defaults to Rails.root.join("public/assets")
  # config.assets.manifest = YOUR_PATH

  # Specifies the header that your server uses for sending files
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # See everything in the log (default is :info)
   config.log_level = :debug

  # Prepend all log lines with the following tags
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production
  #config.cache_store = :dalli_store, { :expires_in => 2.hour }

  # Enable serving of images, stylesheets, and JavaScripts from an asset server
  # config.action_controller.asset_host = "http://assets.example.com"

  # Precompile additional assets (application.js, application.css, and all non-JS/CSS are already added)
  # config.assets.precompile += %w( search.js )

  # Disable delivery errors, bad email addresses will be ignored
  # config.action_mailer.raise_delivery_errors = false

  # Enable threaded mode
  # config.threadsafe!

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  # config.active_record.auto_explain_threshold_in_seconds = 0.5

  # for seo meta tags
  SEO_CONFIG = YAML.load_file("#{Rails.root}/config/seo_config.yml")
end
