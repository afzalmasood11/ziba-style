ZIBAStyle::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # don't serve precompiled assets in development
  config.serve_static_assets = true 

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.default_url_options = { :host => 'localhost:3000' }

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  config.active_record.auto_explain_threshold_in_seconds = 0.5

  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true

  # Output pretty (indented) format
  Slim::Engine.set_default_options :pretty => true

  # s3 bucket
  config.s3_bucket = 'ziba'
  config.s3_credentials = {
    :access_key_id => "AKIAJOBEYMG7WR37ZPHQ",
    :secret_access_key => "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"
  }

#  config.s3_bucket = 'ziba-2'
#  config.s3_credentials = {
#    :access_key_id => "AKIAJOBEYMG7WR37ZPHQ",
#    :secret_access_key => "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"
#  }

 # Gateway in test mode by default ... set to production in environments/production.rb
  config.after_initialize do
    ActiveMerchant::Billing::Base.gateway_mode = :test
    ActiveMerchant::Billing::Base.integration_mode = :test
    ::BTGATEWAY = ActiveMerchant::Billing::BraintreeGateway.new(
      :merchant_id => "cgr5kbf6wv4875dk",
      :public_key => "gp2czyxwy9bkv66z",
      :private_key => "nxj7pvfqck7sydvq"
    )
    ::PPGATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(
      :login => "jon_1331145098_biz_api1.exygy.com",
      :password => "1331145122",
      :signature => "AFcWxV21C7fd0v3bYYYRCpSSRl31A2if32KUmj7htY350.xKLRITVqJ4 "
    )

  end

  ::USA_COUNTRY_ID = 476
  ::OBJECT_D_ART_CATEGORY_ID = 141
  ::GOOGLE_ANALYTICS_ACCOUNT = ""
  ::BLOG_FEED_URL = "http://zibastyle.blogspot.com/feeds/posts/default?alt=rss"
  
  SEO_CONFIG = YAML.load_file("#{Rails.root}/config/seo_config.yml")
  
  #for paperclip delay job
#  config.gem 'delayed_paperclip'

end
