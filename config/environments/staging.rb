ZIBAStyle::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # Code is not reloaded between requests
  config.cache_classes = true
  # config.cache_classes = false

  config.action_mailer.default_url_options = { :host => 'ziba-exygy.herokuapp.com' }

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local       = false

  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_assets = true

  config.static_cache_control = "public, max-age=2592000"

  # Compress JavaScripts and CSS
  config.assets.compress = true

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = true

  # Generate digests for assets URLs
  config.assets.digest = true
  
############## configs added for assets_sync ################
## Disable Rails's static asset server (Apache or nginx will already do this)
#  config.serve_static_assets = true
#
##  config.assets.version = "1.1"
##  config.static_cache_control = "public, max-age=31536000"
#
#  # Generate digests for assets URLs
#  config.assets.digest = true
#
  config.assets.js_compressor  = :uglifier
  config.assets.css_compressor = :scss
  config.gzip_compression = true
#  Needs to be false on Heroku
  config.assets.initialize_on_precompile = false
#  # Precompile additional assets (application.js, application.css, and all non-JS/CSS are already added)
#  config.assets.precompile += %w( active_admin.js active_admin.css )
  config.assets.debug = false
#
#  config.action_controller.asset_host = "//s3.amazonaws.com/#{ENV['FOG_DIRECTORY']}"
#  only for development environment
#  config.action_controller.asset_host = "//s3.amazonaws.com/ziba-2#{ENV['FOG_DIRECTORY']}"

  ###################### end ############

  Slim::Engine.set_default_options :pretty => true

  # s3 bucket
  config.s3_bucket = 'ziba-2'
  config.s3_credentials = {
    :access_key_id => "AKIAJOBEYMG7WR37ZPHQ",
    :secret_access_key => "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"
  }

  # Gateway in test mode by default ... set to production in environments/production.rb
  config.after_initialize do
    ActiveMerchant::Billing::Base.gateway_mode = :test
    ActiveMerchant::Billing::Base.integration_mode = :test
    ::BTGATEWAY = ActiveMerchant::Billing::BraintreeGateway.new(
      :merchant_id => "cgr5kbf6wv4875dk",
      :public_key => "gp2czyxwy9bkv66z",
      :private_key => "nxj7pvfqck7sydvq"
    )
    ::PPGATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(
      :login => "jon_1331145098_biz_api1.exygy.com",
      :password => "1331145122",
      :signature => "AFcWxV21C7fd0v3bYYYRCpSSRl31A2if32KUmj7htY350.xKLRITVqJ4 "
    )

  end

  ::USA_COUNTRY_ID = 2282
  ::OBJECT_D_ART_CATEGORY_ID = 581
  ::GOOGLE_ANALYTICS_ACCOUNT = "UA-29556856-2"
  ::BLOG_FEED_URL = "http://zibastyle.blogspot.com/feeds/posts/default?alt=rss"


  # config.action_dispatch.rack_cache = {
  #   :metastore    => Dalli::Client.new,
  #   :entitystore  => 'file:tmp/cache/rack/body',
  #   :allow_reload => false
  # }

  # Defaults to Rails.root.join("public/assets")
  # config.assets.manifest = YOUR_PATH

  # Specifies the header that your server uses for sending files
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # See everything in the log (default is :info)
  # config.log_level = :debug

  # Prepend all log lines with the following tags
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production
#  config.cache_store = :dalli_store, { :expires_in => 2.hour }


  # Enable serving of images, stylesheets, and JavaScripts from an asset server
  # config.action_controller.asset_host = "http://assets.example.com"

  # Precompile additional assets (application.js, application.css, and all non-JS/CSS are already added)
  # config.assets.precompile += %w( search.js )

  # Disable delivery errors, bad email addresses will be ignored
  # config.action_mailer.raise_delivery_errors = false

  # Enable threaded mode
  # config.threadsafe!

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  # config.active_record.auto_explain_threshold_in_seconds = 0.5

  # for seo meta tags
  SEO_CONFIG = YAML.load_file("#{Rails.root}/config/seo_config.yml")
end
