ZIBAStyle::Application.routes.draw do

  #get "errors/error_404"

  mount Mercury::Engine => '/'
  # Mercury::Engine.routes


  resources :home_page_entities
  resources :home_page, only: :index do 
    collection {
      get :admin
      post :admin
    }
  end


  WepayRails.routes(self)
  
  resources :categories
  match 'categories/:id/:category_id' => 'categories#show'
  
  resources :contacts, only: [:index, :create, :show] do
    collection { get :contact_us, :request_invitation }
  end
  resources :credits
  resources :designers, only: [:index, :show, :edit, :update ] do
    collection { get :activate, :boutique, :sales_report, :force_apply, :videos, :settings, :upload_designers_page_banner
                 put  :deactivate_boutique_request, :upload_designers_page_banner
                 post :sales_report }
    member { get :detailed_view } 
  end

  match '/designers/:id/upload_banner' => 'designers#upload_banner', as: 'upload_banner'

  resources :designer_orders, only: [:index, :show, :edit, :update ]
  resources :designer_videos
  #resources :countries do
  resources :countries do
    member { post :mercury_update
             get :history }
  end

  resources :editor_picks
  #match '/editor_picks' => 'editor_picks#index'
  #match '/editor_picks_first' => 'editor_picks#show', via: :get
  
  match '/shopping_bag'     => 'cart#index',  via: :get,  as: 'shopping_bag'
  match '/shopping_bag'     => 'cart#create', via: :post, as: 'shopping_bag'
  match '/shopping_bag/:id' => 'cart#update', via: :put,  as: 'shopping_bag'
  
  resources :cart, only: [:index, :create, :update ]
  
  resources :feedbacks, only: [:index, :new, :create ]
  resources :file_attachments, only: [:update, :show]

  # route to crop a particular style of a file attachment
  match '/file_attachments/:id/crop_studio/:style', to: 'file_attachments#crop_studio', as: :crop_style


  resources :invitations, only: [:index, :create ]
  resources :orders, except: [:create, :new, :destroy ]
  resources :pages do
    member { post :mercury_update }
  end

  match '/export_products' => 'products#export_products'
  match '/export_users' => 'users#export_users'
  resources :products do
    member { get :share, :share_by_email, :crop_studio, :size_chart }
  end
  resources :returns, except: :delete do 
    member { put :restock }
  end
  
  resources :sales, only: [:index, :edit, :update]

  match '/browsing_gender/:gender' => 'users#browsing_gender', as: 'browsing_gender', via: :put

  #routes for search
  resources :search, only: [:index, :create]

  #routes for products
  match 'products/:id/sizes'    => 'products#sizes', as: :products_sizes
  match 'products/:id/shipping' => 'products#edit', action_type: "shipping", as: :products_shipping

  #routes for customers
  match '/checkout'           => 'orders#checkout',           via: [:get,:post]
  match '/checkout'           => 'orders#update',             via: [:put]
  match '/validate_checkout'  => 'orders#validate_checkout'
  match '/cancel_checkout'    => 'orders#cancel_checkout',    via: :post
  match '/order_complete'     => 'orders#order_complete',     via: :get
  match '/review_orders'      => 'feedbacks#customer_index',  via: :get

  #routes for designers
  match '/designers_feedbacks' => 'feedbacks#index', user_type: "designer", via: :get
  match '/designer_returns'    => 'returns#designer_returns', via: :get
  match '/style_directory'     => 'designers#style_directory', via: :get
  match '/become_designer'     => 'designers#become_designer', via: :get, as: :become_designer
  match '/become_designer'     => 'designers#switch_to_designer', via: :post, as: :become_designer
  match '/designer_accepted/:authentication_token' => 'designers#accepted', via: :get, as: :designer_accepted

  #routes for static pages
  match '/about'                => 'pages#show',                id: "about",                via: :get
  match '/style_directory'      => 'pages#show',                id: "style",                via: :get
  match '/privacy'              => 'pages#show',                id: "privacy",              via: :get
  match '/terms_of_service'     => 'pages#show',                id: "terms_of_service",     via: :get
  match '/membership'           => 'pages#show',                id: "membership",           via: :get
  match '/faq'                  => 'pages#show',                id: "faq",                  via: :get
  match '/shipping_and_returns' => 'pages#show',                id: "shipping_and_returns", via: :get
  match '/blog'                 => 'pages#blog',                id: "blog",                 via: :get
 
  # routes for mercury editor updating page content
  match '/about'                => 'pages#update',                id: "about",                via: :put
  match '/style_directory'      => 'pages#update',                id: "style",                via: :put
  match '/privacy'              => 'pages#update',                id: "privacy",              via: :put
  match '/terms_of_service'     => 'pages#update',                id: "terms_of_service",     via: :put
  match '/membership'           => 'pages#update',                id: "membership",           via: :put
  match '/faq'                  => 'pages#update',                id: "faq",                  via: :put
  match '/shipping_and_returns' => 'pages#update',                id: "shipping_and_returns", via: :put
  
  match '/contact_us'       => 'contacts#contact_us',                                 via: :get
  match '/engage_with_us'   => 'contacts#engage_with_us',                             via: :get
 
 
  #routes for admin
  match 'admin/approve_designers'   => 'admin#approve', action_name: "approve_designers",   via: [:get, :post]
  match 'admin/authorize_designers' => 'admin#approve', action_name: "authorize_designers", via: [:get, :post]
  match 'admin/approve_products'    => 'admin#approve', action_name: "approve_products",    via: [:get, :post]
  match 'admin/approve_videos'      => 'admin#approve', action_name: "approve_videos",      via: [:get, :post]
  match 'admin/orders'              => 'orders#index',  user_type: "admin",                 via: :get
  match 'admin/returns'             => 'returns#index', user_type: "admin",                 via: :get
  match 'admin/returns/:id/edit'    => 'returns#edit',  user_type: "admin",                 via: :get, as: :admin_edit_return
  match 'admin/products'            => 'products#index',user_type: "admin",                 via: :get
  match 'admin/countries'           => 'countries#admin_index',                             via: :get, as: :admin_countries
  match 'admin/switch_to_designer'  => 'admin#switch_to_designer',                          via: :post
  match 'admin/switch_to_admin'     => 'admin#switch_to_admin',                             via: :post



  #devise stuff  
  devise_for :users, controllers: { sessions: "users/sessions", registrations: "users/registrations", confirmations: "users/confirmations", passwords: "users/passwords"  } 
  devise_scope :user do
    # root to: 'countries#index', constraints: lambda {|r| r.env["warden"].authenticate? }
    # root to: 'users/sessions#new'
    root to: 'home_page#index'
    match 'designer/sign_up'          => 'users/registrations#new',    role: User::ROLE[ :designer ], via: :get
    match 'designer/sign_up'          => 'users/registrations#create', role: User::ROLE[ :designer ], via: :post
    match 'customer/sign_up'          => 'users/registrations#new',    role: User::ROLE[ :customer ], via: :get
    match 'customer/sign_up'          => 'users/registrations#create', role: User::ROLE[ :customer ], via: :post
    match '/invite/:invitation_token' => 'users/registrations#new',    role: User::ROLE[ :customer ], as: "customer_sign_up_with_invitation"
    match '/account'                  => 'users/registrations#edit',   view: "account", via: :get
    match '/shipping_info'            => 'users/registrations#edit',   view: "shipping", via: :get
    match '/designer_accepted_registration/:authentication_token' => 'users/registrations#edit', via: :get, as: :designer_accepted_registration

  end
 
  resources :users, only: [:index, :edit, :update]

  match '*not_found', to: 'errors#error_404'
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'users/sessions#new'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
