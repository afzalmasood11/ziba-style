# adds support for the :att_type interpolation which looks at the FileAttachment's attachment_type
# so that this can be added to generated path
# this creates unique URIs for celebrity_picture 1 :banner, and picture 1 :banner

module Paperclip
  module Interpolations
    def att_type attachment, style_name
      attachment.instance.attachment_type.to_s
    end
  end
end
