class CreateDesignerOrders < ActiveRecord::Migration
  def change
    create_table :designer_orders do |t|
      t.integer :designer_id
      t.integer :order_id
      t.string :ups_tracking
      t.integer :shipping_status

      t.timestamps
    end
    add_index :designer_orders, [:designer_id, :order_id], :unique => true
  end
end
