class AddWeightTypeToProducts < ActiveRecord::Migration
  def change
    add_column :products, :weight_type, :string, :after => :weight
  end
end
