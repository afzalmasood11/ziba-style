class CreateSizes < ActiveRecord::Migration
  def change
    create_table :sizes do |t|
      t.string :us_value
      t.text :other_values
      t.integer :category_id
      t.integer :gender_type 

      t.timestamps
    end
    add_index :sizes, :category_id
    add_index :sizes, :gender_type
  end
end
