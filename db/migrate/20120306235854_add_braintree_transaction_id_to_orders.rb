class AddBraintreeTransactionIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :braintree_transaction_id, :string

  end
end
