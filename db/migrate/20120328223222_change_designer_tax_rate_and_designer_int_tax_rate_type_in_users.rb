class ChangeDesignerTaxRateAndDesignerIntTaxRateTypeInUsers < ActiveRecord::Migration
  def up
    change_column :users, :designer_tax_rate,     :decimal, precision: 5, scale: 3, :default => 0
    change_column :users, :designer_int_tax_rate, :decimal, precision: 5, scale: 3, :default => 0
  end

  def down
    change_column :users, :designer_tax_rate,     :decimal
    change_column :users, :designer_int_tax_rate, :decimal
  end
end
