class CreateHomePageEntities < ActiveRecord::Migration
  def change
    create_table :home_page_entities do |t|
      t.integer :entity_id
      t.string  :entity_type
      t.integer :order

      t.timestamps
    end
  end
end
