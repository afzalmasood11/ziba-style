class AddSaleAndOriginalPriceToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :sale, :boolean
    add_column :order_items, :original_price, :decimal, :precision => 9, :scale => 2
	  add_index :order_items, :sale
  end
end
