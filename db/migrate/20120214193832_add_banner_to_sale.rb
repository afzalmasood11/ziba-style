class AddBannerToSale < ActiveRecord::Migration
  def self.up
    change_table :sales do |t|
      t.has_attached_file :banner
    end
  end

  def self.down
    drop_attached_file :sales, :banner
  end
end
