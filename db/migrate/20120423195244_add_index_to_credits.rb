class AddIndexToCredits < ActiveRecord::Migration
  def change
    add_index :credits, :code, :unique => true
  end
end
