class ChangeFileAttachmentEntityType < ActiveRecord::Migration
  def up
    change_column :file_attachments, :entity_type, :string
  end

  def down
    change_column :file_attachments, :entity_type, :integer
  end
end
