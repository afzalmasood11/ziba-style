class RenameBraintreeTransactionIdTotransactionIdForOrders < ActiveRecord::Migration
  def up
    rename_column :orders, :braintree_transaction_id, :transaction_id
    rename_column :orders, :braintree_vault_id, :transaction_cc
  end

  def down
    rename_column :orders, :transaction_id, :braintree_transaction_id
    rename_column :orders, :transaction_cc, :braintree_vault_id
  end
end
