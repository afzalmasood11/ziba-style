class AddUnisexSeizeChartToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :size_chart_unisex_file_name, :string
    add_column :categories, :size_chart_unisex_content_type, :string
    add_column :categories, :size_chart_unisex_file_size, :integer
    add_column :categories, :size_chart_unisex_updated_at, :datetime

  end
end
