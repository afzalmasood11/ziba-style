class AddCropRulesToFileAttachment < ActiveRecord::Migration
  def change
    add_column :file_attachments, :crop_rules, :text

  end
end
