class AddBraintreeVaultIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :braintree_vault_id, :string

  end
end
