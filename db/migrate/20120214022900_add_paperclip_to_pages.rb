class AddPaperclipToPages < ActiveRecord::Migration
  def self.up
    change_table :pages do |t|
      t.has_attached_file :banner
    end
  end

  def self.down
    drop_attached_file :pages, :banner
  end
end