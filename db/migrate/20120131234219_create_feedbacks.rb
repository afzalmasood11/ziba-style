class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.integer :rating
      t.text :comment
      t.integer :designer_order_id
      t.integer :customer_id

      t.timestamps
    end
    add_index :feedbacks, [:designer_order_id, :customer_id], :unique => true
    add_index :feedbacks, :customer_id
  end
end
