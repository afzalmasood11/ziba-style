class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :product_id
      t.integer :size_id
      t.integer :quantity

      t.timestamps
    end
    add_index :items, :product_id
    add_index :items, :size_id
  end
end
