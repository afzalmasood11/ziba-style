class RemoveCustomerIdFromFeedbacks < ActiveRecord::Migration
  def up 
    remove_index :feedbacks, [:designer_order_id, :customer_id]
    remove_index :feedbacks, :customer_id
    remove_column :feedbacks, :customer_id
    add_index :feedbacks, :designer_order_id
  end

  def down
    add_column :feedbacks, :customer_id, :integer
    add_index :feedbacks, [:designer_order_id, :customer_id], :unique => true
    add_index :feedbacks, :customer_id 
 end
end
