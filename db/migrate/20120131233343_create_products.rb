class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.integer :designer_id
      t.integer :category_id
      t.integer :gender_type 
      t.string :short_description
      t.string :history
      t.text :inspiration
      t.text :materials
      t.decimal :price, :precision => 9, :scale => 2
      t.integer :status
      t.boolean :active
      t.boolean :featured
      t.float :weight
      t.integer :return_days
      t.integer :lead_days

      t.timestamps
    end
    add_index :products, :designer_id
    add_index :products, :category_id
    add_index :products, :gender_type
  end
end
