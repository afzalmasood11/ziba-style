class ChangeOrderItemsIndexes < ActiveRecord::Migration
  def up
    remove_index :order_items, [:designer_order_id, :item_id]
    add_index :order_items, :item_id
    add_index :order_items, :designer_order_id
  end

  def down
    remove_index :order_items, :item_id
    remove_index :order_items, :designer_order_id
    add_index :order_items, [:designer_order_id, :item_id]
  end
end
