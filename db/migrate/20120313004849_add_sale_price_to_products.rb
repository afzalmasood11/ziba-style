class AddSalePriceToProducts < ActiveRecord::Migration
  def change
    add_column :products, :sale_price, :decimal, precision: 9, scale: 2, :default => 0
  end
end
