class AddAuthorizationAndMessageToReturns < ActiveRecord::Migration
  def change
    add_column :returns, :gateway_authorization_id, :string

    add_column :returns, :gateway_message, :string

  end
end
