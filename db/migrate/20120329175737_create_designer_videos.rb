class CreateDesignerVideos < ActiveRecord::Migration
  def change
    create_table :designer_videos do |t|
      t.string :name
      t.string :url
      t.integer :status
      t.integer :designer_id

      t.timestamps
    end
  end
end
