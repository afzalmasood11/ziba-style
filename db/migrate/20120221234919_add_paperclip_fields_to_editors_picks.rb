class AddPaperclipFieldsToEditorsPicks < ActiveRecord::Migration
  def self.up
    change_table :editor_picks do |t|
      t.has_attached_file :image
      t.has_attached_file :thumbnail
    end
  end

  def self.down
    drop_attached_file :editor_picks, :image
    drop_attached_file :editor_picks, :thumbnail
  end
end
