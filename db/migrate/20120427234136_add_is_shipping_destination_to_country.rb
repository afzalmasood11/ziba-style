class AddIsShippingDestinationToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :is_shipping_destination, :boolean, default: true

  end
end
