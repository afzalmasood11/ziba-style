class AddFileAttachmentToHomePageEntity < ActiveRecord::Migration
  def self.up
    add_column :home_page_entities, :homepage_image_file_name, 	:string
    add_column :home_page_entities, :homepage_image_content_type, :string
    add_column :home_page_entities, :homepage_image_file_size, 	:integer
    add_column :home_page_entities, :homepage_image_updated_at, 	:datetime
    add_column :home_page_entities, :homepage_image_type, 		:integer
  end

  def self.down
    remove_column :home_page_entities, :homepage_image_file_name
    remove_column :home_page_entities, :homepage_image_content_type
    remove_column :home_page_entities, :homepage_image_file_size
    remove_column :home_page_entities, :homepage_image_updated_at
    remove_column :home_page_entities, :homepage_image_type
  end
end