class AddDesignerTaxRateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :designer_tax_rate, :decimal

  end
end
