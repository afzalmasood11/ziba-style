class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :name

      t.timestamps
    end

    add_column :countries, :region_id, :integer
    add_index :countries, :region_id, after: :history
  end
end
