class AddPaperclipFieldsToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.has_attached_file :banner
    end
  end

  def self.down
    drop_attached_file :categories, :banner
  end
end
