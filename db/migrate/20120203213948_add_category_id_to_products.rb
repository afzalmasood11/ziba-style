class AddCategoryIdToProducts < ActiveRecord::Migration
  def up
    add_column :product_categories, :primary, :boolean, :default => false 
    remove_index :product_categories, :product_id
    add_index :product_categories, [:product_id, :primary], :name => 'index_product_and_primary'
  end
  def down 
    add_index :product_categories, :product_id
    remove_index :product_categories, :name => 'index_product_and_primary'
    remove_column :product_categories, :primary
  end
end
