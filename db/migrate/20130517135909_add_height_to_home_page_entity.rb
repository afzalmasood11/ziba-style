class AddHeightToHomePageEntity < ActiveRecord::Migration
  def change
    add_column :home_page_entities, :height, :integer
  end
end
