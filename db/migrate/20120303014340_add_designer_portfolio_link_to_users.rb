class AddDesignerPortfolioLinkToUsers < ActiveRecord::Migration
  def change
    add_column :users, :designer_portfolio_url, :string
  end
end
