class RemoveColumnGenderTypeFromCategory < ActiveRecord::Migration
  def up
    remove_column :categories, :gender_type
      end

  def down
    add_column :categories, :gender_type, :string
  end
end
