class AddCreditCardExpirationDateToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :credit_card_expiration_date, :date

  end
end
