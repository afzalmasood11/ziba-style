class AddBannerToCountry < ActiveRecord::Migration
  def self.up
    change_table :countries do |t|
      t.has_attached_file :banner
    end
  end

  def self.down
    drop_attached_file :countries, :banner
  end
end