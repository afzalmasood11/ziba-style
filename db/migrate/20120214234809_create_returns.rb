class CreateReturns < ActiveRecord::Migration
  def change
    create_table :returns do |t|
      t.integer :quantity
      t.text    :reason
      t.string  :code
      t.string  :certified
      t.integer :order_item_id

      t.timestamps
    end
  end
end
