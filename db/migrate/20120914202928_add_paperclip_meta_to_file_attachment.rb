class AddPaperclipMetaToFileAttachment < ActiveRecord::Migration
  def self.up
    add_column :file_attachments, :file_attachment_meta,    :text
  end

  def self.down
    remove_column  :file_attachments, :file_attachment_meta
  end
end
