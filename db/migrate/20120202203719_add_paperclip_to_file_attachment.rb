class AddPaperclipToFileAttachment < ActiveRecord::Migration
  def self.up
    change_table :file_attachments do |t|
      t.has_attached_file :attachment
    end
  end

  def self.down
    drop_attached_file :file_attachments, :attachment
  end
end
