class AddSizeChartsToCategory < ActiveRecord::Migration

  def change
    add_column :categories, :size_chart_man_file_name, :string
    add_column :categories, :size_chart_man_content_type, :string
    add_column :categories, :size_chart_man_file_size, :integer
    add_column :categories, :size_chart_man_updated_at, :datetime
    
    add_column :categories, :size_chart_woman_file_name, :string
    add_column :categories, :size_chart_woman_content_type, :string
    add_column :categories, :size_chart_woman_file_size, :integer
    add_column :categories, :size_chart_woman_updated_at, :datetime
  end
end
