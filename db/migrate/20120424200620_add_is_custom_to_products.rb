class AddIsCustomToProducts < ActiveRecord::Migration
  def change
    add_column :products, :is_custom, :boolean, :default => false
  end
end
