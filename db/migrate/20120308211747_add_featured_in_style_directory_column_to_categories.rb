class AddFeaturedInStyleDirectoryColumnToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :featured_in_style_directory, :boolean, { default: false }
  end
end
