class CreateProductExcludedCountries < ActiveRecord::Migration
  def change
    create_table :product_excluded_countries do |t|
      t.integer :product_id
      t.integer :country_id

      t.timestamps
    end
    add_index :product_excluded_countries, [:product_id, :country_id], :unique => true, :name => "product_excluded_countries_index"
  end
end
