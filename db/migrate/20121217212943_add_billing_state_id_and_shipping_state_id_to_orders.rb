class AddBillingStateIdAndShippingStateIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :billing_state_id, :integer

    add_column :orders, :shipping_state_id, :integer

  end
end
