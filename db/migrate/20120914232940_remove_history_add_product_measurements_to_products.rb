class RemoveHistoryAddProductMeasurementsToProducts < ActiveRecord::Migration
  def up
    remove_column :products, :history
    add_column :products, :product_measurements, :text
  end

  def down
    add_column :products, :history, :text
    remove_column :products, :product_measurements
  end
end
