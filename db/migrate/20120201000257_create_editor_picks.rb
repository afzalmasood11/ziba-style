class CreateEditorPicks < ActiveRecord::Migration
  def change
    create_table :editor_picks do |t|
      t.integer :product_id

      t.timestamps
    end
    add_index :editor_picks, :product_id
  end
end
