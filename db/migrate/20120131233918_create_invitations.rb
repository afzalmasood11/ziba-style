class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.string :email
      t.string :token
      t.integer :from_user_id
      t.integer :to_user_id

      t.timestamps
    end
    add_index :invitations, :token, :unique => true
    add_index :invitations, :from_user_id
    add_index :invitations, :to_user_id
  end
end
