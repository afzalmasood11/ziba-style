class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.integer :type
      t.string :email
      t.text :message
      t.string :fname
      t.string :lname
      t.string :phone
      t.integer :country_id
      t.string :order_number

      t.timestamps
    end
  end
end
