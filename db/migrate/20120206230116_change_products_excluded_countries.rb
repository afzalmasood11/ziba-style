class ChangeProductsExcludedCountries < ActiveRecord::Migration
  def change
  	rename_column :product_excluded_countries, :country_id, :excluded_country_id
  end
end
