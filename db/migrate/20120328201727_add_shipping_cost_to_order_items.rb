class AddShippingCostToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :shipping_cost, :decimal, :default => 0

  end
end
