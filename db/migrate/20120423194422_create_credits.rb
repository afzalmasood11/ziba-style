class CreateCredits < ActiveRecord::Migration
  def change
    create_table :credits do |t|
      t.string :code
      t.integer :user_id
      t.boolean :used
      t.float :amount

      t.timestamps
    end
  end
end
