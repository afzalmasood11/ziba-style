class AddSaleAndOriginalPriceToProducts < ActiveRecord::Migration
  def change
    add_column :products, :sale, :boolean
    add_column :products, :original_price, :decimal, :precision => 9, :scale => 2
		add_index :products, :sale
	end
end
