class CreateFileAttachments < ActiveRecord::Migration
  def change
    create_table :file_attachments do |t|
      t.boolean :attachment_primary
      t.integer :attachment_order
      t.integer :attachment_type
      t.integer :entity_type
      t.integer :entity_id

      t.timestamps
    end
    add_index :file_attachments, [:entity_id, :entity_type]
  end
end
