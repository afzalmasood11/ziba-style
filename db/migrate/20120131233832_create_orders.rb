class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.string :shipping_fname
      t.string :shipping_lname
      t.string :shipping_address1
      t.string :shipping_address2
      t.string :shipping_zipcode
      t.string :shipping_city
      t.string :shipping_state
      t.integer :shipping_country_id
      t.string :shipping_phone
      t.string :billing_fname
      t.string :billing_lname
      t.string :billing_address1
      t.string :billing_address2
      t.string :billing_zipcode
      t.string :billing_city
      t.string :billing_state
      t.integer :billing_country_id
      t.string :billing_phone
      t.integer :status

      t.timestamps
    end
    add_index :orders, [:customer_id, :status]
  end
end
