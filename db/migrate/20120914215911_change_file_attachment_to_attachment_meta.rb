class ChangeFileAttachmentToAttachmentMeta < ActiveRecord::Migration
  def change
    rename_column :file_attachments, :file_attachment_meta, :attachment_meta
  end
end
