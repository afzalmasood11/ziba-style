class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :designer_order_id
      t.integer :item_id
      t.decimal :price, :precision => 9, :scale => 2

      t.timestamps
    end
    add_index :order_items, [:designer_order_id, :item_id]
  end
end
