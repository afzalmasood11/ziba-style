# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



def truncate_table(table)
	delete = ActiveRecord::Base.connection_config[:adapter] == 'mysql2' ? 'TRUNCATE' : 'DELETE FROM'
	ActiveRecord::Base.connection.execute("#{delete} #{table}")	
end


# ---- SEED CATEGORIES ---- # 
def categories
	

	truncate_table('products')
	truncate_table('product_categories')
	truncate_table('product_excluded_countries')
	truncate_table('items')
	truncate_table('designer_orders')
	truncate_table('orders')
	truncate_table('order_items')
	truncate_table('returns')
	truncate_table('categories')
	User.all.each do |user|
		user.cart = ""
		user.save
	end




	topLevels = %w{Apparel Jewelry Accessories Objets\ D'Art}

	@apparel_both = %w{}
	@apparel_men = %w{Blazers Coats\ &\ Jackets Jeans Knitwear Nightwear Polos Casual\ shirts Formal\ Shirts Shorts Suits Swimwear T-shirts Trousers Shoes}
	@apparel_women = %w{Dresses Skirts Tops Tunics\ &\ Caftans Pants Shorts Jeans Jumpsuits Suits Knitwear Jackets\ &\ Blazers Coats\ &\ Outerwear Loungewear Beachwear Shoes}

	@jewelry_both = %w{Cufflinks}
	@jewelry_men = %w{Rings Necklaces Chains\ &\ Leather\ Cords Bracelets Cufflinks}
	@jewelry_women = %w{Rings Earrings Necklaces Pendants Chains\ &\ Leather\ Cords Bracelets Brooches Cufflinks}


	@accessories_men = %w{Neckties Hats Belts Gloves Caps Bags Pocket\ Squares Handkerchiefs Scarves Ties Bow\ Ties Wallets\ &\ Card\ Holders Key\ Rings Watches Pouches Glasses\ Cases Hi\ Tech\ Accessories}
	@accessories_women = %w{Hats Belts Hats Bags Scarves Shawls Gloves Hair\ Accessories Wallets\ &\ Card\ Holders Coin\ Purses Key\ Rings Pouches Watches Glasses\ Cases Hi\ Tech\ Accessories Umbrellas}
	@accessories_both = %w{Bags Wallets\ &\ Card\ Holders Key\ Rings Pouches Glasses\ Cases Watches Hi\ Tech\ Accessories Umbrellas Stationery Gift\ Ideas}

	@objets_d_art_both = %w{Objets\ D'Art Artwork Home\ Items Travel\ Souvenirs}

	topLevels.each do |cat| 
		parent = Category.create ({name: cat, featured_in_style_directory: true}) unless Category.find_by_name(cat)

		both = {both: instance_variable_get("@#{cat.gsub("'", "_").gsub(" ", "_").downcase}_both")}
		mens = {men: instance_variable_get("@#{cat.gsub("'", "_").gsub(" ", "_").downcase}_men")}
		womens = {women: instance_variable_get("@#{cat.gsub("'", "_").gsub(" ", "_").downcase}_women")}

		[both,mens,womens].each do |h|
			h.each do |g, subcats|
				unless subcats.nil?
					subcats.each do |name|
					 	subcat = Category.create({name: name, category_id: parent.id}) unless Category.find_by_name(name)
					end
				end
			end
		end
	end

	#Objets D'Art is a special case
	Category.create({name: "Objets D'Art", category_id: Category.find_by_name("Objets D'Art").id})

	puts 'done seeding categories!'
end
# ---- /end CATEGORIES ---- # 


# ---- SEED SIZES ---- # 
def sizes
	truncate_table('sizes')

	one_size_fits_all_text = "One size"

	@jewelry_both = %w{Cufflinks}
	@accessories_both = %w{Bags Wallets\ &\ Card\ Holders Key\ Rings Pouches Glasses\ Cases Watches Hi\ Tech\ Accessories Umbrellas Stationery Gift\ Ideas}
	@objets_d_art_both = %w{Objets\ D'Art Artwork Home\ Items Travel\ Souvenirs}

	unisex_cats = (@jewelry_both + @accessories_both + @objets_d_art_both).flatten

	# UNISEX PRODUCTS
	unisex_cats.each do |subcat_name|
		Category.where{(name == subcat_name) & (category_id != nil)}.each do |cat|
			Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:both]
		end
	end




	# men apparel

	
	# List sizes as XS, S, M, L etc in the dropdown menu
	# Casual shirts
	# T-shirts
	# Polos
	cats = Category.where{((name == 'Shorts') | (name == 'Trousers') | (name == 'Jeans'))}
	cats.each do |cat|
		others = { 'EU' => one_size_fits_all_text, 'IT' => one_size_fits_all_text, 'UK/US Waist' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		10.times do |i|
			i *= 2
			us_value = (i == 0) ? '00' : i
			if us_value.is_a? String and us_value=='00'
				wm_alpha = 'XS'
			elsif us_value > 16
				wm_alpha = '6XL'
			elsif us_value > 14
				wm_alpha = '5XL'
			elsif us_value > 12
				wm_alpha = '4XL'
			elsif us_value > 10
				wm_alpha = 'XXXL'
			elsif us_value > 8
				wm_alpha = 'XXL'
			elsif us_value > 6
				wm_alpha = 'XL'
			elsif us_value > 4
				wm_alpha = 'L' 
			elsif us_value > 2
				wm_alpha = 'M' 
			elsif us_value > 0
				wm_alpha = 'S' 
			end

			others = {
				'Alpha Size' => wm_alpha,
				'EU' => i+44,
				'IT' => i+44,
				'UK/US Waist' => i+28
			}
			size = Size.create us_value: "UK #{i+28}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
			
		end 
	end



	# List sizes in dropdown as US/UK 30, US/UK 32, US/UK 34, US/UK 36, US/UK 38, etc
	# Blazers
	# Knitwear
	# Jackets and coats
	# Suits
	# Nightwear
	cats = Category.where{((name == 'Blazers') | (name == 'Knitwear') | (name == 'Coats & Jackets') | (name == 'Suits') | (name == 'Nightwear'))}
	cats.each do |cat|
		others = { 'EU' => one_size_fits_all_text, 'IT' => one_size_fits_all_text, 'UK/US Waist' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		12.times do |i|
			i *= 2
			us_value = (i == 0) ? '00' : i-2

			if us_value.is_a? String and us_value=='00'
				wm_alpha = 'XXS'
			elsif us_value > 12
				wm_alpha = 'XXXL'
			elsif us_value > 10
				wm_alpha = 'XXL'
			elsif us_value > 8
				wm_alpha = 'XL'
			elsif us_value > 6
				wm_alpha = 'L'
			elsif us_value > 4
				wm_alpha = 'M' 
			elsif us_value > 2
				wm_alpha = 'S' 
			elsif us_value >= 2
				wm_alpha = 'XS' 
			elsif us_value == 0 
				wm_alpha = 'XXS'
			end

			others = {
				'EU' => i+40,
				'IT' => i+40,
				'UK/US Chest' => i+30,
				'UK/US Waist' => i+24,
				'FR' => i+40,
				'JP' => i+32
			}
			size = Size.create us_value: "US #{i+30}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		end 
	end


	# List sizes as XS, S, M, L etc in the dropdown menu
	# Casual shirts
	# T-shirts
	# Polos
	cats = Category.where{((name == 'Casual shirts') | (name == 'T-shirts') | (name == 'Polos'))}
	cats.each do |cat|

		others = { 'EU' => one_size_fits_all_text, 'IT' => one_size_fits_all_text, 'UK/US (Chest)' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]

		17.times do |i|
		
			i = i+1 if i > 14
			us_value = (i == 0) ? '00' : i
			if us_value.is_a? String and us_value=='00'
				wm_alpha = 'XXS'
			elsif us_value > 16
				wm_alpha = 'XXXL'
				i = i+1
			elsif us_value > 13
				wm_alpha = 'XXL'
			elsif us_value > 10
				wm_alpha = 'XL'
			elsif us_value > 7
				wm_alpha = 'L'
			elsif us_value > 4
				wm_alpha = 'M' 
			elsif us_value > 2
				wm_alpha = 'S' 
			elsif us_value > 0
				wm_alpha = 'XS' 
			end

			others = {
				'EU' => i+44,
				'IT' => i+44,
				'UK/US (Chest)' => i+34,
			}
			size = Size.create us_value: wm_alpha, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		end 
	end

	# List sizes as US/UK 14, US/UK 14.5, US/UK 15, US/UK 15.5, etc in dropdown list
	# 1) Men’s Formal Shirts
	cats = Category.where{(name == 'Formal Shirts')}
	cats.each do |cat|

		others = { 'EU' => one_size_fits_all_text, 'EU Neck' => one_size_fits_all_text, 'Wm Alpha' => one_size_fits_all_text, 'US/UK Neck' => one_size_fits_all_text, 'US Sleeve' => one_size_fits_all_text, 'US Chest' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]

		others = { 'EU' => "44", 'EU Neck' => "36", 'Wm Alpha' => 'XS', 'US/UK Neck' => "14", 'US Sleeve' => "32-33", 'US Chest' => "34-36" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	

		others = { 'EU' => "46", 'EU Neck' => "37", 'Wm Alpha' => 'S', 'US/UK Neck' => "14.5", 'US Sleeve' => "32-33", 'US (Chest)' => "36-38" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	
		
		others = { 'EU' => "46", 'EU Neck' => "38", 'Wm Alpha' => 'S', 'US/UK Neck' => "15", 'US Sleeve' => "32-35", 'US (Chest)' => "36-38" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	
		
		others = { 'EU' => "48", 'EU Neck' => "39", 'Wm Alpha' => 'M', 'US/UK Neck' => "15.5", 'US Sleeve' => "33-35", 'US (Chest)' => "38-40" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	
		
		others = { 'EU' => "49", 'EU Neck' => "40", 'Wm Alpha' => 'M', 'US/UK Neck' => "15.75", 'US Sleeve' => "33-35", 'US (Chest)' => "38-40" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	
		
		others = { 'EU' => "49", 'EU Neck' => "40", 'Wm Alpha' => 'M', 'US/UK Neck' => "16", 'US Sleeve' => "33-35", 'US (Chest)' => "38-40" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	
		
		others = { 'EU' => "50", 'EU Neck' => "41", 'Wm Alpha' => 'L', 'US/UK Neck' => "16", 'US Sleeve' => "34-36", 'US (Chest)' => "42-44" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	
		
		others = { 'EU' => "50", 'EU Neck' => "42", 'Wm Alpha' => 'L', 'US/UK Neck' => "16.5", 'US Sleeve' => "34-37", 'US (Chest)' => "42-44" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	
		
		others = { 'EU' => "52", 'EU Neck' => "43", 'Wm Alpha' => 'XL', 'US/UK Neck' => "17", 'US Sleeve' => "35-37", 'US (Chest)' => "46-48" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	
		
		others = { 'EU' => "52", 'EU Neck' => "44", 'Wm Alpha' => 'XL', 'US/UK Neck' => "17.5", 'US Sleeve' => "36.5-37", 'US (Chest)' => "50-52" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	

		others = { 'EU' => "54", 'EU Neck' => "45", 'Wm Alpha' => 'XXL', 'US/UK Neck' => "17.5", 'US Sleeve' => "36.5-37", 'US (Chest)' => "50-52" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	

		others = { 'EU' => "54", 'EU Neck' => "46", 'Wm Alpha' => 'XXL', 'US/UK Neck' => "18", 'US Sleeve' => "36.5-37", 'US (Chest)' => "50-52" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	

		others = { 'EU' => "56", 'EU Neck' => "48", 'Wm Alpha' => 'XXL', 'US/UK Neck' => "18.5", 'US Sleeve' => "36.5-37", 'US (Chest)' => "51-53" }
		Size.create us_value: "US #{others['US/UK Neck']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]	
	end

	# List sizes as XS, S, M, L, XL etc in dropdown list
	# 1) Swimwear
	cats = Category.where{(name == 'Swimwear')}
	cats.each do |cat|

		others = { 'EU' => one_size_fits_all_text, 'IT' => one_size_fits_all_text, 'UK/US Waist' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]

		10.times do |i|
			i *= 2
			us_value = (i == 0) ? '00' : i-2

			if us_value.is_a? String and us_value=='00'
				wm_alpha = 'XS'

			elsif us_value > 8
				wm_alpha = 'XXXL'
			elsif us_value > 6
				wm_alpha = 'XXL'
			elsif us_value > 4
				wm_alpha = 'XL' 
			elsif us_value > 2
				wm_alpha = 'L' 
			elsif us_value == 2
				wm_alpha = 'M' 
			elsif us_value == 0 
				wm_alpha = 'S'
			end

			others = {
				#'Wm Alpha' => wm_alpha,
				'EU' => i+44,
				'IT' => i+44,
				'UK/US Waist' => i+28
			}
			size = Size.create us_value: wm_alpha, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		end 
	end

	# womens apparel
	@apparel_women = %w{Dresses Skirts Tops Tunics\ &\ Caftans Pants Shorts Jeans Jumpsuits Suits Knitwear Jackets\ &\ Blazers Coats\ &\ Outerwear Loungewear Beachwear}
	cats = Category.where{ name.in(my{@apparel_women}) }
	cats.each do |cat|

		others = { 'EU' => one_size_fits_all_text, 'IT' => one_size_fits_all_text, 'UK/US Waist' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]

		8.times do |i|
			i *= 2
			us_value = i

			if us_value > 12
				wm_alpha = 'XXXL'				
			elsif us_value > 10
				wm_alpha = 'XXL'
			elsif us_value > 8
				wm_alpha = 'XL'
			elsif us_value > 6
				wm_alpha = 'L' 
			elsif us_value > 4
				wm_alpha = 'M' 
			elsif us_value > 2
				wm_alpha = 'S' 
			elsif us_value > 0
				wm_alpha = 'XS' 
			elsif us_value == 0 
				wm_alpha = 'XXS'
			end

			if i == 0
				waist = 23.5
			elsif i == 2
				waist = 24
			elsif i == 4
				waist = 25
			elsif i == 6
				waist = 26
			elsif i == 8
				waist = 27
			elsif i == 10
				waist = 28
			elsif i == 12
				waist = 29.5
			elsif i == 14
				waist = 31
			else
				waist = 31
			end

			others = {
				'Italy' => i+36,
				'Alpha Size' => wm_alpha,
				'UK' => i+4,
				'Waist (natural)' => waist,
				'US' => i+0,
				'France' => i+32,
				'Australia' => i+4,
				'Japan' => i+3
			}
			size = Size.create us_value: "US #{i}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
		end 
	end

	# Man's shoes
	cats = Category.where{(name == 'Shoes')}
	cats.each do |cat|
		# men shoes 
		17.times do |i|
			i *= 0.5
			us_value = i+6

			others = {
				'EU' => i+39,
				'IT' => i+39,
				'UK' => i+5,
				'US' => us_value,
			}

			size = Size.create us_value: "EU #{i+39}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		end
	end

	# Women's shoes
	cats = Category.where{(name == 'Shoes')}
	cats.each do |cat|
		15.times do |i|
			i *= 0.5
			us_value = i+4

			us_size = i+5

			others = {
				#'EU' => i+35,
				'IT' => i+35,
				'UK' => i+2,
				'US' => us_size,
				'FR' => i+36,
				'JP' => i+21
			}

			size = Size.create us_value: "EU #{others['EU']}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
		end
	end


	# Man's rings
	cats = Category.where{(name == 'Rings')}
	cats.each do |cat|
		

		sizes = [
			{'Circumference (inches)' => one_size_fits_all_text, 'Circumference (mm)' => one_size_fits_all_text, 'US' => one_size_fits_all_text,	'UK/Australia' => one_size_fits_all_text,'Japan' => one_size_fits_all_text, 'Switzerland' => one_size_fits_all_text, 'France' => one_size_fits_all_text, 'Germany' => one_size_fits_all_text},
			{'Circumference (inches)' => '2.192', 'Circumference (mm)' => '55.7', 'US' => 'US 7 1/2',	'UK/Australia' => 'O 1/2','Japan' => '15', 'Switzerland' => '16 1/2', 'France' => '55 1/4', 'Germany' => '17 1/2'},
			{'Circumference (inches)' => '2.242', 'Circumference (mm)' => '56.9', 'US' => 'US 8',			'UK/Australia' => 'P 1/2','Japan' => '16', 'Switzerland' => '17 3/4', 'France' => '56 1/2', 'Germany' => '18'},
			{'Circumference (inches)' => '2.292', 'Circumference (mm)' => '58.2', 'US' => 'US 8 1/2',	'UK/Australia' => 'Q 1/2','Japan' => '17', 'Switzerland' => '18 3/4', 'France' => '57 3/4', 'Germany' => '18 1/2'},
			{'Circumference (inches)' => '2.342', 'Circumference (mm)' => '59.5', 'US' => 'US 9',			'UK/Australia' => 'R 1/2','Japan' => '18', 'Switzerland' => '20', 		'France' => '59', 		'Germany' => '19'},
			{'Circumference (inches)' => '2.393', 'Circumference (mm)' => '60.8', 'US' => 'US 9 1/2',	'UK/Australia' => 'S 1/2','Japan' => '19', 'Switzerland' => '20 3/4', 'France' => '60 1/4', 'Germany' => '19 1/2'},
			{'Circumference (inches)' => '2.443', 'Circumference (mm)' => '62.1', 'US' => 'US 10',			'UK/Australia' => 'T 1/2','Japan' => '20', 'Switzerland' => '21 3/4', 'France' => '61 1/2', 'Germany' => '20'},
			{'Circumference (inches)' => '2.493', 'Circumference (mm)' => '63.3', 'US' => 'US 10 1/2',	'UK/Australia' => 'U 1/2','Japan' => '22', 'Switzerland' => '22 3/4', 'France' => '62 3/4', 'Germany' => '20 1/2'},
			{'Circumference (inches)' => '2.543', 'Circumference (mm)' => '64.6', 'US' => 'US 11',			'UK/Australia' => 'V 1/2','Japan' => '23', 'Switzerland' => '24 3/4', 'France' => '64', 		'Germany' => '20 3/4'},
			{'Circumference (inches)' => '2.594', 'Circumference (mm)' => '65.9', 'US' => 'US 11 1/2',	'UK/Australia' => 'W 1/2','Japan' => '24', 'Switzerland' => '25 1/2', 'France' => '65 1/4', 'Germany' => '21'},
			{'Circumference (inches)' => '2.644', 'Circumference (mm)' => '67.2', 'US' => 'US 12',			'UK/Australia' => 'X 1/2','Japan' => '25', 'Switzerland' => '27 1/2', 'France' => '66 1/2', 'Germany' => '21 1/4'},
			{'Circumference (inches)' => '2.694', 'Circumference (mm)' => '68.4', 'US' => 'US 12 1/2',	'UK/Australia' => 'Z',		'Japan' => '26', 'Switzerland' => '28 3/4', 'France' => '67 3/4', 'Germany' => '21 3/4'},
			{'Circumference (inches)' => '2.744', 'Circumference (mm)' => '69.7', 'US' => 'US 13',			'UK/Australia' => 'Z1',		'Japan' => '27', 'Switzerland' => '29 3/4', 'France' => '69', 		'Germany' => '22'},
			{'Circumference (inches)' => '2.795', 'Circumference (mm)' => '71', 	'US' => 'US 13 1/2',	'UK/Australia' => 'Z2',		'Japan' => '29', 'Switzerland' => '', 			'France' => '', 			'Germany' => ''},
			{'Circumference (inches)' => '2.845', 'Circumference (mm)' => '72.3', 'US' => 'US 14',			'UK/Australia' => 'Z3',		'Japan' => '31', 'Switzerland' => '', 			'France' => '', 			'Germany' => ''},
			{'Circumference (inches)' => '2.895', 'Circumference (mm)' => '73.5', 'US' => 'US 14 1/2',	'UK/Australia' => 'Z4',		'Japan' => '32', 'Switzerland' => '', 			'France' => '', 			'Germany' => ''}
		]

		sizes.each do |size|

			menu_value = size['US']
			size.delete 'US' 
			others = size

			size = Size.create us_value: "US #{menu_value}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		end
	end

	# Women's rings
	cats = Category.where{(name == 'Rings')}
	cats.each do |cat|
		
		sizes = [
			{'Diameter (inches)' => one_size_fits_all_text, 'Diameter (mm)' => one_size_fits_all_text, 'Circumference (inches)' => one_size_fits_all_text, 'Circumference (mm)' => one_size_fits_all_text, 'US' => one_size_fits_all_text,	'UK' => one_size_fits_all_text, 'EU' => one_size_fits_all_text},
			{'Diameter (inches)' => '0.61',  'Diameter (mm)' => '15.49', 'Circumference (inches)' => '1.92', 'Circumference (mm)' => '48.7', 'US' => 'US 4 3/4',	'UK' => 'J', 		 'EU' => '10'},
			{'Diameter (inches)' => '0.618', 'Diameter (mm)' => '15.70', 'Circumference (inches)' => '1.94', 'Circumference (mm)' => '49.3', 'US' => 'US 5',			'UK' => 'J 1/2', 'EU' => '10/11'},
			{'Diameter (inches)' => '0.626', 'Diameter (mm)' => '15.90', 'Circumference (inches)' => '1.97', 'Circumference (mm)' => '50.0', 'US' => 'US 5 1/4',	'UK' => 'K', 		 'EU' => '11'},
			{'Diameter (inches)' => '0.634', 'Diameter (mm)' => '16.10', 'Circumference (inches)' => '1.99', 'Circumference (mm)' => '50.6', 'US' => 'US 5 1/2',	'UK' => 'K 1/2', 'EU' => '12'},
			{'Diameter (inches)' => '0.642', 'Diameter (mm)' => '16.31', 'Circumference (inches)' => '2.02', 'Circumference (mm)' => '51.2', 'US' => 'US 5 3/4',	'UK' => 'L', 		 'EU' => '12/13'},
			{'Diameter (inches)' => '0.65',  'Diameter (mm)' => '16.51', 'Circumference (inches)' => '2.04', 'Circumference (mm)' => '51.9', 'US' => 'US 6',			'UK' => 'L 1/2', 'EU' => '13'},
			{'Diameter (inches)' => '0.658', 'Diameter (mm)' => '16.71', 'Circumference (inches)' => '2.07', 'Circumference (mm)' => '52.5', 'US' => 'US 6 1/4',	'UK' => 'M', 		 'EU' => '14'},
			{'Diameter (inches)' => '0.666', 'Diameter (mm)' => '16.92', 'Circumference (inches)' => '2.09', 'Circumference (mm)' => '53.1', 'US' => 'US 6 1/2',	'UK' => 'M 1/2', 'EU' => '14/15'},
			{'Diameter (inches)' => '0.674', 'Diameter (mm)' => '17.12', 'Circumference (inches)' => '2.12', 'Circumference (mm)' => '53.8', 'US' => 'US 6 3/4',	'UK' => 'N', 		 'EU' => '15'},
			{'Diameter (inches)' => '0.682', 'Diameter (mm)' => '17.32', 'Circumference (inches)' => '2.14', 'Circumference (mm)' => '54.4', 'US' => 'US 7',			'UK' => 'N 1/2', 'EU' => '15/16'},
			{'Diameter (inches)' => '0.69',  'Diameter (mm)' => '17.53', 'Circumference (inches)' => '2.17', 'Circumference (mm)' => '55.1', 'US' => 'US 7 1/4',	'UK' => 'O', 		 'EU' => '16'},
			{'Diameter (inches)' => '0.698', 'Diameter (mm)' => '17.73', 'Circumference (inches)' => '2.19', 'Circumference (mm)' => '55.7', 'US' => 'US 7 1/2',	'UK' => 'O 1/2', 'EU' => '17'},
			{'Diameter (inches)' => '0.706', 'Diameter (mm)' => '17.93', 'Circumference (inches)' => '2.22', 'Circumference (mm)' => '56.3', 'US' => 'US 7 3/4',	'UK' => 'P', 		 'EU' => '17/18'},
			{'Diameter (inches)' => '0.714', 'Diameter (mm)' => '18.14', 'Circumference (inches)' => '2.24', 'Circumference (mm)' => '57.0', 'US' => 'US 8',			'UK' => 'P 1/2', 'EU' => '18'},
			{'Diameter (inches)' => '0.722', 'Diameter (mm)' => '18.34', 'Circumference (inches)' => '2.27', 'Circumference (mm)' => '57.6', 'US' => 'US 8 1/4',	'UK' => 'Q', 		 'EU' => '18/19'},
		]

		sizes.each do |size|

			menu_value = size['US']
			size.delete 'US' 
			others = size

			size = Size.create us_value: "US #{menu_value}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
		end
	end


	# Man's Bracelets
	cats = Category.where{(name == 'Bracelets')}
	cats.each do |cat|
		size = Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		size = Size.create us_value: "XS", other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		size = Size.create us_value: "S", other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		size = Size.create us_value: "M", other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		size = Size.create us_value: "L", other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
	end


	# Women's Bracelets
	cats = Category.where{(name == 'Bracelets')}
	cats.each do |cat|
		size = Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
		size = Size.create us_value: "XS", other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
		size = Size.create us_value: "S", other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
		size = Size.create us_value: "M", other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
		size = Size.create us_value: "L", other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
	end


	# Man's Hats
	cats = Category.where{(name == 'Hats')}
	cats.each do |cat|
		sizes = [
			{'Alpha Size' => one_size_fits_all_text, 'Circumference (cm)' => one_size_fits_all_text, 'Circumference (inches)' => one_size_fits_all_text},
			{'Alpha Size' => 'XS', 'Circumference (cm)' => '54', 'Circumference (inches)' => '21.5'},
			{'Alpha Size' => 'S', 'Circumference (cm)' => '55', 'Circumference (inches)' => '21.75'},
			{'Alpha Size' => 'M', 'Circumference (cm)' => '56-57', 'Circumference (inches)' => '22-22.5'},
			{'Alpha Size' => 'L', 'Circumference (cm)' => '55-59', 'Circumference (inches)' => '22.75-23'},
			{'Alpha Size' => 'XL', 'Circumference (cm)' => '60-61', 'Circumference (inches)' => '23.5-24'},
			{'Alpha Size' => 'XXL', 'Circumference (cm)' => '62-64', 'Circumference (inches)' => '24.5-25.5'}
		]

		sizes.each do |size|

			us_value = size['Alpha Size']
			size.delete 'Alpha Size'
			other = size

			size = Size.create us_value: us_value, other_values: other, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		end
	end

	# Women's Hats
	cats = Category.where{(name == 'Hats')}
	cats.each do |cat|
		sizes = [
			{'Alpha Size' => one_size_fits_all_text},
			{'Alpha Size' => 'XS'},
			{'Alpha Size' => 'S'},
			{'Alpha Size' => 'M'},
			{'Alpha Size' => 'L'},
		]

		sizes.each do |size|

			us_value = size['Alpha Size']
			size.delete 'Alpha Size'
			other = size

			size = Size.create us_value: us_value, other_values: other, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
		end
	end


	# Man's Belts
	cats = Category.where{(name == 'Belts')}
	cats.each do |cat|
		others = { 'cm' => one_size_fits_all_text, 'inches' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]

		17.times do |i|
			i *= 2
			us_value = (i == 0) ? '00' : i-2

			if us_value.is_a? String and us_value=='00'
				wm_alpha = 'XXS'
			elsif us_value > 26
				wm_alpha = 'XXXL'
			elsif us_value > 24
				wm_alpha = 'XXL'
			elsif us_value > 20
				wm_alpha = 'XL'
			elsif us_value > 16
				wm_alpha = 'L'
			elsif us_value > 12
				wm_alpha = 'M' 
			elsif us_value > 6
				wm_alpha = 'S' 
			elsif us_value >= 4
				wm_alpha = 'XS' 
			elsif us_value >= 0 
				wm_alpha = 'XXS'
			end

			others = {
				'cm' => i*2.5+50,
				'inches' => ((i*2.5+50)/2.54).ceil,
			}

			others['inches'] = 19.5 if others['cm'] == 50.0
			others['inches'] = 21.5 if others['cm'] == 55.0
			others['inches'] = 39.5 if others['cm'] == 100.0
			others['inches'] = 43 	if others['cm'] == 110.0
			others['inches'] = 43.5 if others['cm'] == 115.0
			others['inches'] = 45.5 if others['cm'] == 120.0

			size = Size.create us_value: wm_alpha, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		end 


	end

	# Women's Belts
	cats = Category.where{(name == 'Belts')}
	cats.each do |cat|
		others = { 'cm' => one_size_fits_all_text, 'inches' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]

		17.times do |i|
			i *= 2
			us_value = (i == 0) ? '00' : i-2

			if us_value.is_a? String and us_value=='00'
				wm_alpha = 'XXS'
			elsif us_value > 26
				wm_alpha = 'XXXL'
			elsif us_value > 24
				wm_alpha = 'XXL'
			elsif us_value > 20
				wm_alpha = 'XL'
			elsif us_value > 16
				wm_alpha = 'L'
			elsif us_value > 12
				wm_alpha = 'M' 
			elsif us_value > 6
				wm_alpha = 'S' 
			elsif us_value >= 4
				wm_alpha = 'XS' 
			elsif us_value >= 0 
				wm_alpha = 'XXS'
			end

			others = {
				'cm' => i*2.5+50,
				'inches' => ((i*2.5+50)/2.54).ceil,
			}

			others['inches'] = 19.5 if others['cm'] == 50.0
			others['inches'] = 21.5 if others['cm'] == 55.0
			others['inches'] = 39.5 if others['cm'] == 100.0
			others['inches'] = 43 	if others['cm'] == 110.0
			others['inches'] = 43.5 if others['cm'] == 115.0
			others['inches'] = 45.5 if others['cm'] == 120.0

			size = Size.create us_value: wm_alpha, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
			puts size.inspect
		end 


	end

	# Man's Gloves
	cats = Category.where{(name == 'Gloves')}
	cats.each do |cat|
		others = { 'Alpha Size' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]

		10.times do |i|
			i *= 0.5

			us_value = i+7.5

			if us_value == 12
				wm_alpha = 'XXL'
			elsif us_value == 11.5
				wm_alpha = 'XXL'
			elsif us_value == 11
				wm_alpha = 'XL'
			elsif us_value == 10.5
				wm_alpha = 'XL'
			elsif us_value == 10
				wm_alpha = 'L/XL'
			elsif us_value == 9.5
				wm_alpha = 'L'
			elsif us_value == 9
				wm_alpha = 'M/L' 
			elsif us_value == 8.5
				wm_alpha = 'M' 
			elsif us_value == 8
				wm_alpha = 'S' 
			elsif us_value == 7.5 
				wm_alpha = 'XS'
			end

			others = {
				'Alpha Size' => wm_alpha
			}

			size = Size.create us_value: "US #{us_value}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
		end 


	end

	# Women's Gloves
	cats = Category.where{(name == 'Gloves')}
	cats.each do |cat|
		others = { 'Alpha Size' => one_size_fits_all_text }
		Size.create us_value: one_size_fits_all_text, other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]

		6.times do |i|
			i *= 0.5

			us_value = i+6


			if us_value == 8.5
				wm_alpha = 'L'
				us_value = "8.5 - 9"
			elsif us_value == 8
				wm_alpha = 'M/L' 
			elsif us_value == 7
				wm_alpha = 'M' 
				us_value = "7 - 7.5"
			elsif us_value == 6.5
				wm_alpha = 'S' 
			elsif us_value == 6
				wm_alpha = 'XS'
			end

			others = {
				'Alpha Size' => wm_alpha
			}

			if wm_alpha
				size = Size.create us_value: "US #{us_value}", other_values: others, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
			end
		end 


	end


	# arbitrary sizes for jewelry / accessories for now
	cat = Category.find_by_name('Jewelry')
	Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
	Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]

	cat = Category.find_by_name('Accessories')
	Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
	Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]



	@accessories_women = %w{Bags Scarves Shawls Hair\ Accessories Wallets\ &\ Card\ Holders Coin\ Purses Key\ Rings Pouches Watches Glasses\ Cases Hi\ Tech\ Accessories Umbrellas}
	@jewelry_women = %w{Earrings Necklaces Pendants Chains\ &\ Leather\ Cords Brooches Cufflinks}
	cat_names = (@accessories_women + @jewelry_women).flatten
	cats = Category.where{ name.in( my{cat_names} ) }.each do |cat|
		Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:women]
	end

	@accessories_men = %w{Neckties Bags Pocket\ Squares Handkerchiefs Scarves Ties Bow\ Ties Wallets\ &\ Card\ Holders Key\ Rings Watches Pouches Glasses\ Cases Hi\ Tech\ Accessories}
	@jewelry_men = %w{Necklaces Chains\ &\ Leather\ Cords Cufflinks}
	cat_names = (@accessories_men + @jewelry_men).flatten
	cats = Category.where{ name.in( my{cat_names} ) }.each do |cat|
		Size.create us_value: one_size_fits_all_text, other_values: {}, category_id: cat.id, gender_type: Category::GENDER_TYPES[:men]
	end







	puts 'done seeding sizes!'
end
# ---- /end SIZES ---- # 



# ---- SEED COUNTRIES and REGIONS ---- # 
def countries
	truncate_table('countries')
	truncate_table('regions')

	i = 1
	regions = %w{Africa Asia Australia Europe North\ America South\ America}
	regions.each do |r_name|
		r = Region.new({name: r_name})
		r.id = regions.index(r_name)+1 # force the id to match up with the jQuery map (1-6)
		r.save
	  # 4.times do
	  #   Country.create({ name: "Country_#{i}", region_id: r.id })
	  #   i += 1
	  # end
	end

	# add all countries -- here we go!
	# these use the `region_id`s set up above
	# Note: appears that seeds.rb is not using UTF-8 encoding, special chars in country names break things
	Country.create({'name'=>'Andorra','region_id'=> 4})
	Country.create({'name'=>'United Arab Emirates','region_id'=> 2})
	Country.create({'name'=>'Afghanistan','region_id'=> 2})
	Country.create({'name'=>'Antigua and Barbuda','region_id'=> 5})
	Country.create({'name'=>'Anguilla','region_id'=> 5})
	Country.create({'name'=>'Albania','region_id'=> 4})
	Country.create({'name'=>'Armenia','region_id'=> 2})
	Country.create({'name'=>'Angola','region_id'=> 1})
	Country.create({'name'=>'Argentina','region_id'=> 6})
	Country.create({'name'=>'American Samoa','region_id'=> 2})
	Country.create({'name'=>'Austria','region_id'=> 4})
	Country.create({'name'=>'Australia','region_id'=> 3})
	Country.create({'name'=>'Aruba','region_id'=> 5})
	Country.create({'name'=>'Aland Islands','region_id'=> 4})
	Country.create({'name'=>'Azerbaijan','region_id'=> 2})
	Country.create({'name'=>'Bosnia and Herzegovina','region_id'=> 4})
	Country.create({'name'=>'Barbados','region_id'=> 5})
	Country.create({'name'=>'Bangladesh','region_id'=> 2})
	Country.create({'name'=>'Belgium','region_id'=> 4})
	Country.create({'name'=>'Burkina Faso','region_id'=> 1})
	Country.create({'name'=>'Bulgaria','region_id'=> 4})
	Country.create({'name'=>'Bahrain','region_id'=> 2})
	Country.create({'name'=>'Burundi','region_id'=> 1})
	Country.create({'name'=>'Benin','region_id'=> 1})
	Country.create({'name'=>'Saint Barthelemy','region_id'=> 5})
	Country.create({'name'=>'Bermuda','region_id'=> 5})
	Country.create({'name'=>'Brunei','region_id'=> 2})
	Country.create({'name'=>'Bolivia','region_id'=> 6})
	Country.create({'name'=>'Bonaire, Saint Eustatius and Saba ','region_id'=> 5})
	Country.create({'name'=>'Brazil','region_id'=> 6})
	Country.create({'name'=>'Bahamas','region_id'=> 5})
	Country.create({'name'=>'Bhutan','region_id'=> 2})
	Country.create({'name'=>'Botswana','region_id'=> 1})
	Country.create({'name'=>'Belarus','region_id'=> 4})
	Country.create({'name'=>'Belize','region_id'=> 5})
	Country.create({'name'=>'Canada','region_id'=> 5})
	Country.create({'name'=>'Cocos Islands','region_id'=> 2})
	Country.create({'name'=>'Democratic Republic of the Congo','region_id'=> 1})
	Country.create({'name'=>'Central African Republic','region_id'=> 1})
	Country.create({'name'=>'Republic of the Congo','region_id'=> 1})
	Country.create({'name'=>'Switzerland','region_id'=> 4})
	Country.create({'name'=>'Ivory Coast','region_id'=> 1})
	Country.create({'name'=>'Cook Islands','region_id'=> 2})
	Country.create({'name'=>'Chile','region_id'=> 6})
	Country.create({'name'=>'Cameroon','region_id'=> 1})
	Country.create({'name'=>'China','region_id'=> 2})
	Country.create({'name'=>'Colombia','region_id'=> 6})
	Country.create({'name'=>'Costa Rica','region_id'=> 5})
	Country.create({'name'=>'Cuba','region_id'=> 5})
	Country.create({'name'=>'Cape Verde','region_id'=> 1})
	Country.create({'name'=>'Curacao','region_id'=> 5})
	Country.create({'name'=>'Christmas Island','region_id'=> 2})
	Country.create({'name'=>'Cyprus','region_id'=> 4})
	Country.create({'name'=>'Czech Republic','region_id'=> 4})
	Country.create({'name'=>'Germany','region_id'=> 4})
	Country.create({'name'=>'Djibouti','region_id'=> 1})
	Country.create({'name'=>'Denmark','region_id'=> 4})
	Country.create({'name'=>'Dominica','region_id'=> 5})
	Country.create({'name'=>'Dominican Republic','region_id'=> 5})
	Country.create({'name'=>'Algeria','region_id'=> 1})
	Country.create({'name'=>'Ecuador','region_id'=> 6})
	Country.create({'name'=>'Estonia','region_id'=> 4})
	Country.create({'name'=>'Egypt','region_id'=> 1})
	Country.create({'name'=>'Western Sahara','region_id'=> 1})
	Country.create({'name'=>'Eritrea','region_id'=> 1})
	Country.create({'name'=>'Spain','region_id'=> 4})
	Country.create({'name'=>'Ethiopia','region_id'=> 1})
	Country.create({'name'=>'Finland','region_id'=> 4})
	Country.create({'name'=>'Fiji','region_id'=> 2})
	Country.create({'name'=>'Falkland Islands','region_id'=> 6})
	Country.create({'name'=>'Micronesia','region_id'=> 2})
	Country.create({'name'=>'Faroe Islands','region_id'=> 4})
	Country.create({'name'=>'France','region_id'=> 4})
	Country.create({'name'=>'Gabon','region_id'=> 1})
	Country.create({'name'=>'United Kingdom','region_id'=> 4})
	Country.create({'name'=>'Grenada','region_id'=> 5})
	Country.create({'name'=>'Georgia','region_id'=> 2})
	Country.create({'name'=>'French Guiana','region_id'=> 6})
	Country.create({'name'=>'Guernsey','region_id'=> 4})
	Country.create({'name'=>'Ghana','region_id'=> 1})
	Country.create({'name'=>'Gibraltar','region_id'=> 4})
	Country.create({'name'=>'Greenland','region_id'=> 5})
	Country.create({'name'=>'Gambia','region_id'=> 1})
	Country.create({'name'=>'Guinea','region_id'=> 1})
	Country.create({'name'=>'Guadeloupe','region_id'=> 5})
	Country.create({'name'=>'Equatorial Guinea','region_id'=> 1})
	Country.create({'name'=>'Greece','region_id'=> 4})
	Country.create({'name'=>'Guatemala','region_id'=> 5})
	Country.create({'name'=>'Guam','region_id'=> 2})
	Country.create({'name'=>'Guinea-Bissau','region_id'=> 1})
	Country.create({'name'=>'Guyana','region_id'=> 6})
	Country.create({'name'=>'Hong Kong','region_id'=> 2})
	Country.create({'name'=>'Honduras','region_id'=> 5})
	Country.create({'name'=>'Croatia','region_id'=> 4})
	Country.create({'name'=>'Haiti','region_id'=> 5})
	Country.create({'name'=>'Hungary','region_id'=> 4})
	Country.create({'name'=>'Indonesia','region_id'=> 2})
	Country.create({'name'=>'Ireland','region_id'=> 4})
	Country.create({'name'=>'Israel','region_id'=> 2})
	Country.create({'name'=>'Isle of Man','region_id'=> 4})
	Country.create({'name'=>'India','region_id'=> 2})
	Country.create({'name'=>'British Indian Ocean Territory','region_id'=> 2})
	Country.create({'name'=>'Iraq','region_id'=> 2})
	Country.create({'name'=>'Iran','region_id'=> 2})
	Country.create({'name'=>'Iceland','region_id'=> 4})
	Country.create({'name'=>'Italy','region_id'=> 4})
	Country.create({'name'=>'Jersey','region_id'=> 4})
	Country.create({'name'=>'Jamaica','region_id'=> 5})
	Country.create({'name'=>'Jordan','region_id'=> 2})
	Country.create({'name'=>'Japan','region_id'=> 2})
	Country.create({'name'=>'Kenya','region_id'=> 1})
	Country.create({'name'=>'Kyrgyzstan','region_id'=> 2})
	Country.create({'name'=>'Cambodia','region_id'=> 2})
	Country.create({'name'=>'Kiribati','region_id'=> 2})
	Country.create({'name'=>'Comoros','region_id'=> 1})
	Country.create({'name'=>'Saint Kitts and Nevis','region_id'=> 5})
	Country.create({'name'=>'North Korea','region_id'=> 2})
	Country.create({'name'=>'South Korea','region_id'=> 2})
	Country.create({'name'=>'Kosovo','region_id'=> 4})
	Country.create({'name'=>'Kuwait','region_id'=> 2})
	Country.create({'name'=>'Cayman Islands','region_id'=> 5})
	Country.create({'name'=>'Kazakhstan','region_id'=> 2})
	Country.create({'name'=>'Laos','region_id'=> 2})
	Country.create({'name'=>'Lebanon','region_id'=> 2})
	Country.create({'name'=>'Saint Lucia','region_id'=> 5})
	Country.create({'name'=>'Liechtenstein','region_id'=> 4})
	Country.create({'name'=>'Sri Lanka','region_id'=> 2})
	Country.create({'name'=>'Liberia','region_id'=> 1})
	Country.create({'name'=>'Lesotho','region_id'=> 1})
	Country.create({'name'=>'Lithuania','region_id'=> 4})
	Country.create({'name'=>'Luxembourg','region_id'=> 4})
	Country.create({'name'=>'Latvia','region_id'=> 4})
	Country.create({'name'=>'Libya','region_id'=> 1})
	Country.create({'name'=>'Morocco','region_id'=> 1})
	Country.create({'name'=>'Monaco','region_id'=> 4})
	Country.create({'name'=>'Moldova','region_id'=> 4})
	Country.create({'name'=>'Montenegro','region_id'=> 4})
	Country.create({'name'=>'Saint Martin','region_id'=> 5})
	Country.create({'name'=>'Madagascar','region_id'=> 1})
	Country.create({'name'=>'Marshall Islands','region_id'=> 2})
	Country.create({'name'=>'Macedonia','region_id'=> 4})
	Country.create({'name'=>'Mali','region_id'=> 1})
	Country.create({'name'=>'Myanmar','region_id'=> 2})
	Country.create({'name'=>'Mongolia','region_id'=> 2})
	Country.create({'name'=>'Macao','region_id'=> 2})
	Country.create({'name'=>'Northern Mariana Islands','region_id'=> 2})
	Country.create({'name'=>'Martinique','region_id'=> 5})
	Country.create({'name'=>'Mauritania','region_id'=> 1})
	Country.create({'name'=>'Montserrat','region_id'=> 5})
	Country.create({'name'=>'Malta','region_id'=> 4})
	Country.create({'name'=>'Mauritius','region_id'=> 1})
	Country.create({'name'=>'Maldives','region_id'=> 2})
	Country.create({'name'=>'Malawi','region_id'=> 1})
	Country.create({'name'=>'Mexico','region_id'=> 5})
	Country.create({'name'=>'Malaysia','region_id'=> 2})
	Country.create({'name'=>'Mozambique','region_id'=> 1})
	Country.create({'name'=>'Namibia','region_id'=> 1})
	Country.create({'name'=>'New Caledonia','region_id'=> 2})
	Country.create({'name'=>'Niger','region_id'=> 1})
	Country.create({'name'=>'Norfolk Island','region_id'=> 2})
	Country.create({'name'=>'Nigeria','region_id'=> 1})
	Country.create({'name'=>'Nicaragua','region_id'=> 5})
	Country.create({'name'=>'Netherlands','region_id'=> 4})
	Country.create({'name'=>'Norway','region_id'=> 4})
	Country.create({'name'=>'Nepal','region_id'=> 2})
	Country.create({'name'=>'Nauru','region_id'=> 2})
	Country.create({'name'=>'Niue','region_id'=> 2})
	Country.create({'name'=>'New Zealand','region_id'=> 2})
	Country.create({'name'=>'Oman','region_id'=> 2})
	Country.create({'name'=>'Panama','region_id'=> 5})
	Country.create({'name'=>'Peru','region_id'=> 6})
	Country.create({'name'=>'French Polynesia','region_id'=> 2})
	Country.create({'name'=>'Papua New Guinea','region_id'=> 2})
	Country.create({'name'=>'Philippines','region_id'=> 2})
	Country.create({'name'=>'Pakistan','region_id'=> 2})
	Country.create({'name'=>'Poland','region_id'=> 4})
	Country.create({'name'=>'Saint Pierre and Miquelon','region_id'=> 5})
	Country.create({'name'=>'Pitcairn','region_id'=> 2})
	Country.create({'name'=>'Puerto Rico','region_id'=> 5})
	Country.create({'name'=>'Palestinian Territory','region_id'=> 2})
	Country.create({'name'=>'Portugal','region_id'=> 4})
	Country.create({'name'=>'Palau','region_id'=> 2})
	Country.create({'name'=>'Paraguay','region_id'=> 6})
	Country.create({'name'=>'Qatar','region_id'=> 2})
	Country.create({'name'=>'Reunion','region_id'=> 1})
	Country.create({'name'=>'Romania','region_id'=> 4})
	Country.create({'name'=>'Serbia','region_id'=> 4})
	Country.create({'name'=>'Russia','region_id'=> 4})
	Country.create({'name'=>'Rwanda','region_id'=> 1})
	Country.create({'name'=>'Saudi Arabia','region_id'=> 2})
	Country.create({'name'=>'Solomon Islands','region_id'=> 2})
	Country.create({'name'=>'Seychelles','region_id'=> 1})
	Country.create({'name'=>'Sudan','region_id'=> 1})
	Country.create({'name'=>'South Sudan','region_id'=> 1})
	Country.create({'name'=>'Sweden','region_id'=> 4})
	Country.create({'name'=>'Singapore','region_id'=> 2})
	Country.create({'name'=>'Saint Helena','region_id'=> 1})
	Country.create({'name'=>'Slovenia','region_id'=> 4})
	Country.create({'name'=>'Svalbard and Jan Mayen','region_id'=> 4})
	Country.create({'name'=>'Slovakia','region_id'=> 4})
	Country.create({'name'=>'Sierra Leone','region_id'=> 1})
	Country.create({'name'=>'San Marino','region_id'=> 4})
	Country.create({'name'=>'Senegal','region_id'=> 1})
	Country.create({'name'=>'Somalia','region_id'=> 1})
	Country.create({'name'=>'Suriname','region_id'=> 6})
	Country.create({'name'=>'Sao Tome and Principe','region_id'=> 1})
	Country.create({'name'=>'El Salvador','region_id'=> 5})
	Country.create({'name'=>'Sint Maarten','region_id'=> 5})
	Country.create({'name'=>'Syria','region_id'=> 2})
	Country.create({'name'=>'Swaziland','region_id'=> 1})
	Country.create({'name'=>'Turks and Caicos Islands','region_id'=> 5})
	Country.create({'name'=>'Chad','region_id'=> 1})
	Country.create({'name'=>'Togo','region_id'=> 1})
	Country.create({'name'=>'Thailand','region_id'=> 2})
	Country.create({'name'=>'Tajikistan','region_id'=> 2})
	Country.create({'name'=>'Tokelau','region_id'=> 2})
	Country.create({'name'=>'East Timor','region_id'=> 2})
	Country.create({'name'=>'Turkmenistan','region_id'=> 2})
	Country.create({'name'=>'Tunisia','region_id'=> 1})
	Country.create({'name'=>'Tonga','region_id'=> 2})
	Country.create({'name'=>'Turkey','region_id'=> 2})
	Country.create({'name'=>'Trinidad and Tobago','region_id'=> 5})
	Country.create({'name'=>'Tuvalu','region_id'=> 2})
	Country.create({'name'=>'Taiwan','region_id'=> 2})
	Country.create({'name'=>'Tanzania','region_id'=> 1})
	Country.create({'name'=>'Ukraine','region_id'=> 4})
	Country.create({'name'=>'Uganda','region_id'=> 1})
	Country.create({'name'=>'United States Minor Outlying Islands','region_id'=> 2})
	Country.create({'name'=>'United States','region_id'=> 5})
	Country.create({'name'=>'Uruguay','region_id'=> 6})
	Country.create({'name'=>'Uzbekistan','region_id'=> 2})
	Country.create({'name'=>'Vatican','region_id'=> 4})
	Country.create({'name'=>'Saint Vincent and the Grenadines','region_id'=> 5})
	Country.create({'name'=>'Venezuela','region_id'=> 6})
	Country.create({'name'=>'British Virgin Islands','region_id'=> 5})
	Country.create({'name'=>'U.S. Virgin Islands','region_id'=> 5})
	Country.create({'name'=>'Vietnam','region_id'=> 2})
	Country.create({'name'=>'Vanuatu','region_id'=> 2})
	Country.create({'name'=>'Wallis and Futuna','region_id'=> 2})
	Country.create({'name'=>'Samoa','region_id'=> 2})
	Country.create({'name'=>'Yemen','region_id'=> 2})
	Country.create({'name'=>'Mayotte','region_id'=> 1})
	Country.create({'name'=>'South Africa','region_id'=> 1})
	Country.create({'name'=>'Zambia','region_id'=> 1})
	Country.create({'name'=>'Zimbabwe','region_id'=> 1})
	Country.create({'name'=>'Serbia and Montenegro','region_id'=> 4})
	Country.create({'name'=>'Netherlands Antilles','region_id'=> 5})

	puts 'done seeding countries!'
end
# ---- /end countries ---- # 

def us_states
	us = Country.find_by_name("United States")

	State.create({'name' => 'Alaska', 'code' => 'AK', 'country_id' => us.id})
	State.create({'name' => 'Alabama', 'code' => 'AL', 'country_id' => us.id})
	State.create({'name' => 'American Samoa', 'code' => 'AS', 'country_id' => us.id})
	State.create({'name' => 'Arizona', 'code' => 'AZ', 'country_id' => us.id})
	State.create({'name' => 'Arkansas', 'code' => 'AR', 'country_id' => us.id})
	State.create({'name' => 'California', 'code' => 'CA', 'country_id' => us.id})
	State.create({'name' => 'Colorado', 'code' => 'CO', 'country_id' => us.id})
	State.create({'name' => 'Connecticut', 'code' => 'CT', 'country_id' => us.id})
	State.create({'name' => 'Delaware', 'code' => 'DE', 'country_id' => us.id})
	State.create({'name' => 'District of Columbia', 'code' => 'DC', 'country_id' => us.id})
	State.create({'name' => 'Florida', 'code' => 'FL', 'country_id' => us.id})
	State.create({'name' => 'Georgia', 'code' => 'GA', 'country_id' => us.id})
	State.create({'name' => 'Hawaii', 'code' => 'HI', 'country_id' => us.id})
	State.create({'name' => 'Idaho', 'code' => 'ID', 'country_id' => us.id})
	State.create({'name' => 'Illinois', 'code' => 'IL', 'country_id' => us.id})
	State.create({'name' => 'Indiana', 'code' => 'IN', 'country_id' => us.id})
	State.create({'name' => 'Iowa', 'code' => 'IA', 'country_id' => us.id})
	State.create({'name' => 'Kansas', 'code' => 'KS', 'country_id' => us.id})
	State.create({'name' => 'Kentucky', 'code' => 'KY', 'country_id' => us.id})
	State.create({'name' => 'Louisiana', 'code' => 'LA', 'country_id' => us.id})
	State.create({'name' => 'Maine', 'code' => 'ME', 'country_id' => us.id})
	State.create({'name' => 'Maryland', 'code' => 'MD', 'country_id' => us.id})
	State.create({'name' => 'Massachusetts', 'code' => 'MA', 'country_id' => us.id})
	State.create({'name' => 'Michigan', 'code' => 'MI', 'country_id' => us.id})
	State.create({'name' => 'Minnesota', 'code' => 'MN', 'country_id' => us.id})
	State.create({'name' => 'Mississippi', 'code' => 'MS', 'country_id' => us.id})
	State.create({'name' => 'Missouri', 'code' => 'MO', 'country_id' => us.id})
	State.create({'name' => 'Montana', 'code' => 'MT', 'country_id' => us.id})
	State.create({'name' => 'Nebraska', 'code' => 'NE', 'country_id' => us.id})
	State.create({'name' => 'Nevada', 'code' => 'NV', 'country_id' => us.id})
	State.create({'name' => 'New Hampshire', 'code' => 'NH', 'country_id' => us.id})
	State.create({'name' => 'New Jersey', 'code' => 'NJ', 'country_id' => us.id})
	State.create({'name' => 'New Mexico', 'code' => 'NM', 'country_id' => us.id})
	State.create({'name' => 'New York', 'code' => 'NY', 'country_id' => us.id})
	State.create({'name' => 'North Carolina', 'code' => 'NC', 'country_id' => us.id})
	State.create({'name' => 'North Dakota', 'code' => 'ND', 'country_id' => us.id})
	State.create({'name' => 'Ohio', 'code' => 'OH', 'country_id' => us.id})
	State.create({'name' => 'Oklahoma', 'code' => 'OK', 'country_id' => us.id})
	State.create({'name' => 'Oregon', 'code' => 'OR', 'country_id' => us.id})
	State.create({'name' => 'Pennsylvania', 'code' => 'PA', 'country_id' => us.id})
	State.create({'name' => 'Rhode Island', 'code' => 'RI', 'country_id' => us.id})
	State.create({'name' => 'South Carolina', 'code' => 'SC', 'country_id' => us.id})
	State.create({'name' => 'South Dakota', 'code' => 'SD', 'country_id' => us.id})
	State.create({'name' => 'Tennessee', 'code' => 'TN', 'country_id' => us.id})
	State.create({'name' => 'Texas', 'code' => 'TX', 'country_id' => us.id})
	State.create({'name' => 'Utah', 'code' => 'UT', 'country_id' => us.id})
	State.create({'name' => 'Vermont', 'code' => 'VT', 'country_id' => us.id})
	State.create({'name' => 'Virginia', 'code' => 'VA', 'country_id' => us.id})
	State.create({'name' => 'Washington', 'code' => 'WA', 'country_id' => us.id})
	State.create({'name' => 'West Virginia', 'code' => 'WV', 'country_id' => us.id})
	State.create({'name' => 'Wisconsin', 'code' => 'WI', 'country_id' => us.id})
	State.create({'name' => 'Wyoming', 'code' => 'WY', 'country_id' => us.id})

	  # Province of Canada
	  country_id = Country.find_by_name("Canada").id
	  canada_province_codes = ["AB", "BC", "MB", "NB", "NL", "NS", "ON", "PE", "QC", "SK"]
	  ["Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland and Labrador", "Nova Scotia", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan"].each_with_index do |province, index|
	    State.create({name: province, code: canada_province_codes[index], country_id: country_id})
	  end

  puts "States seeded"
end






# ---- SEED PRODUCTS (and designers) ---- # 
# for testing only
def products
	truncate_table('products')
	truncate_table('product_categories')
	truncate_table('items')

	# just delete fake users so we can keep our real ones
	User.delete_all("email LIKE '%design.com'")
	maxuid = User.select{max(id).as('max')}.first.max || 0

	fnames_m = ['Aaron', 'Adam', 'Albert', 'Boris', 'Carlos', 'Chris', 'Dave', 'Derek', 'Doug', 'Eric', 'Greg', 'Guillermo', 'Jake', 'Joe', 'Jon', 'Jonathan', 'Justin', 'Manish', 'Muhammad', 'Mustafa', 'Pavan', 'Pierre', 'Roger', 'Ryu', 'Samuel', 'Shoji', 'Terrence', 'Thomas', 'William', 'Xavier', 'Zachary']
	fnames_f = ['Abigail', 'Amanda', 'Amina', 'Amy', 'Belle', 'Calista', 'Claire', 'Emily', 'Erin', 'Gwen', 'Helen', 'Isabella', 'Jenny', 'Julia', 'Karen', 'Kristin', 'Madelin', 'Madison', 'Martina', 'Nina', 'Rebecca', 'Rita', 'Sabrina', 'Sarah', 'Sophie', 'Stephanie', 'Tina', 'Vicky']
	lnames = ['Abdullah', 'Adams', 'Bentley', 'Barilla', 'Chen', 'Chu', 'Cruz', 'Daniels', 'Edwards', 'Fischer', 'Gunderson', 'Henson', 'Huber', 'Iniro', 'Jones', 'Kwon', 'Lillio', 'Marshall', 'Mendoza', 'Michaels', 'Newton', 'Orgullo', 'Rogers', 'Smith', 'Tanaka', 'Thompson', 'Waldorf', 'Washani', 'Williamson', 'Yoshimitsu', 'Zamundi']
	dnames = ['{{fname}} Designs', '{{lname}} Wear', '{{lname}}Co', '{{fname}} and Friends', 'Studio {{lname}}', '{{fname}} and Stuff', "{{fname}}'s Cool Things"]
	pnames = %w{Huge Wonderful Comfy Beautiful Crazy Old-fashioned Soft Bulky Striped Plain Flexible}

	users = []
	items = []

	40.times do |i|
		gender = (Random.rand < 0.5) ? User::GENDER[:female] : User::GENDER[:male]
		fnames = (gender == User::GENDER[:female]) ? fnames_f : fnames_m
		u = User.new({
			gender: gender,
			fname: fnames.sample,
			lname: lnames.sample,
			email: "designer_#{i}@design.com",
			address1: '123 Main st.',
			city: 'Citytown',
			zipcode: '11223',
			state: 'CA',
			phone: '555-555-1234',
			designer_portfolio_url: "http:://myportfol.io",
			designer_city: "DesignerTown",
			designer_biography: "My Life as a Designer"
		})
		c_id = Country.random.id
		u.country_id = c_id
		u.designer_country_id = c_id
		u.designer_name = dnames.sample.gsub('{{fname}}', u.fname).gsub('{{lname}}', u.lname)
		u.role = User::ROLE[:designer]
		u.designer_status = User::STATUS[:approved]
		u.designer_active = true
		#u.save({validate: false})
		u.id = maxuid + i + 1
		users << u

		# seed some products for this designer
		Random.rand(10).times do |j|
			# cat = Category.subcategories.random
			cat = Category.where{category_id != nil}.random
			p = Product.new({
				designer_id: u.id,
				name: pnames.sample + " " + (cat.name == 'Pants' ? cat.name : cat.name.singularize),
				gender_type: Category::GENDER_TYPES[:women], #we only have sizing for women at the moment
				short_description: "This is a nice product.",
				price: Random.rand(1000),
				status: Product::STATUS[:approved],
				active: true,
				inspiration: "inspiration",
				materials: "materials",
				primary_category_id: cat.id,
				lead_days: 5,
				product_measurements: 'measurements...'
			})
			#cant use bulk import because of our 'primary_category_id' custom save function
			p.save({validate: false})
			p.status = Product::STATUS[:approved]

			#attach primary picture to product
			# file = FileAttachment.new
			# file.attachment = File.open("seed_pic.jpeg")
			# file.attachment_primary = true
			# file.attachment_type = FileAttachment::TYPE[:product_picture]
			# p.primary_image = file

			p.save
			# puts p.inspect
			puts p.errors.inspect if p.errors.present?

		end
		puts "done seeding designer#{u.id}"
		# /end products
	end
	# /end designers

	# save our new designers
	User.import users, validate: false

	# seed some items for this product
	Product.all.each do |p|
		# TODO: limit this by # of relevant sizes 
		sizes = p.relevant_sizes
		if sizes.present?
			Random.rand(5).times do |k|
				item = Item.new({
					product_id: p.id,
					# TODO: check if that size has already been used
					size_id: sizes.random.id,
					quantity: Random.rand(10)
				})

				items << item
			end
		end
		puts "done seeding product#{p.id}"
		# /end items
	end

	Item.import items, validate: false

	puts 'done seeding products!'
end

# ---- SEED PAGES ---- # 
def pages
	Page.create({:name => "about", 								:title => "About Us"}) 						 unless Page.find_by_name("about")
	Page.create({:name => "style", 								:title => "Style Directory"})			 unless Page.find_by_name("style")
	Page.create({:name => "privacy", 							:title => "Privacy"})							 unless Page.find_by_name("privacy")
	Page.create({:name => "terms_of_service", 		:title => "Terms Of Use"})				 unless Page.find_by_name("terms_of_service")
	Page.create({:name => "contact", 							:title => "Contact"})							 unless Page.find_by_name("contact")
	Page.create({:name => "membership", 					:title => "Membership"})					 unless Page.find_by_name("membership")
	Page.create({:name => "faq", 									:title => "FAQ"})									 unless Page.find_by_name("faq")
	Page.create({:name => "shipping_and_returns", :title => "Shipping & Returns"})	 unless Page.find_by_name("shipping_and_returns")

	puts 'done seeding pages!'
end

# ---- SEED SALE PAGE ---- # 
def sales
	Sale.create({:name => "sale"})
	puts 'done seeding sales!'
end

# --- EXECUTE! ---- 
# comment out any individual methods if you don't want to seed (/truncate) them

categories
sizes
countries
us_states
products #this is for development only
pages
sales
