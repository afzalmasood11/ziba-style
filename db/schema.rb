# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130517135909) do

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.boolean  "featured_in_style_directory",        :default => false
    t.string   "style_directory_image_file_name"
    t.string   "style_directory_image_content_type"
    t.integer  "style_directory_image_file_size"
    t.datetime "style_directory_image_updated_at"
    t.string   "size_chart_man_file_name"
    t.string   "size_chart_man_content_type"
    t.integer  "size_chart_man_file_size"
    t.datetime "size_chart_man_updated_at"
    t.string   "size_chart_woman_file_name"
    t.string   "size_chart_woman_content_type"
    t.integer  "size_chart_woman_file_size"
    t.datetime "size_chart_woman_updated_at"
    t.string   "size_chart_unisex_file_name"
    t.string   "size_chart_unisex_content_type"
    t.integer  "size_chart_unisex_file_size"
    t.datetime "size_chart_unisex_updated_at"
    t.string   "homepage_image_file_name"
    t.string   "homepage_image_content_type"
    t.integer  "homepage_image_file_size"
    t.datetime "homepage_image_updated_at"
    t.string   "slug"
  end

  add_index "categories", ["category_id"], :name => "index_categories_on_category_id"
  add_index "categories", ["slug"], :name => "index_categories_on_slug"

  create_table "contacts", :force => true do |t|
    t.integer  "contact_type"
    t.string   "email"
    t.text     "message"
    t.string   "fname"
    t.string   "lname"
    t.string   "phone"
    t.integer  "country_id"
    t.string   "order_number"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "link"
    t.integer  "discover_product"
  end

  create_table "countries", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "history"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "region_id"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.string   "flag_file_name"
    t.string   "flag_content_type"
    t.integer  "flag_file_size"
    t.datetime "flag_updated_at"
    t.string   "history_title"
    t.boolean  "is_shipping_destination", :default => true
    t.string   "slug"
  end

  add_index "countries", ["name"], :name => "index_countries_on_name", :unique => true
  add_index "countries", ["region_id"], :name => "index_countries_on_region_id"
  add_index "countries", ["slug"], :name => "index_countries_on_slug"

  create_table "credits", :force => true do |t|
    t.string   "code"
    t.integer  "user_id"
    t.boolean  "used"
    t.float    "amount"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "referring_user_id"
    t.integer  "order_id"
  end

  add_index "credits", ["code"], :name => "index_credits_on_code", :unique => true

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "designer_orders", :force => true do |t|
    t.integer  "designer_id"
    t.integer  "order_id"
    t.string   "ups_tracking"
    t.integer  "shipping_status"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "designer_orders", ["designer_id", "order_id"], :name => "index_designer_orders_on_designer_id_and_order_id", :unique => true

  create_table "designer_videos", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.integer  "status"
    t.integer  "designer_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "editor_picks", :force => true do |t|
    t.integer  "product_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "thumbnail_file_name"
    t.string   "thumbnail_content_type"
    t.integer  "thumbnail_file_size"
    t.datetime "thumbnail_updated_at"
    t.string   "description"
  end

  add_index "editor_picks", ["product_id"], :name => "index_editor_picks_on_product_id"

  create_table "feedbacks", :force => true do |t|
    t.integer  "rating"
    t.text     "comment"
    t.integer  "designer_order_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "feedbacks", ["designer_order_id"], :name => "index_feedbacks_on_designer_order_id"

  create_table "file_attachments", :force => true do |t|
    t.boolean  "attachment_primary"
    t.integer  "attachment_order"
    t.integer  "attachment_type"
    t.string   "entity_type"
    t.integer  "entity_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string   "meta"
    t.text     "attachment_meta"
    t.text     "crop_rules"
  end

  add_index "file_attachments", ["entity_id", "entity_type"], :name => "index_file_attachments_on_entity_id_and_entity_type"

  create_table "friendly_id_slugs", :force => true do |t|
    t.string   "slug",                         :null => false
    t.integer  "sluggable_id",                 :null => false
    t.string   "sluggable_type", :limit => 40
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type"], :name => "index_friendly_id_slugs_on_slug_and_sluggable_type", :unique => true
  add_index "friendly_id_slugs", ["sluggable_id"], :name => "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], :name => "index_friendly_id_slugs_on_sluggable_type"

  create_table "home_page_entities", :force => true do |t|
    t.integer  "entity_id"
    t.string   "entity_type"
    t.integer  "order"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.string   "homepage_image_file_name"
    t.string   "homepage_image_content_type"
    t.integer  "homepage_image_file_size"
    t.datetime "homepage_image_updated_at"
    t.integer  "homepage_image_type"
    t.integer  "height"
  end

  create_table "invitations", :force => true do |t|
    t.string   "email"
    t.string   "token"
    t.integer  "from_user_id"
    t.integer  "to_user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.text     "message"
    t.integer  "product_id"
  end

  add_index "invitations", ["from_user_id"], :name => "index_invitations_on_from_user_id"
  add_index "invitations", ["to_user_id"], :name => "index_invitations_on_to_user_id"
  add_index "invitations", ["token"], :name => "index_invitations_on_token", :unique => true

  create_table "items", :force => true do |t|
    t.integer  "product_id"
    t.integer  "size_id"
    t.integer  "quantity"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "items", ["product_id"], :name => "index_items_on_product_id"
  add_index "items", ["size_id"], :name => "index_items_on_size_id"

  create_table "mercury_images", :force => true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "order_items", :force => true do |t|
    t.integer  "designer_order_id"
    t.integer  "item_id"
    t.decimal  "price",             :precision => 9,  :scale => 2
    t.datetime "created_at",                                                      :null => false
    t.datetime "updated_at",                                                      :null => false
    t.integer  "quantity"
    t.boolean  "sale"
    t.decimal  "original_price",    :precision => 9,  :scale => 2
    t.decimal  "shipping_cost",     :precision => 10, :scale => 0, :default => 0
  end

  add_index "order_items", ["designer_order_id"], :name => "index_order_items_on_designer_order_id"
  add_index "order_items", ["item_id"], :name => "index_order_items_on_item_id"
  add_index "order_items", ["sale"], :name => "index_order_items_on_sale"

  create_table "orders", :force => true do |t|
    t.integer  "customer_id"
    t.string   "shipping_fname"
    t.string   "shipping_lname"
    t.string   "shipping_address1"
    t.string   "shipping_address2"
    t.string   "shipping_zipcode"
    t.string   "shipping_city"
    t.string   "shipping_state"
    t.integer  "shipping_country_id"
    t.string   "shipping_phone"
    t.string   "billing_fname"
    t.string   "billing_lname"
    t.string   "billing_address1"
    t.string   "billing_address2"
    t.string   "billing_zipcode"
    t.string   "billing_city"
    t.string   "billing_state"
    t.integer  "billing_country_id"
    t.string   "billing_phone"
    t.integer  "status"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.date     "credit_card_expiration_date"
    t.datetime "processed_at"
    t.string   "transaction_id"
    t.integer  "transaction_platform"
    t.string   "transaction_cc"
    t.integer  "billing_state_id"
    t.integer  "shipping_state_id"
  end

  add_index "orders", ["customer_id", "status"], :name => "index_orders_on_customer_id_and_status"

  create_table "pages", :force => true do |t|
    t.string   "name"
    t.text     "text"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.string   "title"
    t.boolean  "private",             :default => false
  end

  add_index "pages", ["name"], :name => "index_pages_on_name", :unique => true

  create_table "product_categories", :force => true do |t|
    t.integer  "product_id"
    t.integer  "category_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "primary",     :default => false
  end

  add_index "product_categories", ["category_id"], :name => "index_product_categories_on_category_id"
  add_index "product_categories", ["product_id", "primary"], :name => "index_product_and_primary"

  create_table "product_excluded_countries", :force => true do |t|
    t.integer  "product_id"
    t.integer  "excluded_country_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "product_excluded_countries", ["product_id", "excluded_country_id"], :name => "product_excluded_countries_index", :unique => true

  create_table "products", :force => true do |t|
    t.string   "name"
    t.integer  "designer_id"
    t.integer  "gender_type"
    t.string   "short_description"
    t.text     "inspiration"
    t.text     "materials"
    t.decimal  "price",                :precision => 9,  :scale => 2
    t.integer  "status"
    t.boolean  "active"
    t.boolean  "featured"
    t.float    "weight"
    t.string   "weight_type"
    t.integer  "return_days"
    t.integer  "lead_days"
    t.datetime "created_at",                                                             :null => false
    t.datetime "updated_at",                                                             :null => false
    t.boolean  "sale"
    t.decimal  "original_price",       :precision => 9,  :scale => 2
    t.decimal  "sale_price",           :precision => 9,  :scale => 2
    t.decimal  "shipping_cost",        :precision => 10, :scale => 0, :default => 0
    t.boolean  "is_custom",                                           :default => false
    t.text     "product_measurements"
    t.string   "slug"
  end

  add_index "products", ["designer_id"], :name => "index_products_on_designer_id"
  add_index "products", ["gender_type"], :name => "index_products_on_gender_type"
  add_index "products", ["sale"], :name => "index_products_on_sale"
  add_index "products", ["slug"], :name => "index_products_on_slug"

  create_table "regions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "returns", :force => true do |t|
    t.integer  "quantity"
    t.text     "reason"
    t.string   "code"
    t.string   "certified"
    t.integer  "order_item_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.integer  "status"
    t.boolean  "restocked"
    t.string   "gateway_authorization_id"
    t.string   "gateway_message"
  end

  create_table "sales", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
  end

  create_table "sizes", :force => true do |t|
    t.string   "us_value"
    t.text     "other_values"
    t.integer  "category_id"
    t.integer  "gender_type"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "sizes", ["category_id"], :name => "index_sizes_on_category_id"
  add_index "sizes", ["gender_type"], :name => "index_sizes_on_gender_type"

  create_table "states", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "country_id"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                                                 :null => false
    t.string   "encrypted_password",                                                    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "designer_name"
    t.text     "designer_biography"
    t.boolean  "designer_active"
    t.integer  "designer_status"
    t.integer  "designer_country_id"
    t.string   "fname"
    t.string   "lname"
    t.string   "address1"
    t.string   "address2"
    t.string   "zipcode"
    t.string   "city"
    t.string   "state"
    t.integer  "country_id"
    t.string   "phone"
    t.integer  "role"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                                                            :null => false
    t.datetime "updated_at",                                                            :null => false
    t.text     "cart"
    t.integer  "gender"
    t.integer  "invitation_limit"
    t.string   "designer_city"
    t.string   "designer_paypal"
    t.date     "dob"
    t.text     "email_preferences"
    t.string   "designer_portfolio_url"
    t.string   "braintree_customer_id"
    t.decimal  "designer_tax_rate",      :precision => 5, :scale => 3, :default => 0.0
    t.integer  "designer_state_id"
    t.decimal  "designer_int_tax_rate",  :precision => 5, :scale => 3, :default => 0.0
    t.string   "wepay_credit_card_id"
    t.string   "designer_ships_from"
    t.string   "authentication_token"
    t.string   "slug"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["country_id"], :name => "index_users_on_country_id"
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["role"], :name => "index_users_on_role"
  add_index "users", ["slug"], :name => "index_users_on_slug"

end
