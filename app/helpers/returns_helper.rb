module ReturnsHelper

  def return_status_to_string(return_status)
    case return_status
      when Return::STATUS[:pending]; "Pending"
      when Return::STATUS[:shipped]; "Shipped"
      when Return::STATUS[:arrived]; "Arrived"
      when Return::STATUS[:restocked]; "Restocked"
      when Return::STATUS[:refunded]; "Refunded"
      else; "Unknown"      
    end
  end

end
