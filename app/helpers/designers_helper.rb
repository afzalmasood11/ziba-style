module DesignersHelper
	def designer_banner_pic(designer)
		designer.banner_pic.blank? ? image_tag("css/banner.jpg", width: "960", height: "180") : image_tag(designer.banner_pic.attachment.url(:banner), width: "960", height: "180", alt: "Banner Photo")
	end

	def designer_profile_pic(designer)
		designer.profile_pic.blank? ? image_tag("css/bio1.jpg", width: "150", height: "150") : image_tag(designer.profile_pic.attachment.url(:profile), width: "150", height: "150", alt: "Profile Photo")
	end
end
