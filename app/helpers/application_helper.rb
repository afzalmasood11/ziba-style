module ApplicationHelper

	def user_processing_cart?
	  current_user and !current_user.pending_checkout.blank?
	end

  def params_include?(p, item)
    params[p] ? params[p].include?(item.to_s) : nil
  end

  def remove_linebreaks(str)
    str.gsub(/\n/, "").html_safe
  end

  # helper method for gallery on products#show page
  def product_nav_link(attachment, css_class='')
    link_to (image_tag attachment.url(:mini)),
      attachment.url(:medium),
      'class' => css_class,
      'data-popup' => attachment.url(:popup),
      'data-image' => attachment.url(:medium),
      'data-zoom-image'=> "#{attachment.url(:original)}"
  end
  
  def share_on_twitter_link(url = nil, text = nil, style = nil)
    text = (text != nil) ? text : ''
    url = (url != nil) ? u(url) : ''
    "<a href=\"javascript:twitterPop('#{url}', '#{text}')\" #{style ? "style=\"#{style}\"" : "" }><img  class=\"twitter_btn\" src=\"/assets/css/twitter-icon.png\"/></a>    
    <script type=\"text/javascript\">function twitterPop(url, text) {
    mywindow = window.open('http://twitter.com/share?url='+url+'&text='+text,\"Tweet_widow\",\"channelmode=no,directories=no,location=no,menubar=no,scrollbars=no,toolbar=no,status=no,width=500,height=375,left=300,top=200\");
    mywindow.focus();
    }</script>".html_safe
  end 

  def share_on_facebook_link(product)
    "<a onclick=\"window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=#{u product.name}&amp;p[summary]=#{u product.short_description}&amp;p[url]=#{u share_product_url(product)}&amp;&amp;p[images][0]=#{u product.primary_image.attachment.url if product.primary_image}','sharer','toolbar=0,status=0,width=548,height=325');\" href=\"javascript:void(0)\"><img  class=\"facebook_btn\" src=\"/assets/css/face-icon.png\"/></a>"
  end

  def share_on_pinterest_link(product)
    "<a href=\"http://pinterest.com/pin/create/button/?url=#{u share_product_url(product)}&media=#{u product.primary_image.attachment.url if product.primary_image}&description=#{u product.short_description}\" onclick=\"window.open(this.href); return false;\"><img src=\"/assets/css/pinterest-icon.png\"/></a>"
  end

  def share_product_by_email_link(product)
    "#{link_to (image_tag "/assets/css/mail.png"), share_by_email_product_path(product), class: "fancybox"}"
  end

  def title(title = nil)
    if title.present?
      content_for :title, title
    else
      content_for?(:title) ? SEO_CONFIG['default_title'] + ' | ' + content_for(:title) : SEO_CONFIG['default_title']
    end
  end

  def meta_keywords(tags = nil)
    if tags.present?
      content_for :meta_keywords, tags
    else
      content_for?(:meta_keywords) ? [content_for(:meta_keywords), SEO_CONFIG['meta_keywords']].join(', ') : SEO_CONFIG['meta_keywords']
    end
  end

  def meta_description(desc = nil)
    if desc.present?
      content_for :meta_description, desc
    else
      content_for?(:meta_description) ? content_for(:meta_description) : SEO_CONFIG['meta_description']
    end
  end

end
