module CountriesHelper
	def country_flag(country, size)
		# after we add paperclip to country, should actually look up country image url (+ size)
		case size
		when :medium
			image_tag('css/flag-big.png')
		when :small
			image_tag('css/flag1.png')
		end
	end
end
