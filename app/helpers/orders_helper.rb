module OrdersHelper

  def shipping_status_to_s(shipping_status)
    shipping_status == DesignerOrder::SHIPPING_STATUS[:shipped] ? "Shipped" : "Pending"
  end

  def ups_tracking_link(shipping_number)
    "http://wwwapps.ups.com/WebTracking/track?HTMLVersion=5.0&loc=en_US&Requester=UPSHome&WBPM_lid=homepage%2Fct1.html_pnl_trk&trackNums=#{shipping_number}&track.x=Track"
  end

end
