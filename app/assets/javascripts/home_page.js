var colCount = 0;
var colWidth = 250;
var margin = 10;
var spaceLeft = 0;
var windowWidth = 0;
var blocks = [];

$(function(){
  $(window).resize(setupBlocks);
});

function setupBlocks() {
	windowWidth = $(window).width();
	blocks = [];

	// Calculate the margin so the blocks are evenly spaced within the window
	colCount = Math.floor(windowWidth/(colWidth+margin*2));
	spaceLeft = (windowWidth - ((colWidth*colCount)+(margin*(colCount-1)))) / 2;
	//console.log(spaceLeft);

	for(var i=0;i<colCount;i++){
		blocks.push(margin);
	}
  positionBlocks();
  $('#entities_area').css('height', Array.max(blocks)+'px');
  $('.block').css('visibility', 'visible');
}

function positionBlocks() {
	$('.block').css('position', 'absolute');
  $('.block').css('padding', '5px 0');
  $('.block').each(function(i){
		var min = Array.min(blocks);
		var index = $.inArray(min, blocks);
		var leftPos = margin+(index*(colWidth+margin)) - 7;
    newmin = min + 95;
		$(this).css({
			'left':(leftPos+spaceLeft)+'px',
			'top':newmin+'px'
		});
    var img = $(this).find('img');
    var data_src = img.attr('data-src');
    $(this).find('img').attr('src', data_src);
		blocks[index] = min+$(this).outerHeight()+margin;
	});
}

// Function to get the Min value in Array
Array.min = function(array) {
    return Math.min.apply(Math, array);
};
Array.max = function(array) {
    return Math.max.apply(Math, array);
};