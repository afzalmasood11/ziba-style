$("#cancel_checkout_link").live("click", function(e){ e.preventDefault(); if(confirm("Are you sure?")){ $("#cancel_checkout").submit(); } });
$("#order_payment_method_switch.active").live("click", function(e){ 
  e.preventDefault(); 
  $(".credit-card-info").toggle(); 
  $(".billing-addresss").toggle(); 
  $("#action_buttons").toggle(); 
  $(".paypal").toggle(); 
  $(".order_payment_method_indicator").toggleClass("bold");
})

$("#submit_paypal_payment").live("click", function(e){
  e.preventDefault();
  $('#paypal_payment').val("1");
  $('.edit_order').submit();
});

$('.credit_order_switch').live("change", function(e){
  e.preventDefault();
  var val = ($(this).is(":checked")) ? parseFloat($(this).data("order_total")) - parseFloat($(this).data("credit")) : parseFloat($(this).data("order_total"));
  $('.order_total_with_tax').text("$"+val.formatMoney());
})

$(".order_checkout_quantity_dropdown").live("change", function(){
  var total = 0;
  var total_tax = 0;
  var total_with_tax = 0;

  $(".order_checkout_quantity_dropdown").each(function(){

    total = total + (parseFloat($(this).data("price")) + parseFloat($(this).data("shipping-cost"))) * parseInt($(this).val());
    total_tax = total_tax + (parseFloat($(this).data("price")) * parseInt($(this).val())) * parseFloat($(this).data("tax_rate"));
  });

  total_with_tax = total + total_tax;
  $('.order_total').text("$"+total.formatMoney());
  $('.order_total_tax').text("$"+total_tax.formatMoney());
  $('.order_total_with_tax').text("$"+total_with_tax.formatMoney());
})

$(function(){
  $(".checkout .paypal").hide();
  $('.credit_order_switch').change();

  $(".order_checkout_quantity").each(function(){

    var qty = $(this).val();
    var max_qty = $(this).data("quantity");
    var tax_rate = $(this).data("tax-rate");
    var price = $(this).data("price");
    var shipping_cost = $(this).data("shipping-cost");
    var selectbox_id = $(this).attr('id');
    var selectbox = '<select id="'+selectbox_id+'" name="'+$(this).attr('name')+'" data-tax_rate="'+tax_rate+'" data-price="'+price+'" data-shipping-cost="'+shipping_cost+'" style="" class="order_checkout_quantity_dropdown"></select>';
    var shell = $(this).closest('td')
    var i;
    $(this).remove();
    shell.html(selectbox);
    for(i=1;i<parseInt(max_qty)+1;i++){
      $('#'+selectbox_id).append($("<option></option>").attr("value",i).text(i));
    }
    $('#'+selectbox_id).val(qty);
  })



})

function display_thank_you_for_your_order(order){
  $('body').append('<a style="display:none" href="/order_complete" id="link_to_thank_you_for_your_order"></a>');
  $('#link_to_thank_you_for_your_order').fancybox().click();
}
