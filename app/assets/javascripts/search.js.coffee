$ ->
  $('.show-search-box').click ->
    $searchBox = $('.search-container')
    if $searchBox.is(':visible') then $searchBox.fadeOut() else $searchBox.fadeIn().find('.search-query').focus()
    false