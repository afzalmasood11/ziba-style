//submitWithAjax override for form submit (with configurable options)
jQuery.fn.submitWithAjax = function(options) {
		if (typeof options.success == 'undefined') options.success = null; 
		if (typeof options.type == 'undefined') options.type = 'POST'; 
		if (typeof options.dataType == 'undefined') options.dataType = 'json';

    this.submit(function() {
      if(typeof options.callbefore === 'function'){
        if(!options.callbefore($(this))){
          return false;
        }
      }  
      // prevent race conditions between a flurry of requests
      if (this.pending) {
        this.pending.abort();
      }

			this.pending = $.ajax({
			  type: options.type,
			  url: this.action,
			  data: $(this).serialize(),
			  success: function(data) {
          if (typeof options.success === 'function') {
            options.success(data);
          }
          positionFooter();
        },
			  dataType: options.dataType
			});
      return false;
    })
    return this;
};

Number.prototype.formatMoney = function(c, d, t){
  var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

$(function() {
  if($('.login_subheader')[0]){
    if($('.alert_messages')[0]){
      $('.login_subheader').css({'top': $('.alert_messages').length * 130 });
    }
    var $window = $(window),
   $stickyEl = $('.login_subheader'),
   elTop = $stickyEl.offset().top;
   $stickyEl2 = $('.login_fancybox_wrapper');

   $window.scroll(function() {

        $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop);
        $('.login_fancybox_wrapper').toggleClass('sticky', $window.scrollTop() > elTop);
        
    });
  }
  $("#order_billing_as_shipping").change(function(e){
    if($(this).is(":checked")){
      $("input, select").each(function(){
        if($(this).attr("name").indexOf("order[billing") > -1){
          var input_name = $(this).attr("name").replace("billing", "shipping"),
              input      = $(this);
          $.each(order_shipping_info, function(k, v) {
            if(k == input_name){ 
              input.val(v).change().val(v); 
            }
          });
        }
      })
    }else{
      $("input").each(function(){
        if($(this).attr("name").indexOf("order[billing") > -1){
          var input_name = $(this).attr("name").replace("billing", "shipping")
          $.each(order_shipping_info, function(k, v) {
            if(k == input_name){ $(this).val("") }
          });
        }
      })
    }
  })


  // improve file input presentation on registration forms
  $('input#file').customFileInput();
  // this makes the customFileInputs jQuery plugin actually work
  // the plugin creates the .customfile-feedback div, on click
  // simulate a click on the now hidden file upload input
  $('.customfile-feedback, .customfile-button').click(function() { 
      $('#file').click();
  })

  var gender_categories = "";
  $('input[name="product[gender_type]"]').change(function(){
    var previously_selected_value = $("#product_primary_category_id").val();
    if(gender_categories == ""){
      gender_categories = $("#product_primary_category_id").html();
    }
    $("#product_primary_category_id").html(gender_categories)
    gender_type_radio_val = $('input[name="product[gender_type]"]:checked').val();

    if($('input[name="product[gender_type]"]').is(":checked") != ""){
      $("#product_primary_category_id").find("option").each(function(){
        if( typeof($(this).attr("data-gender_type")) === "undefined" || $(this).data("gender_type").indexOf(parseInt(gender_type_radio_val)) == -1){
          $(this).remove();
        }
      })
      $("#product_primary_category_id").removeAttr("disabled");
      $("#product_primary_category_id").val(previously_selected_value);
    }else{
      $("#product_primary_category_id").attr("disabled", "disabled");
    }
  }).change();

	$('.field, textarea').focus(function() {
        if(this.title==this.value) {
            this.value = '';
        }
    }).blur(function(){
        if(this.value=='') {
            this.value = this.title;
        }
    });
	
	// Drop Down
	$('#navigation ul li, .header-top ul.menu li').hover(function(){ 
		var dd = $(this).find('ul.dd:eq(0)');

		if (!dd.is(':visible')) {
      // dd.stop(true,true).slideDown();
			dd.stop(true,true).slideDown(function() { 
      
        var $dd = $(this);
        if ($dd.hasClass('countries-dropdown')) {
          $dd.jScrollPane({ showArrows: true }) 
        }      

      });
			$(this).find('a:eq(0)').addClass('hover');			
		}
	 },
	 function(){  
		var dd = $(this).find('ul.dd');
		if (dd.is(':visible')) {
			$(this).find('ul.dd').stop(true,true).slideUp();
			$(this).find('a:eq(0)').removeClass('hover');
		}
 	});


  // generate options object literal to be passed to $.jcarousel
  // only shows next/prev buttons when appropriate
  // only enables autoscrolling when appropriate
  // this fixes some undesirable jcarousel UX behaviors 
  var jcarouselOptions = function(numItems, pageLength) {
    return {
      scroll:1,
      auto: (numItems >= pageLength ? 4 : false),
      showArrows: (numItems > 4),
      buttonNextHTML: (numItems >= pageLength ? '<div></div>' : null ),
      buttonPrevHTML: (numItems >= pageLength ? '<div></div>' : null ),
      wrap: "circular",
      start: 1,
      itemFallbackDimension: 960  
    };
  }

  var editorPickCount     = $('#editor-picks-slider').find('li').length;  
  var bigEditorPickCount  = $('.big-editor-picks-slider').find('li').length;
  var asSeenOnCount       = $('#as-seen-on-slider').find('.box').length;
  if(editorPickCount > 0){
    $('#editor-picks-slider').jcarousel(jcarouselOptions(editorPickCount, 5));
  }
  if(bigEditorPickCount > 0){

    var params                = jcarouselOptions(bigEditorPickCount, 1);
        params.buttonNextHTML = null;
        params.buttonPrevHTML = null;
        params.showArrows     = false;
        params.auto           = false;

    $('.big-editor-picks-slider').jcarousel(params);

    $('ul.big-editor-picks-slider li .pick-controls a.jcarousel.prev, ul.big-editor-picks-slider li .pick-controls a.jcarousel.next').click(function(e) {
      e.preventDefault();
      if($(this).hasClass("prev")){
        $('.big-editor-picks-slider').jcarousel('prev');
      }
      if($(this).hasClass("next")){
        $('.big-editor-picks-slider').jcarousel('next');       
      }
    });

    $(document).keydown(function(e){
        if (e.keyCode == 37) { 
           $('.big-editor-picks-slider').jcarousel('prev');
           return false;
        }
        if (e.keyCode == 39) { 
           $('.big-editor-picks-slider').jcarousel('next');       
           return false;
        }        
    });
  }
  if(asSeenOnCount > 0){
    $("#as-seen-on-slider").jcarousel(jcarouselOptions(asSeenOnCount, 5));
  }

	$('.scroll-pane, .scroll-designer-pane').jScrollPane({
		showArrows: true
	});

  // Want to enable scrolling here unless mercury editor is active
  $('.mercury-scroll').jScrollPane({ showArrows: true });  

	
	$('.filter > ul > li').each(function() {
		$(this).find('a:eq(0)').not('.alpha').click(function(e) {
			e.preventDefault();
      if(typeof filters_hide_delay !== "undefined") clearTimeout(filters_hide_delay);
			if ( !$(this).parent().find('.dd').is(':visible') ) {
				$('.filter ul li .dd').slideUp();
				$slideMe = $(this).parent().find('.dd');
        $slideMe.slideDown(function() {
          if ($(this).hasClass('countries')) {
            $(this).jScrollPane({ showArrows: true });
          }
        });
			} else {
				$(this).parent().find('.dd').slideUp();
			};
		});
	});

	$('h5 span.details').hover(function() {
		$(this).parents('.box:eq(0)').find('.popup').fadeIn(); 
	}, function() {
		$('.popup').hide();
	});

	$('.gallery-nav li a').click(function(){
		var big_img = $(this).attr('href');

		
    // switch the large image to the one the user clicked on
    $('.big-image img').fadeOut('fast', function() {
      $(this).attr('src', big_img).fadeIn();
    })

    $('.big-image a').attr('href', $(this).data('popup'))

		$('.gallery-nav li a').removeClass('active');
		$(this).addClass('active');
		return false;
 	});
});


// ---------------------------
// -- Filtering related functions (for designers index, and product filtering)
// ---------------------------
var sizes_dd_template;
var designers_dd_template;
var product_template;

function embedVideos(){
  console.log('called embedVideos');
  $('.oembed').oembed(null, {
    embedMethod: 'auto',
    maxHeight: 169,
    maxWidth: 300
  });
  $('.oembed').hide();
  //$(".oembed").each(function(){
    //var url = $(this).attr("href");
    //var parent = $(this).parent("div");
    //parent.html("");
    //parent.oembed(url,{ maxWidth: 300, maxHeight: 200 });
  //})
}

var video_div;
$(".more_videos").live("click", function(e){
  e.preventDefault();
  video_div = $(".jcarousel-container").html();
  $(".video").each(function(){
    $(this).removeClass("hidden_video");
    $(this).find("a").addClass("oembed");
  })
  embedVideos();
  //$(".featured.videos ul").jcarousel();
  $(".featured.videos .jcarousel-next, .featured.videos .jcarousel-prev").show();
  $(this).addClass("less_videos").removeClass("more_videos").text("View Less -");
})

$(".less_videos").live("click", function(e){
  e.preventDefault();
  $(".jcarousel-container").html(video_div);
  $(this).addClass("more_videos").removeClass("less_videos").text("View More +");
})


$(function() {

  embedVideos();
  $(".featured.videos ul").jcarousel();
  $(".featured.videos .jcarousel-next, .featured.videos .jcarousel-prev").hide();

	sizes_dd_template = Handlebars.compile($('#sizes-dd-template').html());
	designers_dd_template = Handlebars.compile($('#designers-dd-template').html());
	product_template = Handlebars.compile($('#product-template').html());

  if($('#general_modal.login_modal_window.signed_out_modal').length > 0 && !$('.custom-fancybox-overlay')[0]){
    $('body').append($('<div class="custom-fancybox-overlay  fancybox-overlay fancybox-overlay-fixed"></div>'));
  }
  $('.login_modal_window form#new_user').submitWithAjax({
      callbefore: function($form){

        var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        $email_field = $form.find('#user_email');
        if(!emailExp.test($email_field.val())){
          $error_field = $form.parent().parent().find('.errors');
          $error_field.text('please enter a valid email address');
          $error_field.show();
          $email_field.parent().addClass('error');
          return false;
        }else{
          return true;
        }
      },
      success: function(data) { 
        if(data.success){
          if(data.action == "sign_in"){
            referrer = data.redirect ? data.redirect : (data.last_url ? data.last_url : '/');
            window.location = referrer;
          }else if(data.action == "password_reset"){
            $('.login_modal_window form#new_user').parent().parent().find('.login_header').text(data.message);
            $('.login_modal_window form#new_user').remove();
          }
        }else{
          $('.login_modal_window form#new_user .login_field').addClass('error');
          $error_field = $('.login_modal_window form#new_user').parent().parent().find('.errors');

          $error_field.text(processEmailErrors(data));
          $error_field.show();
        }
      },
  });

  $('form#new_contact').submitWithAjax({
      success: function(data) { 
        if(data.success){
          $('#invite_modal .login_form .login_header').text('Your request has been received.');
          $('#new_contact').remove();
        }else{
          $('form#new_contact .login_field').addClass('error');
          $error_field = $('form#new_contact').parent().parent().find('.errors');
          $error_field.text(processEmailErrors(data));
          $error_field.show();
        }
      },
  });

  $('#filterForm').submitWithAjax({
      // success: function(data) { 
      //   //console.log(data);
      //   $('#designers').html(designers_template({designers: data}));
      // },
      type: 'GET',
      dataType: 'script'
  });

  $(document).on('change', '#filterForm input', function(e) {
  	// e.preventDefault();
    $('#filterForm').submit();
  });

  $(document).on('change', '#filterFormNoAjax input', function(e) {
    e.preventDefault();
    $('#filterFormNoAjax').submit();
  });

  $(document).on('click', '#pagination.ajax a', function(e) {
    e.preventDefault();
    $.get($(this).attr('href'), null, null, 'script');
  }); 

  $(document).on('click', '.designers-more', function(e) {
  	e.preventDefault();
  	$('#designers-more').toggle();
  });

  var displayed = false;
  $(document).on('click', '.login_modal_button', function(e){

    e.preventDefault();
    $('.login_modal_window').hide();
    $($(this).attr('href') + ' .errors').hide();
    if($(this).attr('href') == '#general_modal'){
      $($(this).attr('href')).find('.login_header').text($(this).data('message'));
    }
    if(displayed){
      $($(this).attr('href')).show();

    } else {
      $($(this).attr('href')).fadeIn('fast');
    }
    if(!$('.custom-fancybox-overlay')[0]){
      $('body').append($('<div class="custom-fancybox-overlay  fancybox-overlay fancybox-overlay-fixed"></div>'));
    }
    displayed = true;

  });
  $(document).on('click', '.custom-fancybox-overlay', function(e){
    e.preventDefault();
    displayed=false;
    $('.login_modal_window').fadeOut('fast');
    $(this).hide();
  });
  $(document).keyup(function(e) {
    if (e.keyCode == 27) {     
      e.preventDefault();
      displayed=false;
      $('.login_modal_window').fadeOut('fast');
      $('.custom-fancybox-overlay').remove();
    }   // esc
  });

  $(document).keyup(function(e) {
    if (e.keyCode == 27) {     
      e.preventDefault();
      displayed=false;
      $('.login_modal_window').fadeOut('fast');
      $('.custom-fancybox-overlay').remove();
    }   // esc
  });
  // $('.login_old .login_form #user_email').val("EMAIL").attr("title", "EMAIL"); //.css("color", "grey");
  // $('.login_old .login_form #user_password').hide().after('<input type="text" value="PASSWORD" id="tmp_user_password" class="blink" />');
  // $('.login_old .login_form a.popup, .fancybox').fancybox({
  //   type: 'ajax'
  // });
  $('.login .login_form #user_email').val("EMAIL").attr("title", "EMAIL"); //.css("color", "grey");
  $('.login .login_form #user_password').hide().after('<input type="text" value="PASSWORD" id="tmp_user_password" class="blink" />');
  $('.login .login_form a.popup, .fancybox').fancybox({
    type: 'ajax'
  });
  // $('.login_modal_button').fancybox({
  //   padding: 0,
  //   margin: 0,
  //   scrolling: 'no',
  //   wrapCSS: 'login_fancybox_wrapper',
  //   autoCenter: false,
  //   onUpdate: function(){
  //     $('.login_fancybox_wrapper').css({
  //       'top': $('.login_subheader').offset().top + 60
  //     });
  //   }
  // });



  $('a.popup.designer-already-has-account').fancybox({});

  // on the become a designer page
  $('#login-popup #user_email').val("EMAIL").attr("title", "EMAIL"); //.css("color", "grey");
  $('#login-popup #user_password').hide().after('<input type="text" value="PASSWORD" id="tmp_user_password" class="blink" />');

  // $('.login .login_form #user_email')
  //   .blur(function() { 
  //     $(this).removeClass('selected');
  //     if ($(this).val() === 'EMAIL') {
  //       $(this).css('letter-spacing', '5px')
  //     } else {
  //       $(this).css('letter-spacing', '2px')
  //     }
  //   })
  //   .focus(function() {
  //     $(this).addClass('selected');
  //     if ($(this).val() === 'EMAIL') {
  //       $(this).css('letter-spacing', '5px')
  //     } else {
  //       $(this).css('letter-spacing', '2px')
  //     }
      
  //   });

  $('.login .login_form #user_password')
    .blur(function() { $(this).removeClass('selected'); })
    .focus(function() { $(this).addClass('selected'); });  

  var updateCartCount = function(n) {
    $('.cart_link').text('(' + n + ')');
  };

  var showCartPopup = function() {
    
    // remove existing cart popup
    hideCartPopup();

    $.get('/cart', function(response) {

      $('.shell').prepend(response);
      $('#cart-popup-form').submitWithAjax({
        success: function() {
          hideCartPopup();
          showCartPopup();
        }
      });

      // each time the cart popup is retrieved, update the cart item count
      var cartCount = 0;
      $(response).find('li.cart-item').each(function() {
        cartCount += $(this).data('item-quantity');
      });          
      updateCartCount(cartCount);

    }, 'html');

  };

  var hideCartPopup = function() {
    $('.cart-popup').remove();
  };

  // $('.cart_link').hover(showCartPopup, function() {});
  $('.cart_link').click(function() {
    
    if ($('.cart-popup:visible').length > 0) {
      $('.cart-popup').fadeOut();
    } else {
      showCartPopup();
    }
    return false;
  });

  // setup behavior for removing an item from the cart via the cart popup remove button
  $(document).on('click', 'a.remove-item', function(e) {
  
    $(this).parents('li:first').find('.remove-item-checkbox input').attr('checked', 'checked');
    
    var $cartForm = $('#cart-popup-form');

    $cartForm.submit();
  });

  // if click not on cart-popup, hide cart-popup
  $(window).click(function(e) {    
    if ($(e.target).parents('.cart-popup').length === 0) {
      $('.cart-popup').fadeOut(500).find('ul').slideUp(500);
    }
  });

  // show the cart on hover
  $('.cart_link').hover(function() {
    if ($('.cart-popup:visible').length) {

    } else {
      showCartPopup();
    }
  }, function(){});

  var resizeLoginBg = function() {

    // footer was reverting to position:relative, causing the vertical scroll bar to appear on the page
    $('.footer').css('position', 'fixed')

    var minWidth = parseInt($('.login_old').css('min-width'));
    var minHeight = parseInt($('.login_old').css('min-height'));
    var $img = $('.login-bg');

    if ($(window).height() > minHeight && $(window).width() > minWidth) {

      var leftPos = ($(window).width() / 2) - ($img.width() / 2);
      if (isAppleDevice()) {
        leftPos = 200;
      }

      $img.css({
        height:  $(window).height(),
        left: leftPos, //($(window).width() / 2) - ($img.width() / 2),
        'vertical-align': 'bottom'
      });
    } else {      
      $img.css({
        height: ($(window).height() < minHeight ? (minHeight + 'px') : $(window).height()),
        left: "200px",
        right: '90px',      
        'vertical-align': 'bottom'
      });      
    }
  }

  // for login page only
  if ($('.login_old').length > 0) {
    $(window).load(function() {

      $('.footer').css('position', 'fixed');
      
      var $img = $('.login-bg');
      var minWidth = parseInt($('.login').css('min-width'));
      var minHeight = parseInt($('.login').css('min-height'));
      if ($(window).width() < minWidth) {
        $img.css({
          height: ($(window).height() < minHeight ? (minHeight + 'px') : $(window).height()),
          left: '200px',
          right: '90px',
          'vertical-align': 'bottom'
        })        
      } else {
        $img.css({
          height: ($(window).height() < minHeight ? (minHeight + 'px') : $(window).height()),
          left: ($(window).width() / 2) - ($img.width() / 2),
          'vertical-align': 'bottom'
        })        
      }

      $('.login-bg').fadeIn(200);
    });
    $(window).resize( resizeLoginBg );    
  }
});
// -- end filtering ----------
// ---------------------------

function cart_quantity_to_dropdown(){
  $('.cart_quantity_input_wrapper').each(function(){
    var input = $(this).children('input');
    if(input.length > 0){
      var selectbox_id = input.attr("id");
      var val = input.val();
      var available_quantity = input.attr("data_available_quantity");
      var price = input.attr("data_price");
      var selectbox = $("<select data_price=\""+price+"\"></select>").attr("id",input.attr("id")).attr("name", input.attr("name")).addClass("cart_item_quantity_dropdown")
      input.remove();
      $(this).append(selectbox);
      for(i=1;i<parseInt(available_quantity)+1;i++){
        selectbox.append($("<option></option>").attr("value",i).text(i));
      }
      selectbox.val(val);  
    }
  })
  $('.cart_remove_action_wrapper').each(function(){
    $(this).children('input').hide();
    $(this).children('.remove').html("<a href=\"#\" class=\"cart_item_remove_link\"><img src=\"/assets/cart_remove_button.png\"/><span>Remove</span></a>")
  })
  $(".cart_buttons .update_cart").toggle();
  $(".cart_buttons .continue_shopping").toggle();
  $("#cart_form").submitWithAjax({"success":function(data){
    var item_ids = [];
    var subtotal = 0;
    for(var i=0;i<data.length;i++){
      item_ids.push(data[i].id);
    }
    $(".item_entry").each(function(){
      var to_hide = true;
      for(var i=0;i<item_ids.length;i++){
        if(item_ids[i] == $(this).data("item-id")) to_hide = false;
      }
      if(to_hide){
         $(this).slideUp("slow");
      }else{
        var item_price = $(this).find('.cart_item_quantity_dropdown').attr("data_price");
        var item_qty = parseInt($(this).find('.cart_item_quantity_dropdown').val());
        item_price = parseFloat(item_price.match(/\d+/)[0]);

        subtotal += item_price*item_qty;

        $(this).children('.cart_item_total').html("$"+(item_price*item_qty).formatMoney(2, '.', ','));        
      }
    });
    $('.cart .cart_subtotal').html("$"+(subtotal).formatMoney(2, '.', ','));
  }});
}

$('.cart_item_remove_link').live("click", function(e){
  e.preventDefault();
  $(this).closest('td').children('input[type="checkbox"]').attr("checked", "checked");
  $("#cart_form").submit();
})

$('.cart_item_quantity_dropdown').live("change", function(){
  $("#cart_form").submit();
})

 $('.blink').live("focus", function(){
  if($(this).val() == $(this).attr("title")) $(this).val("").addClass('selected');
 }).live("blur", function(){
  if($(this).val() == "") $(this).val($(this).attr("title")).removeClass('selected');
 });
 
 $('.login .login_form #tmp_user_password').live("focus", function(){
  $(this).hide();
  $('.login .login_form #user_password').show().focus();
 });
 $('.login .login_form #user_password').live("blur", function(){
  if($(this).val() == ""){
    $('.login .login_form #user_password').hide();
    $('.login .login_form #tmp_user_password').show();
  }
 });

 $('#login-popup #tmp_user_password').live("focus", function(){
  $(this).hide();
  $('#login-popup #user_password').show().focus();
 });
 $('#login-popup #user_password').live("blur", function(){
  if($(this).val() == ""){
    $('#login-popup #user_password').hide();
    $('#login-popup #tmp_user_password').show();
  }
 });


$(".alert_messages_button").live("click", function(e){
  e.preventDefault();
  $(".alert_messages").slideUp("slow");
  if($('.login_subheader')[0]){
    $('.login_subheader').css({'top':''});
  }
});

$(".designer_nav").live("click", function(e){
  e.preventDefault();
  $("."+$(this).data("action")).prependTo(".designer_boutique_content");
})

// dynamic footer positioning
$(window).bind("load", function() { 

  positionFooter();

  $(window)
    .scroll(positionFooter)
    .resize(positionFooter);
});

function positionFooter() {
  // if window height > document height, stick footer to page bottom
  // else (window not big enough for document), display footer immediately after content
  var footerHeight = 0,
      footerTop = 0,
      $footer = $(".footer"),
      documentHeight = $('.shell').height(),
      windowHeight = $(window).height();

  // console.log('documentHeight', documentHeight);
  // console.log('windowHeight', windowHeight);

  if (windowHeight > documentHeight + $footer.outerHeight()) {
    // console.log('window height > documentHeight => stick footer to BOTTOM of page')
    $footer.css({
      position: 'fixed',
    });
  } else {
    // console.log('window too small for document => footer should immediately follow content')
    $footer.css({
      position: 'relative',      
    });
  }
}


var alertFallback = true;
if (typeof console === "undefined" || typeof console.log === "undefined") {
 console = {};
 if (alertFallback) {
     console.log = function(msg) {
          alert(msg);
     };
 } else {
     console.log = function() {};
 }
}

// color form selects
$(function() {
  $('#contact_country_id, .black-select select, select#contact_discover_product').selectBox();

});

function isAppleDevice() {
    return (
        (navigator.userAgent.toLowerCase().indexOf("ipad") > -1) ||
        (navigator.userAgent.toLowerCase().indexOf("iphone") > -1) ||
        (navigator.userAgent.toLowerCase().indexOf("ipod") > -1)
    );
}
var PARAMS = String(window.location).split("?").length > 0 ? String(window.location).split("?")[1] : "";;

var INFINITE_SCROLLING = {
  currentPage:1,
  loaderDivReference: "",
  checkScroll: function() {
    if (INFINITE_SCROLLING.nearBottomOfPage()) {
      INFINITE_SCROLLING.currentPage++;
      if(INFINITE_SCROLLING.loaderDivReference != ""){
        $(INFINITE_SCROLLING.loaderDivReference).html('<img src="/assets/ajax-loader.gif" alt="Loading more content"/>');
      }

      var loc = String(window.location).split("?")[0];
      $.get(loc+'.js?infinite_scrolling=1&page=' + INFINITE_SCROLLING.currentPage+ '&' + PARAMS, function(js){
        //console.log(js);
      });

    } else {
      setTimeout("INFINITE_SCROLLING.checkScroll()", 250);
    }
  },
  nearBottomOfPage: function() {
    return INFINITE_SCROLLING.scrollDistanceFromBottom() < 150;
  },
  scrollDistanceFromBottom: function(argument) {
    var scrollHeight = INFINITE_SCROLLING.pageHeight();
    return scrollHeight - (window.pageYOffset + self.innerHeight + (scrollHeight*0.24));
  },
  pageHeight: function() {
    return Math.max(document.body.scrollHeight, document.body.offsetHeight);
  },
  setup: function(div_ref){
    if(typeof div_ref !== "undefined"){
      INFINITE_SCROLLING.loaderDivReference = div_ref;
    }
    INFINITE_SCROLLING.checkScroll();
  }
}


  // $(window).scroll(function(){
  //   console.log('wtf');
  //   INFINITE_SCROLLING.checkScroll();

  // });


var SEE_ALL = {
  restrictive_area: "",
  is_set: false,
  setup: function(restrictive_area){
    if(typeof restrictive_area !== "undefined"){
      SEE_ALL.restrictive_area = restrictive_area;
    }
    if($(SEE_ALL.restrictive_area+" #pagination").length > 0){
      SEE_ALL.is_set = true;
      var li_html = $(SEE_ALL.restrictive_area+" #pagination ul li").last().clone().removeClass("current").removeClass("active");
      $(SEE_ALL.restrictive_area+" #pagination ul li").last().removeClass("last")
      $(SEE_ALL.restrictive_area+" #pagination ul").append(li_html);
      //$(SEE_ALL.restrictive_area+" #pagination ul li").last().find("a").text("View all").attr("href", $("#pagination ul li").last().find("a").attr("href").split("?")[0] + "?page=all");
      $(SEE_ALL.restrictive_area+" #pagination ul li").last().find("a").text("View all").attr("href", $("#pagination ul li").last().find("a").attr("href").replace(/(page=)[^\&]+/, '$1' + "all"));

    }    
  },
  select: function(){
    $(SEE_ALL.restrictive_area+" #pagination ul li.current.active").removeClass("current").removeClass("active");
    $(SEE_ALL.restrictive_area+" #pagination ul li.last").addClass("current").addClass("active");
  },
  reset: function(){
    if(SEE_ALL.is_set){
      SEE_ALL.setup();
    }
  }
}
function invitiationFormAjax(){
  $('#new_invitation').submitWithAjax({
    success: function(data){ 
      if(data.success){ 
        parent.$.fancybox.close(); 
      }else{
        $('#new_invitation .errors').text(processEmailErrors(data));
      }

  }}
  );
}
function setupHomePage(){
  $("body .shell").css("width", "auto").css("margin", "auto");
  $("body").resize();
}


function processEmailErrors(data){
  errors = "";
  if(data.errors){
    errors += data.errors.email ? "Email " + data.errors.email + "\n" : "";
    errors += data.errors.base ?  data.errors.base + "\n" : "";
  }else if(data.message){
    errors += data.message + "\n";
  }else{
    errors += "There was an error processing your request." + "\n";
  }     
  return errors;
}