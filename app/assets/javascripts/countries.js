// = require jquery.cssmap.js 
$(function(){

	var scrollJspaneTop = function() {
		$('.jspPane').css('top', '0');
	};
		
	var showAllCountries = function() {
		scrollCountriesTop();
		$('#countries-list').find('li').show();
	};

	var showCountriesInRegion = function(region_id) {
		var $country_list = $('#countries-list');
		$country_list.find('a').closest('li').hide();
		var shown_count = $country_list.find('a[data-region-id='+region_id+']').closest('li').show().length;

		if (shown_count == 0) {
			$('.no-countries-yet').show();	
			$('#country-selector h4').hide();
		} else {
			$('.no-countries-yet').hide();
			$('#country-selector h4').show();
		}		
	}

	var selectRegion = function(region_id) {				
		$('.active-region').removeClass('active-region');
		var $selectMe = $('.continents.css-map li[data-region-id='+region_id+']');
		$selectMe.addClass('active-region');		
	};

  // simulate map region hover
  var mouseoverRegion = function(region_id) {
    $('.active-region').removeClass('active-region');
    var $hoverMe = $('.continents.css-map li[data-region-id='+region_id+']');
    $hoverMe.mouseover();
  }
  // simulate map region hover
  var mouseoutRegion = function(region_id) {
    // $('.active-region').removeClass('active-region');
    var $leaveMe = $('.continents.css-map li[data-region-id='+region_id+']');
    $leaveMe.mouseout();
  }

	var showCountrySelector = function() {

		$('#region-selector').stop(true, true).hide(0);

		$('#country-selector').stop(true, true).fadeIn(function() {
			$('.scroll-pane').jScrollPane({
				showArrows: true
			});
		});
	};

	var showRegionSelector = function() {
		
		$('#country-selector').stop(true, true).hide(0);
		
		$('#region-selector').stop(true, true).fadeIn(function() {
			$('.scroll-pane').jScrollPane({
				showArrows: true
			});
		});
	};

	$('.region-selector').click(function() {
		var region_id = $(this).data('region-id');		

		// update the region style to be active
		selectRegion(region_id);

		// update the list to now contain a list of countries in the given region
		showCountriesInRegion(region_id);

		// hide the region selector and show the country selector
		showCountrySelector();

		return false;
	});

	// on hover of region links map regions should appear hovered
	$('.region-selector').hover(function() {		
    mouseoverRegion($(this).data('region-id'));
	}, function() {		
    mouseoutRegion($(this).data('region-id'));
	});

	// require because cssmap provides on hover callback, but no on mouseleave
	$('.bg').live('mouseleave', function() {				
		$('a.region-selector').css('color', 'white');		
	});

	$('#map-continents').click(function(e) {

		// unless they're activating a region, re-display all countries
		if ($(e.target).parents('li').hasClass('active-region')) {			
			return true
		} else {
			$('.active-region').removeClass('active-region');
			// showAllCountries();	
			showRegionSelector();
		}		
	});

  $('#map-continents').cssMap({
	  size: '650',
	  onClick: function(e) {

	  	// show the region country selector
	  	showCountriesInRegion(e.data('region-id'));

	  	//show the country selector
	  	showCountrySelector();

	  	scrollJspaneTop();
	  },
	  onHover: function(e) {
	  	// highlight the region in list of region links
	  	$('a.region-selector').css('color', 'white');
	  	$('a.region-selector[data-region-id=' + e.data('region-id') + ']').css('color', '#9bbbe4');
	  },
	  tooltips: false
	});

    //$('.edit_country').submitWithAjax({
      //success: function(data) { 
        //$('.loading').remove();
      //},
    //});

});


$('.admin_country_shipping_checkbox').live("change", function(e){
	e.preventDefault();
	$(this).after('<span class="loading">Saving</spam>');
	$(this).closest('form').submit();
})
