# $(function() {  
#   $('#editor-picks-slider li').hover(
#       function() {
#         $(this).find('.product-name').stop(true, true).slideDown('fast');
#       },
#       function() {
#         $(this).find('.product-name').stop(true, true).slideUp('fast');       
#       }
#     );  
# });

# this does the same as the preceding js
$ ->  
  mouseOn  = -> $('.product-name', this).stop(true, true).slideDown('fast')   
  mouseOff = -> $('.product-name', this).stop(true, true).slideUp('fast') 
  $('#editor-picks-slider li').hover mouseOn, mouseOff

  mouseOn  = -> $('.designer-name', this).stop(true, true).slideDown('fast')   
  mouseOff = -> $('.designer-name', this).stop(true, true).slideUp('fast') 
  $('#as-seen-on-slider li').hover mouseOn, mouseOff  