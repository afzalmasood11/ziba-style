$('.filters form .categories ul li input[type="checkbox"]').live('change', function(e){ e.preventDefault(); $(this).closest('form').submit(); })
$('.filters form .dates .sales_report_filter').live('change', function(e){ e.preventDefault(); $(this).closest('form').submit(); })
$('.designer_country_id_select').live("change", function(){ adapt_boutique_form($(this).val); })

var designers_template;
$(function() {
  designers_template = Handlebars.compile($("#designers-template").html());

  $('a.alpha').on('click', function(e) {
    e.preventDefault();
    //set 'letter' hidden form value
    $('#letter').val($(this).data('letter'));
    $('a.alpha').removeClass('active');
    $(this).addClass('active');
    $('#filterForm').submit(); 
  });

//  $('a.designer-already-has-account').click(function() {
//    // show login modal
//
//    // redirect to referrer
//    return false;
//  });
});

function adapt_boutique_form(designer_country_id){
  if(typeof usa_country_id !== "undefined"){
    if(designer_country_id == usa_country_id){ // if selected contry is USA
      $('.designer_state_id_wrapper').show();
      $('.int_tax_rate_wrapper').hide();
    }else{
      $('.designer_state_id_wrapper').hide();
      $('.int_tax_rate_wrapper').show();
    }
  }
}
