var reason_textarea;
$(function(){
  reason_textarea = $("textarea#return_reason");
  $("textarea#return_reason").remove();
  $("select#return_reason").change(function(){
    if($(this).val() == "Other") $(".field.reason").append(reason_textarea);
    else $("textarea#return_reason").remove();
  });
})
