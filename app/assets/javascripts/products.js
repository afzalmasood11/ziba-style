var processingCart = false;
$('.add-to-bag').live('click', function(e) {
  e.preventDefault();
  if ($('#cart_item_id').val() == "" || $('#cart_quantity').val() == "") {
    $('.size-quantity-warning').remove();
    $(this).after('<p style="color:red" class="size-quantity-warning">Please select size and quantity</p>');
  } else {

    // $('#cart_form').submit();
    if (!processingCart) {
      processingCart = true;
      
      $('.loading-animation').show();
      $('.add-to-bag').hide();

      $.post('/cart', $('#cart_form').serialize(), function(){
        $('.cart-popup').hide();
        $('.cart_link').click();
        processingCart = false;

        $('.loading-animation').hide();
        $('.add-to-bag').show();
      });
    }
  }
});

$('.product_index_active_checkbox').live("change", function(e){
  e.preventDefault();
  $('.loader').remove();
  $('.checked').remove();
  $(this).after('<img class="loader" src="/assets/loader.gif" alt="Loading"/>');
  $(this).addClass("current_product_index_active_checkbox")
  $(this).closest("form").submit();
});

$.fn.preload = function() {
    this.each(function(){
      $('<img/>')[0].src = this;      
    });
}

$(function() {
    
  $('.img-zm').elevateZoom({gallery:'gal1', cursor: 'pointer', galleryActiveClass: 'active', zoomWindowWidth:622, zoomWindowHeight:450, zoomWindowOffetx:22, zoomWindowOffety:-2, borderSize:1});
    $(".img-zm").bind("click", function(e) {
    var ez =   $('.img-zm').data('elevateZoom');
    $.fancybox(ez.getGalleryList());
    return false;
  });

  $('.big-image a').fancybox({});
  
  $('.size-chart-link').fancybox({
    //autoDimensions: false,
    onComplete: function() {
      var imgWidth = $('#fancybox-content img').width()
      //$('#fancybox-inner').width(imgWidth);
      //$('#fancybox-wrap').width(imgWidth);
    }
  });

  $('.product_index_active_form').submitWithAjax({success: function(result){
      $('.loader').remove();
      $('.current_product_index_active_checkbox').after('<img class="checked" src="/assets/checked.png" alt="Loading"/>');
      $('.current_product_index_active_checkbox').removeClass('current_product_index_active_checkbox');
      setTimeout("$('.checked').remove();", 2000)
  }});

  if(typeof product_size_quantity_values !== "undefined"){
    var span = $('#cart_quantity').parent().find('span')
    var selectbox = '<select id="cart_quantity" name="cart[quantity]" disabled="disabled" style="width:50px;margin-left:0px;float:right;"><option value="0">0</option></select>';
    $('#cart_quantity').remove();
    span.after(selectbox);
  }

});

$("#cart_item_id").live('change', function(e){
  e.preventDefault();
  available_quantity = product_size_quantity_values[$(this).val()];
  $('#cart_quantity').html("");
  if(typeof available_quantity !== "undefined"){
    for(i=1;i<parseInt(available_quantity)+1;i++){
      $('#cart_quantity').append($("<option></option>").attr("value",i).text(i));
    }
    document.getElementById('cart_quantity').disabled = false;
  }else{
    document.getElementById('cart_quantity').disabled = true;
  }
});

// remove no size/quantity warnings when their select elements change
$(function() {  
  $('.select-size select, .quantity select').change(function() {    
    $('.size-quantity-warning').remove();  
  });
});
