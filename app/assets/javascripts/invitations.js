$(document).ready(function() {
    $('#invitation_textarea').keyup(function() {
        var len = this.value.length;
        if (len >= 200) this.value = this.value.substring(0, 200);
        $('#invitation_textarea_limit_display').text(200 - len);
    });
});