class HomePageEntitiesController < ApplicationController
  cache_sweeper :home_page_entity_sweeper, only: [:create, :update, :destroy]


  # GET /home_page_entities
  # GET /home_page_entities.json
  def index
    @home_page_entities = HomePageEntity.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @home_page_entities }
    end
  end

  # GET /home_page_entities/1
  # GET /home_page_entities/1.json
  def show
    @home_page_entity = HomePageEntity.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @home_page_entity }
    end
  end

  # GET /home_page_entities/new
  # GET /home_page_entities/new.json
  def new
    @home_page_entity = HomePageEntity.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @home_page_entity }
    end
  end

  # GET /home_page_entities/1/edit
  def edit
    @home_page_entity = HomePageEntity.find(params[:id])
  end

  # POST /home_page_entities
  # POST /home_page_entities.json
  def create
    @home_page_entity = HomePageEntity.new(params[:home_page_entity])

    respond_to do |format|
      if @home_page_entity.save
        format.html { redirect_to admin_home_page_index_url, notice: 'Home page entity was successfully created.' }
        format.json { render json: @home_page_entity, status: :created, location: @home_page_entity }
      else
        format.html { render action: "new" }
        format.json { render json: @home_page_entity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /home_page_entities/1
  # PUT /home_page_entities/1.json
  def update
    @home_page_entity = HomePageEntity.find(params[:id])

    respond_to do |format|
      if @home_page_entity.update_attributes(params[:home_page_entity])
        format.html { redirect_to admin_home_page_index_url, notice: 'Home page entity was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @home_page_entity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /home_page_entities/1
  # DELETE /home_page_entities/1.json
  def destroy
    @home_page_entity = HomePageEntity.find(params[:id])
    @home_page_entity.destroy

    respond_to do |format|
      format.html { redirect_to admin_home_page_index_url }
      format.json { head :no_content }
    end
  end
end
