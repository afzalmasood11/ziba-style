class ErrorsController < ApplicationController
  
  def error_404
    respond_to do |format|
      format.html { render :error_404, layout: false}
    end
  end
end
