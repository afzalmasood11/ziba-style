require 'oembed'

class DesignersController < ApplicationController
  
  layout "dashboard_designers", except: [:index, :show, :style_directory]
  before_filter :ensure_confirmed_designer, only: [:terms_of_service, :boutique, :update, :settings, :videos]
  before_filter :ensure_user_is_admin, only: [:upload_banner, :detailed_view]

  cache_sweeper :designer_sweeper, only: [:update, :upload_banner]

  def index
    params[:status] = nil # dont allow status change via params
    params[:designer_active] = true
    params[:with_products] = true

    @designers = User.designers(params).includes(:designer_country).uniq.page(params[:page]).per(20)
    
    logger.debug "designers ..."
    logger.debug @designers

    @banner = FileAttachment.find_by_attachment_type(FileAttachment::TYPE[:designer_index])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @designers.to_json(methods: :profile_pic_url, include: :designer_country) }
      format.js
    end
  end

  def show
    @designer = User.find(params[:id]) || not_found

    #security - make sure the designer is approved or that the current_user is the designer or that current_user is an admin
    not_found unless (@designer.designer_status == User::STATUS[:approved] and @designer.designer_active == true) or @designer.id == current_user.id or current_user.is_admin?
    flash[:notice] = "Your boutique has not been approved yet.".html_safe unless (@designer.designer_status == User::STATUS[:approved] and @designer.designer_active == true) or current_user.is_admin?
  
    # grab the embed html from youtube
    @designer.designer_videos.each do |video|
      video.oembed_resource = OEmbed::Providers::Youtube.get(video.url)
      # hacky way to force youtube oembed over https -- but does it work?
      video.oembed_resource.html.gsub! 'http:', 'https:'
    end
    
    @infinite_scrolling = params[:infinite_scrolling].present?
    @view_all = ((params[:page] == "all") or @infinite_scrolling)
    
#    @products = @designer.products.best_seller
#    @products = @products.page(params[:page]).per(9)
    @products = @designer.products.best_seller.page(params[:page]).per(9)
    @products = filter_products(@products, params) if current_user
    
    @params = params
    @params.delete("page")
    @params.delete("infinite_scrolling")
    respond_to do |format|
      format.html { render generate_view_name }
      format.json { render json: @designer }
      format.js { render 'shared/filter_results'}
    end
  end

  def boutique
    #initialize pictures if not exist yet
    @designer = (current_user_is_admin? and params.include? :id) ? User.find(params[:id]) : current_user
    @designer.build_banner_pic  if @designer.banner_pic.nil?
    @designer.build_profile_pic if @designer.profile_pic.nil?
    @disable = (!current_user_is_admin? and @designer.designer_status > User::STATUS[:authorized]) ? true : false

    if  admin_as_designer?
      @disable = false
    end

    @designer.designer_tax_rate = @designer.designer_tax_rate ? @designer.designer_tax_rate*100 : 0 #convert tax rate to percentage
    @designer.designer_int_tax_rate = @designer.designer_int_tax_rate ? @designer.designer_int_tax_rate*100 : 0 #convert int tax rate to percentage
    @designer_videos = DesignerVideo.where(designer_id: current_user.id)
  end

  def settings
    @designer = (current_user_is_admin? and params.include? :id) ? User.find(params[:id]) : current_user
  end

  def videos
    @designer = (current_user_is_admin? and params.include? :id) ? User.find(params[:id]) : current_user
    @designer_videos = DesignerVideo.where(designer_id: current_user.id)
  end


  def detailed_view
    @designer = User.find(params[:id])
    not_found unless @designer.role == User::ROLE[:designer]
    render :detailed_view, layout: "dashboard_admin"
  end

  def style_directory
    @countries = Country.with_designers(true) #true to included the active_designers
    @featured_categories = Category.featured_in_style_directory
  end

  def sales_report
    conditions = {designer_id: current_user.id}
    conditions['orders.status'] = Order::COMPLETED_STATUS
    conditions['product_categories.category_id'] = Category.find_all_by_id(params[:filters][:category_id]).collect{|c| c.subcategories }.flatten.collect(&:id) if params.include? :filters and params[:filters].include? :category_id
    @filters = params[:filters] if params.include? :filters
    @designer_orders = DesignerOrder.joins(:order, order_items: {item: {product: :product_categories}}).where(conditions).order(:created_at)    
    @designer_orders = @designer_orders.where{products.id == my{params[:filters][:product_id]}} if params.include? :filters and params[:filters].include? :product_id and params[:filters][:product_id] != "0"
    @date_filter_type = (params.include? :filters and params[:filters].include? :date_type) ? params[:filters][:date_type] : "month"
    @products = @designer_orders.collect{|d_o| d_o.products }.flatten.uniq
    @product_id = (params.include? :filters and params[:filters].include? :product_id) ? params[:filters][:product_id] : 0
    unless @designer_orders.empty?
      @start_date = @designer_orders.first.created_at.to_date
      @end_date = @designer_orders.last.created_at.to_date
      case @date_filter_type
        when "month"
          @step = 1.month
          @format = "%B %Y"
          @start_date = @start_date.beginning_of_month
        when "day"            
          @step = 1.day
          @format = "%B %d %Y"
        when "year"            
          @step = 1.year
          @format = "%Y"       
          @start_date = @start_date.beginning_of_year   
        else #default to "month"
          @step = 1.month
          @format = "%B %Y"    
      end      
    else
      @no_result = true
    end
  end

  def activate
    @designer = current_user
  end

  def deactivate_boutique_request
    notify_admin("new_message_request_deactivate_boutique", current_user)
    redirect_to boutique_designers_path, notice: 'Profile modification request sent.'
  end
  
  def update
    respond_to do |format|
      @designer = current_user_is_admin? ? User.find(params[:id]) : current_user
      #conver designer_tax_rate from percentage to decimal
      params[:user][:designer_tax_rate] = params[:user][:designer_tax_rate].to_f/100 if params[:user].include? :designer_tax_rate
      params[:user][:designer_int_tax_rate] = params[:user][:designer_int_tax_rate].to_f/100 if params[:user].include? :designer_int_tax_rate
      #handle special type of update - designer_status field MUST NOT be updated through a form (too risky)
      if params[:user].include? :designer_request_approval and @designer.designer_status == User::STATUS[:authorized]
        # if true or (params[:user].include? :terms_of_service and params[:user][:terms_of_service] == "1") #do not need to agree to terms of service anymore
          @designer.designer_status = User::STATUS[:pending_approval]
          notify_admin("new_boutique_designer_to_approve", @designer) if @designer.save
          format.html { redirect_to boutique_designers_path, notice: 'Your designer profile is now pending for approval by an admin.' }
          format.json { head :no_content }
        # else
        #   @designer.build_banner_pic  if @designer.banner_pic.nil?
        #   @designer.build_profile_pic if @designer.profile_pic.nil?
        #   @designer.errors.add :terms_of_service, "you need to agree to the terms of service"
        #   format.html { render action: "boutique" }
        #   format.json { render json: @designer.errors, status: :unprocessable_entity }
        # end
      else
        #SECURITY: ensure that if the designer is approved, the only thing he can update is the status of his boutique

        if admin_as_designer? or !(@designer.designer_status == User::STATUS[:approved] and ((!params[:user].include?(:designer_active) and params[:user].count == 1) or params[:user].count > 1))

          params[:user][:designer_active] = params[:user][:designer_active].to_i if params[:user].include? :designer_active

          if @designer.update_attributes(params[:user])
            #Notify admins if user if activating/inactivating his/her boutique
            if params[:user].include? :designer_active
              notify_admin(params[:user][:designer_active] == 'true' ? 'new_message_request_activate_boutique' : 'new_message_request_deactivate_boutique', @designer)
            end
            
            format.html { redirect_to request.referrer, notice: 'Your designer profile has been successfully updated.' }
            format.json { head :no_content }
          else
            @designer.build_banner_pic  if @designer.banner_pic.nil?
            @designer.build_profile_pic if @designer.profile_pic.nil?
            @designer.designer_tax_rate = @designer.designer_tax_rate ? @designer.designer_tax_rate*100 : 0 #convert tax rate to percentage
            @designer.designer_int_tax_rate = @designer.designer_int_tax_rate ? @designer.designer_int_tax_rate*100 : 0 #convert int tax rate to percentage
            format.html { render action: "boutique" }
            format.json { render json: @designer.errors, status: :unprocessable_entity }
          end
        else
          format.html { redirect_to request.referrer }
          format.json { head :no_content }
        end
      end
    end
  end

  def become_designer
    unless current_user 
      redirect_to designer_sign_up_path
      return
    end

    if current_user.is_designer?
      redirect_to root_path, alert: 'Your account is already a designer account'
      return
    end

    render 'devise/registrations/new_designer', layout: 'application'
  end

  # allows a regular user to upgrade his/her account to become a designer
  def switch_to_designer
    unless current_user.present?
      redirect_to designer_sign_up_path
      return
    end

    if current_user.is_designer?
      redirect_to root_path, alert: 'Your account is already a designer account'
      return
    end
  
    old_role = current_user.role
    current_user.role = User::ROLE[:designer]
    current_user.designer_status = current_user_is_admin? ? User::STATUS[:approved] : User::STATUS[:pending_promotion_to_designer]

    # these fields are hidden on the minimalistic version of the "become designer" form
    current_user.designer_country_id = current_user.country_id
    
    current_user.designer_city          = params['user']['designer_city']
    current_user.designer_name          = params['user']['designer_name']
    current_user.designer_biography     = params['user']['designer_biography']
    current_user.designer_portfolio_url = params['user']['designer_portfolio_url']
    current_user.designer_country_id    = params['user']['designer_country_id']

    if current_user.save
      notify_admin("new_designer_signup", current_user)
      redirect_to root_path, notice: 'Your designer account is now pending approval!'
    else
      logger.debug current_user.errors.full_messages
      logger.debug '^^^^'
      current_user.role = old_role
      render 'devise/registrations/new_designer', layout: 'application'
    end
  end

  def upload_banner
    puts "\n\n\n\n......\n\n\n" 
    @designer = User.find params[:id]
    
    @designer.build_banner_pic unless @designer.banner_pic.present?


    if request.put?
      if @designer.update_attributes(params[:user])
        redirect_to designer_path(@designer), notice: 'banner updated'
      else
        render "upload_banner", layout: "dashboard_admin"
      end 
    else
      render "upload_banner", layout: "dashboard_admin"
    end
  end

  def upload_designers_page_banner
    @banner = FileAttachment.find_or_create_by_attachment_type(FileAttachment::TYPE[:designer_index])
    #binding.pry

    if request.put?
      logger.debug "method == put"
      if @banner.update_attributes(params[:file_attachment])
        redirect_to designers_path
      else
        render "upload_designers_page_banner", layout: "dashboard_admin"
      end
    else
      render "upload_designers_page_banner", layout: "dashboard_admin"
    end

  end

  def accepted
    user = User.find_by_authentication_token(params[:authentication_token])
    if user
      sign_out current_user if current_user.present?
      sign_in user
      redirect_to designer_accepted_registration_url(params[:authentication_token])
    end
  end
end
