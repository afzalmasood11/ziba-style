class CategoriesController < ApplicationController

  before_filter :ensure_user_is_admin, except: :show
  cache_sweeper :category_sweeper, only: [:create, :update, :destroy]

  def show
    gender_filter = is_numeric?(session_gender) ? session_gender : Category::GENDER_TYPES[session_gender]
    @category = Category.where("categories.slug= ? or categories.id=?", params[:id], params[:id]).first || not_found
    @parent_category = @category
    if params.has_key? :category_id
      @category = @category.subcategories.where("categories.slug = ? or categories.id = ?", params[:category_id], params[:category_id]).first || not_found
    end
    
    # not_found unless @category #security

    unless @category.category_id.blank? || params.has_key?(:category_id)
        @category_ids = @category.id
        #redirect_to category_path [@category.category.slug, @category.slug]        
    else
      if @category.category_id.nil?
        @category_ids = @category.subcategories.collect(&:id)
      else
        params[:category_id] = @category.id
        @category_ids = [@category.id]
      end
    end

#    @products = Product.best_seller.filter_by_category(@category_ids).page(params[:page]).per(9)

    # Filter products by gender before setting @designers
    # so the list of designer filters only contains designers for the browsing gender
#    @products  = @products.filter_by_gender(gender_filter)

    #combined the above two queries:
#    @products = Product.best_seller.filter_by_category_and_gender(@category_ids, gender_filter).page(params[:page]).per(9)
    @products = Product.best_seller.filter_by_category(@category_ids)

#    @designers = User.where{id.in(my{Product.best_seller.filter_by_category(@category_ids).collect{ |p| p.designer_id }.flatten.uniq})}
    @designers = User.where{id.in(my{@products.collect{ |p| p.designer_id }.flatten.uniq})}
#    @designers = @products.collect{ |pro| pro.designer }.flatten.uniq

    @countries = Country.find_all_by_id(@designers.collect{|d| d.designer_country_id }.flatten.uniq)
    @products  = @products.filter_by_gender(gender_filter).page(params[:page]).per(9)
    # completely filter products after setting @designers, so that designers for
    # products that get filtered out are still shown

    @products = filter_products(@products, params)
    get_sizes

    @infinite_scrolling = params[:infinite_scrolling].present?
    @params = params
    @params.delete("page")
    @params.delete("infinite_scrolling")
    respond_to do |format|
        red = @category.category_id.present? ? category_path([@category.category.slug, @category.slug]) : category_path(@category)
        format.html { red } # show.html.erb
        format.json { render json: @category }
        format.js { render 'shared/filter_results'}
    end

  end

#  def show
#
#    gender_filter = is_numeric?(session_gender) ? session_gender : Category::GENDER_TYPES[session_gender]
#    @category = Category.where("categories.slug= ? or categories.id=?", params[:id], params[:id]).includes(:categories).first || not_found
#    if params.has_key?(:category_id)
#      @category = @category.subcategories.where("categories.slug= ? or categories.id=?", params[:category_id], params[:category_id]).first
#    end
#
#    #@category = Category.where("categories.slug= ? or categories.id=?", params[:id], params[:id]).first || not_found
#
#    # where{(categories.id == my{params[:id]}) & (subcategories.gender_type.in([ Category::GENDER_TYPES[:both], gender_filter ]))}.includes(:subcategories).first
#    # not_found unless @category #security
#
#    unless @category.category_id.blank?
#      puts ":::::::::::::::::::::::::::::::::::::::::;"
#      redirect_to category_path(@category.category_id, category_ids: [params[:id]])
#      #redirect_to category_path([@category.category_id, params[:id]])
#    else
#      @category_ids = params[:category_ids].present? ? params[:category_ids] : @category.subcategories.collect(&:id)
#      @products = Product.best_seller.filter_by_category(@category_ids).page(params[:page]).per(9)
#
#      # Filter products by gender before setting @designers
#      # so the list of designer filters only contains designers for the browsing gender
#      @products  = @products.filter_by_gender(gender_filter)
#      @designers = User.where{id.in(my{Product.best_seller.filter_by_category(@category_ids).collect{ |p| p.designer_id }.flatten.uniq})}
#      @countries = Country.find_all_by_id(@designers.collect{|d| d.designer_country_id }.flatten.uniq)
#
#      # completely filter products after setting @designers, so that designers for
#      # products that get filtered out are still shown
#      @products = filter_products(@products, params)
#      get_sizes
#
#      #useless - PH - 10/08/2012
#      #User.designers(category_ids: category_ids).uniq
#      @infinite_scrolling = params[:infinite_scrolling].present?
#
#      @params = params
#      @params.delete("page")
#      @params.delete("infinite_scrolling")
#      respond_to do |format|
#          format.html # show.html.erb
#          format.json { render json: @category }
#          format.js { render 'shared/filter_results'}
#      end
#
#    end
#
#  end

  # ADMIN ONLY

  def index
    @categories = Category.all

    respond_to do |format|
      format.html { render :index, layout: "dashboard_admin" } # index.html.erb 
      format.json { render json: @categories }
    end
  end


  def new
    @category = Category.new

    respond_to do |format|
      format.html { render :new, layout: "dashboard_admin" } # new.html.erb
      format.json { render json: @category }
    end
  end

  # GET /categories/1/edit
  def edit
    @category = Category.find(params[:id])
    render :edit, layout: "dashboard_admin"
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(params[:category])


    respond_to do |format|
      if @category.save
        redirect = @category.category_id.present? ? category_path([Category.find(@category.category_id), @category]) : category_path(@category)
        #format.html { redirect_to @category, notice: 'Category was successfully created.' }
        format.html { redirect_to redirect, notice: 'Category was successfully created.' }
        format.json { render json: @category, status: :created, location: @category }
      else
        format.html { render action: "new", layout: "dashboard_admin" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /categories/1
  # PUT /categories/1.json
  def update
    @category = Category.find(params[:id])

    respond_to do |format|
      if @category.update_attributes(params[:category])
        redirect = params[:category][:homepage_image].present? ? admin_home_page_index_url : (@category.category_id.present? ? category_path([Category.find(@category.category_id), @category]) : category_path(@category))
        format.html { redirect_to redirect, notice: 'Category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit", layout: "dashboard_admin" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category = Category.find(params[:id])
    @category.destroy

    respond_to do |format|
      format.html { redirect_to categories_url }
      format.json { head :no_content }
    end
  end

end
