class Users::ConfirmationsController < Devise::ConfirmationsController

  #method called to confirm email
  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])

    # designer don't login after creating an account - the admin is notified instead that he needs to approve the account
    role = resource.role
    designer_status = resource.designer_status

    if resource.errors.empty?
      unless role == 2 and designer_status == 0
        set_flash_message(:notice, :confirmed) if is_navigational_format?
        sign_in(resource_name, resource)
        respond_with_navigational(resource){ redirect_to after_confirmation_path_for(resource_name, resource) }
      else
        signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
        redirect_to root_path, notice: "Your email is now verified. An admin will approve your account shortly"
      end
    else
      respond_with_navigational(resource.errors, :status => :unprocessable_entity){ render :new }
    end
  end

  protected

    def after_confirmation_path_for(resource_name, resource)
      after_sign_in_path_for(resource)
    end

end