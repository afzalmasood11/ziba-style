class Users::RegistrationsController < Devise::RegistrationsController

  layout "dashboard_users", only: [:edit, :update] 
  def new
    resource = build_resource({})

    if params.include? :invitation_token
      resource.invitation_token = params[:invitation_token] 
      sign_out current_user if current_user.present?
    end

    resource.email = params[:email] if params.include? :email
    session[:role] = params[:role] #we store the role param in the session cause this MUST NOT be on the form
    resource.portfolio_docs.build if params[:role] == User::ROLE[:designer]
    current_user.portfolio_docs.build if params[:role] == User::ROLE[:designer] and current_user
    render params[:role] == User::ROLE[:designer] ? '/devise/registrations/new_designer' : '/devise/registrations/new_customer', layout: params[:role] == User::ROLE[:designer] ? "application" : false
  end

  def edit
    if params.include? :invitation_token 
      resource.invitation_token = params[:invitation_token] 
      sign_out current_user if current_user.present?
    end

    if params.include? :authentication_token
      render  '/devise/registrations/pending_designer', layout: false
      return
    end

    render params[:view]
  end
  
  # POST /resource
  def create

    build_resource

    resource.role = session[:role]
    resource.designer_status = current_user_is_admin? ? User::STATUS[:approved] : User::STATUS[:pending_auth] if resource.role == User::ROLE[:designer]

    if resource.role == User::ROLE[:designer]
      resource.password = SecureRandom.hex(8)
      resource.country_id = resource.designer_country_id
      resource.city = resource.designer_city
    end

    resource.confirm_invitation_email # <-- if the user email is the same as the invitation email then the email address is already confirmed - no need to send an email confirmation request

    if resource.save
      unless current_user_is_admin?
        if resource.active_for_authentication?
          set_flash_message :notice, :signed_up if is_navigational_format?
          sign_in(resource_name, resource)
          notify_customer("welcome", resource) #send welcome email to user with already approved email address
          respond_with resource, :location => after_sign_up_path_for(resource)
        else
          if resource.role == User::ROLE[:designer] and resource.designer_status == User::STATUS[:pending_auth]
            set_flash_message :notice, :signed_up_but_pending_designer if is_navigational_format?
            notify_admin("new_designer_signup", resource)
            notify_designer("pending_designer", resource)
          else
            set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
          end
          expire_session_data_after_sign_in!
          respond_with resource, :location => after_inactive_sign_up_path_for(resource)
        end
      else
        redirect_to '/' # TODO: redirect to right path
      end
    else
      clean_up_passwords resource
      resource.portfolio_docs.build if resource.portfolio_docs.empty?
      render resource.role == User::ROLE[:designer] ? '/devise/registrations/new_designer' : '/devise/registrations/new_customer', layout: params[:role] == User::ROLE[:designer] ? "application" : false
    end
  end

  # PUT /resource
  # We need to use a copy of the resource because we don't want to change
  # the current user in place.
  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    if params[resource_name][:authentication_token] and params[resource_name][:password]
      from_auth_page = true
      resource.designer_status = User::STATUS[:approved]
      params[resource_name].delete :authentication_token
    end
    if resource.update_with_password(params[resource_name])

      if is_navigational_format?
        if resource.respond_to?(:pending_reconfirmation?) && resource.pending_reconfirmation?
          flash_key = :update_needs_confirmation
        end
        set_flash_message :notice, flash_key || :updated
      end
      sign_in resource_name, resource, :bypass => true
      if from_auth_page
        respond_with resource, :location => '/account' 
      else
        respond_with resource, :location => params[:view] == "account" ? account_path : shipping_info_path #after_update_path_for(resource)
      end
    else
      clean_up_passwords resource
      if from_auth_page
        render  '/devise/registrations/pending_designer', layout: false
      else
        render params[:view] == "account"  ? '/devise/registrations/account' : '/devise/registrations/shipping'
      end
    end
  end

  def delete

  end
  
  # override devise method, so that if you're an admin you can do actions that an already logged in user normally couldn't do
  def require_no_authentication
    super if false #anyone can see this page when they're loggued in
  end

end
