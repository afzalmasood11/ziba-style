class Users::SessionsController < Devise::SessionsController

  #layout false

  # POST /resource/sign_in
  def create
    resource = warden.authenticate!(:scope => resource_name, :recall => 'users/sessions#failure')

    role = resource.role
    designer_status = resource.designer_status

    if role == 2 and designer_status == 0
      signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
      redirect_to root_path, alert: "Your account hasn't been approved yet."
    else
      #set_flash_message(:notice, :signed_in) if is_navigational_format? #no flash message needed - changing the page is enough

      # if a user signed in from the become a featured designer page, bring him back to that page after login
      if request.referrer.include? designer_sign_up_path
        session[:post_sign_in_redirect] = request.referrer
      end

      # add the users gender to the session
      session[:browsing_gender] = resource.gender

      return sign_in_and_redirect(resource_name, resource)
      #respond_with resource, :location => after_sign_in_path_for(resource)
    end
  end

  #same code minus flash message
  def destroy
    redirect_path = after_sign_out_path_for(resource_name)
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))

    # We actually need to hardcode this as Rails default responder doesn't
    # support returning empty response on GET request
    respond_to do |format|
      format.any(*navigational_formats) { redirect_to redirect_path }
      format.all do
        head :no_content
      end
    end
  end

  def failure
    return render :json => {success: false, action: "sign_in", message: "The email address or the password that you entered does not match our records."}
  end



  protected

  def sign_in_and_redirect(resource_or_scope, resource=nil)
    scope = Devise::Mapping.find_scope!(resource_or_scope)
    resource ||= resource_or_scope
    sign_in(scope, resource) unless warden.user(scope) == resource
    return render :json => {success: true, action: "sign_in", message: "Signed in successfully", redirect: stored_location_for(scope), resource: resource  || after_sign_in_path_for(resource), last_url: request.referrer}
  end


  def auth_options
    { :scope => resource_name, :recall => "#{controller_path}#new" }
  end

end
