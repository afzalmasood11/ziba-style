class Users::PasswordsController < Devise::PasswordsController  
  before_filter :pick_layout
  def pick_layout
    self.class.layout(request.xhr? ? false : "application")
  end

  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)

    if successfully_sent?(resource)
      return render :json => {success: true, action: "password_reset", message: "You should receive an email shortly."}
    else
      return render :json => {success: false, action: "password_reset", message: "There was a problem processing your request."}
    end
  end
  
  def successfully_sent?(resource)
    notice = if Devise.paranoid
      resource.errors.clear
      :send_paranoid_instructions
    elsif resource.errors.empty?
      :send_instructions
    end

    if notice
      true
    end
  end


end

