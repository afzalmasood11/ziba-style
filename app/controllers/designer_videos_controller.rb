class DesignerVideosController < ApplicationController

  before_filter :ensure_user_is_designer

  # GET /designer_videos
  # GET /designer_videos.json
  def index
    @designer_videos = DesignerVideo.where(designer_id: current_user.id)

    respond_to do |format|
      format.html {render "index", layout: "dashboard_designers"}
      format.json { render json: @designer_videos }
    end
  end

  # GET /designer_videos/new
  # GET /designer_videos/new.json
  def new
    @designer_video = DesignerVideo.new

    respond_to do |format|
      format.html {render "new", layout: "dashboard_designers"}
      format.json { render json: @designer_video }
    end
  end

  # POST /designer_videos
  # POST /designer_videos.json
  def create
    @designer_video = DesignerVideo.new(params[:designer_video])
    @designer_video.designer_id = current_user.id
    @designer_video.status = DesignerVideo::STATUS[:pending]

    respond_to do |format|
      if @designer_video.save
        format.html { redirect_to videos_designers_path, notice: 'Video was successfully created.' }
        format.json { render json: @designer_video, status: :created, location: @designer_video }
      else
        format.html { render action: "new", layout: "dashboard_designers" }
        format.json { render json: @designer_video.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /designer_videos/1
  # DELETE /designer_videos/1.json
  def destroy
    @designer_video = DesignerVideo.find(params[:id])
    @designer_video.destroy

    respond_to do |format|
      format.html { redirect_to boutique_designers_path }
      format.json { head :no_content }
    end
  end
end
