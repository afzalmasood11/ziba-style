class InvitationsController < ApplicationController

  layout "dashboard_users"
  before_filter :authenticate_user!
  before_filter :get_invitations, only: :index
  before_filter :ensure_user_can_create_invitation, only: :create

  def index
    @invitation = Invitation.new

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @invitations }
    end
  end

  def create
    existing_user = User.find_by_email(params[:invitation][:email].strip)
    if existing_user
      @invitation = Invitation.new
      @invitation.errors.add(:base, "User with email address already exists")      
    else
      @invitation = Invitation.new(params[:invitation])
      @invitation.from_user_id = current_user.id
      @invitation.token = generate_token
    end
      
    respond_to do |format|
      if existing_user.nil? && @invitation.save
        send_invitation(@invitation)
        format.html { redirect_to @invitation.product_id.blank? ? invitations_path : product_path(@invitation.product_id), notice: 'Invitation was successfully sent.' }
        format.json { render json: {invitation: @invitation, success: true}, status: :created }
      else
        format.html { render action: "index"}
        format.json { render json: {invitation: @invitation, success: false, errors: @invitation.errors } } 
      end
    end
  end

  private 

  def generate_token
    token  =  Invitation::generate_token
    while Invitation.find_by_token(token) != nil
      token = Invitation::generate_token
    end
    token
  end

  def send_invitation(invitation)
    unless invitation.product_id.blank?
      user = User.find_by_email(@invitation.email)
      action = user ? "product" : "product_invitation"
    else
      action = "invitation"
    end
    notify_send_invitation(action, invitation)
  end

  def ensure_user_can_create_invitation
    get_invitations
    unless !@invitations.collect{|i| i if i.email == params[:invitation][:email] }.empty? or @number_of_invitations_sent < current_user.invitation_limit or current_user.is_admin?
      flash[:alert] = "No more invitations available"
      redirect_to action: "index" 
    end
  end

  def get_invitations
    @invitations = Invitation.where(from_user_id: current_user.id).group(:email).order('created_at DESC')
    @number_of_invitations_sent = @invitations.all.count
  end

end