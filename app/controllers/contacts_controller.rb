class ContactsController < ApplicationController

  before_filter :ensure_user_is_admin, except: [:contact_us, :request_invitation, :engage_with_us, :create]


  #caches_page :contact_us
  def contact_us
    @contact = Contact.new
    @contact.contact_type = Contact::TYPE[:contact_us]    
    @contact.order_number = params[:order_id] if params.include? :order_id

    if user_signed_in?
      @contact.fname = current_user.fname
      @contact.lname = current_user.lname
      @contact.email = current_user.email
      @contact.phone = current_user.phone
      @contact.country_id = current_user.country_id
    end


    respond_to do |format|
      format.html { render :contact_us, layout: request.xhr? ? false : "application" }
      format.json { render json: @contact }
    end
  end

  def request_invitation
    @contact = Contact.new
    @contact.contact_type = Contact::TYPE[:request_invitation]

    respond_to do |format|
      format.html { render :request_invitation, layout: request.xhr? ? false : "application" }
      format.json { render json: @contact }
    end
  end

  def engage_with_us
    @contact = Contact.new
    @contact.contact_type = Contact::TYPE[:engage_with_us]

    respond_to do |format|
      format.html { render :engage_with_us, layout: request.xhr? ? false : "application" }
      format.json { render json: @contact }
    end
  end

  def create
    existing_user = User.find_by_email(params[:contact][:email].strip)
    if existing_user
      @contact = Contact.new
      @contact.contact_type = params[:contact][:contact_type]
      @contact.errors.add(:base, "You are already a registered ZIBA Style member")
    else  
      @contact = Contact.new(params[:contact])
    end
      
    respond_to do |format|
      if existing_user.nil? && @contact.save
        email_admin(@contact)
        format.html { redirect_to root_path, notice: "We'll get back to you as soon as possible." }
        format.json { render :json => {contact: @contact, status: :created, location: @contact, success: true } }
      else
        format.html { render action: get_action_name(@contact.contact_type) }
        format.json { render :json => {errors: @contact.errors, success: false} }
      end
    end
  end

  # ADMIN ONLY

  def index
    @contacts = Contact.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contacts }
    end
  end

  def show
    @contact = Contact.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contact }
    end
  end

  private 

  def email_admin(contact)
    case contact.contact_type
      when Contact::TYPE[:request_invitation]; action = "new_message_request_invitation"
      when Contact::TYPE[:contact_us];         action = "new_message_contact_us"
      when Contact::TYPE[:engage_with_us];     action = "new_message_engage_with_us"
    end
    notify_admin(action, contact)
  end

  def get_action_name(contact_type)
    case contact_type
      when Contact::TYPE[:request_invitation]; action = "request_invitation"
      when Contact::TYPE[:contact_us];         action = "contact_us"
      when Contact::TYPE[:engage_with_us];     action = "engage_with_us"
    end
  end

end
