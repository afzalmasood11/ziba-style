 class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :current_user_is_designer?, :current_user_is_admin?, :product_status_to_string,
                :current_user_is_browsing_mens?, :current_user_is_browsing_womens?, :session_gender,
                :gender_switch_enabled, :continue_shopping_path, :show_cart_popup_on_page?, :admin_as_designer?,
                :current_user_is_confirmed_designer?

  before_filter :update_continue_shopping_uri, :force_ssl, :designer_needs_password

   
  protected

    def after_sign_in_path_for(resource)
      # if a user signed in from the become a featured designer page, bring him back to that page after login
      if session.include? :post_sign_in_redirect
        session[:post_sign_in_redirect]
      else
        super
      end
    end

    def after_sign_out_path_for(resource_or_scope)
      "/?signed_out=true"
    end

  # convenience method
    def current_user_is_admin? 
      current_user and current_user.is_admin?
    end
   
    def current_user_is_designer? 
      current_user and current_user.is_designer?
    end

    def current_user_is_customer? 
      current_user and current_user.is_customer?
    end

    # this handles the case where an active user has applied to be a designer, this allows him to still use his accounjt
    # but without being able to performs actions that a designer would
    def current_user_is_confirmed_designer?
      return true if current_user.present? and current_user.is_admin?
      current_user and current_user.is_designer? and current_user.designer_status != User::STATUS[:pending_promotion_to_designer] and current_user.designer_status != User::STATUS[:approved_needs_password]
    end
  
    def ensure_confirmed_designer
      redirect_to '/' unless current_user_is_confirmed_designer?
    end

    def current_user_is_designer_or_admin?
      current_user_is_admin? || current_user_is_designer? 
    end

    def current_user_is_browsing_mens?
      session[:browsing_gender] == User::GENDER[:male]
    end

    def current_user_is_browsing_womens?
      session[:browsing_gender] == User::GENDER[:female]
    end

    def ensure_user_is_designer_or_admin?
      redirect_to '/' unless current_user_is_designer_or_admin?
    end

    def ensure_user_is_designer
      redirect_to '/' unless current_user_is_designer_or_admin?
    end

    def ensure_user_is_admin
      redirect_to '/' unless current_user_is_admin?
    end

    def admin_as_designer?
      session.include? :admin_id
    end

    def generate_view_name(view_name = nil)
      "#{view_name.blank? ? params[:action] : view_name}"
    end

    # returns true if a controller + action pair should allow gender-specific filtering
    # 10/15/2012 - seems like Yasmin would like the gender switch to appear on all pages
    def gender_switch_enabled(controller, action)      
      true
      # these are controller#action tuples that require explicit rules
      #disable_gender = ["products#index", "categories#index", "designers#boutique", "designers#activate"]
      #return false if disable_gender.include? "#{controller}##{action}"
      
      ## the following controllers generally should have the gender switch enabled
      ## any explicit exceptions should be handled above
      #return (%w[categories countries designers sales].include? controller)
    end

    def show_cart_popup_on_page?
      %w{/checkout /validate_checkout /shopping_bag}.none? { |url| request.fullpath.include? url }  
    end

    def filter_products(products, params)
      # -- include designer + country 
      products = products.includes({designer: :designer_country}, :primary_image)

      # filter gender - not needed on some specific pages      
      if gender_switch_enabled(params[:controller], params[:action])
        products = products.filter_by_gender(session_gender) 
      end

      %w{size country category designer}.each do |param|
        #sort of unnecessary to keep both of these ("id" and "ids") but just doing it for the hell of it, for now
        products = products.send("filter_by_#{param}", params[:"#{param}_id"])  if params[:"#{param}_id"]
        products = products.send("filter_by_#{param}", params[:"#{param}_ids"]) if params[:"#{param}_ids"]
      end
      # price filtering works differently
      if !params[:prices].blank? or !params[:price_min].blank? or !params[:price_max].blank?
        # if we used the checkboxes to select a range...
        prices = params[:prices] ? params[:prices].collect{|i| Product::PRICES[i.to_i]} : []
        prices << {min: params[:price_min], max: params[:price_max]} if(params[:price_min].present? and params[:price_max].present?)
        prices << {min: 0, max: Product::PRICE_MAX } if prices.empty?
        logger.debug(prices)
        products = products.filter_by_price_ranges(prices)
      end

      products
    end

    def product_status_to_string(product_status)
      case product_status
        when Product::STATUS[:pending];          "Editable"
        when Product::STATUS[:pending_approval]; "Pending for Approval"
        when Product::STATUS[:approved];         "On the market"
      end
    end

    def get_sizes
      @sizes = (params[:category_ids] and params[:category_ids].count == 1) ? Category.find(params[:category_ids]).first.relevant_sizes(session_gender).select([:id, :us_value]) : nil
    end

    # should put this somewhere else? or what? 
    def is_numeric?(s)
        !!Float(s) rescue false
    end

    def not_found
      raise ActiveRecord::RecordNotFound
    end

    def session_gender
      session[:browsing_gender] ||= User::GENDER[:female]
    end

    # path to last relevant shopping page, or default to apparel 
    def continue_shopping_path
      session[:continue_shopping_uri]
    end

    def update_continue_shopping_uri
      if request.referrer
        shopping_controllers = ['products', 'countries', 'categories']
        checkout_controllers = ['cart', 'checkout']

        if shopping_controllers.any? { |c| request.referrer.include? c }
          # if the user came from a shopping-related page, save this
          session[:continue_shopping_uri] = request.referrer

        elsif checkout_controllers.any? { |c| request.referrer.include? c }
          # if the referrer is a checkout-related controller, keep the existing
          # continue shopping url, if it doesn't exist: use the default
          session[:continue_shopping_uri] ||= category_path Category.first
        else
          # the refferer is set but it's not a shopping or checkout path
          # default to the first category
          session[:continue_shopping_uri] ||= category_path Category.first
        end
      else
        # if the request referrer is nil, default to the first category
        session[:continue_shopping_uri] ||= category_path Category.first
      end            
    end
    
    #NOTIFICATIONS

    def notify_admin(action, data)
      admins = User.find_all_by_role(User::ROLE[:admin])
      admins.each do |admin|
        case action
          when "new_designer_signup";                     Notifier.delay.send_admin_new_designer_signup_notification(data, admin)
          when "new_boutique_designer_to_approve";        Notifier.delay.send_admin_new_boutique_designer_to_approve_notification(data, admin)
          when "new_product_designer_to_approve";         Notifier.delay.send_admin_new_product_designer_to_approve_notification(data, admin)
          when "new_message_contact_us";                  Notifier.delay.send_admin_new_message_contact_us_notification(data, admin)
          when "new_message_engage_with_us";              Notifier.delay.send_admin_new_message_engage_with_us_notification(data, admin)
          when "new_message_request_invitation";          Notifier.delay.send_admin_new_message_request_invitation_notification(data, admin)
          when "new_message_request_deactivate_boutique"; Notifier.delay.send_admin_new_message_request_deactivate_boutique_notification(data, admin)
          when "new_message_request_activate_boutique";   Notifier.delay.send_admin_new_message_request_activate_boutique_notification(data, admin)  
          when "new_order";                               Notifier.delay.send_admin_new_order_notification(data, admin)
        end
      end
    end

    def notify_customer_and_designers(order)
      order.designer_orders.each do |designer_order|
        Notifier.delay.send_designer_new_order_notification(designer_order, designer_order.designer)
      end
      Notifier.delay.send_customer_new_order_notification(order, order.customer)
      notify_admin("new_order", order)
    end

    def notify_designer(action, entity)
      case action
        when "pending_designer"; Notifier.delay.send_designer_account_is_pending_notification(entity)
        when "authorize_designers"; Notifier.delay.send_designer_account_has_been_approved_notification(entity)
        when "approve_designers";   Notifier.delay.send_designer_boutique_has_been_approved_notification(entity)
        when "account_editable";    Notifier.delay.send_designer_account_has_become_editable(entity)
        when "approve_products";    Notifier.delay.send_designer_product_has_been_approved_notification(entity.designer, entity)
      end
    end

    def notify_customer(action, entity)
      case action
        when "welcome";    Notifier.delay.send_customer_welcome_notification(entity)
        when "new_credit"; Notifier.delay.send_customer_new_credit_notification(entity)
      end
    end

    def notify_send_invitation(action, invitation)
      case action
        when "invitation";         Notifier.delay.send_invitation(invitation) 
        when "product_invitation"; Notifier.delay.send_product_invitation(invitation) 
        when "product";            Notifier.delay.send_product(invitation) 
        else;                      Notifier.delay.send_invitation(invitation)
      end
    end

    def notify_customer_designer_order_shipped(designer_order)
      Notifier.delay.notify_customer_designer_order_shipped(designer_order)
    end

    def force_ssl
      redirect_to "https://#{request.host}#{request.fullpath}" unless env['HTTP_X_FORWARDED_PROTO'] == 'https' or Rails.env == 'development'
    end

    def designer_needs_password
      params_check = true
      params_check = false if params.present? and (params[:authentication_token] or (params[:user] and params[:user][:authentication_token]))
      redirect_to designer_accepted_registration_url(current_user.authentication_token) if current_user and current_user.role == User::ROLE[:designer] and current_user.designer_status == User::STATUS[:approved_needs_password] and params_check
    end

    rescue_from ActiveRecord::RecordNotFound do
      render :partial => "errors/error_404", :status => :not_found
    end

end
