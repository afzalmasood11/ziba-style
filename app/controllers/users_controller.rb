class UsersController < ApplicationController

  layout "dashboard_admin"

  before_filter :ensure_user_is_admin, except: [:browsing_gender, :become_designer]

  cache_sweeper :designer_sweeper, only: :update


  def index
  	@users = User.where("id IS NOT NULL").page(params[:page]).per(10) # weird conditions but Users.all.page ... doesn't work
	end

  def export_users
    @users = User.all

    respond_to do |format|
      format.html
      format.xls { send_data(@users.to_xls(:only => [:user_role, :registration_date, :email]))
        return
      }
    end
  end

  def edit
  	@user = User.find(params[:id])
  end

  def update
  	@user = User.find(params[:id])

    if params[:user].include? :role or params[:user].include? :designer_status
      @user.role            = params[:user][:role]            if params[:user].include? :role
      @user.designer_status = params[:user][:designer_status] if params[:user].include? :designer_status

      if @user.save and @user.designer_status == User::STATUS[:authorized]
        notify_designer("account_editable", @user)
      end

      params[:user].delete :designer_status if params[:user].include? :designer_status
      params[:user].delete :role            if params[:user].include? :role
    end
    
    respond_to do |format|
      if @user.update_attributes(params[:user])
        redirect = params[:user][:homepage_image_attributes].present? ? admin_home_page_index_url : users_path
        format.html { redirect_to redirect, notice: "#{@user.fname} #{@user.lname} was successfully updated." }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end



  def browsing_gender
    if User::GENDER.value? params[:gender].to_i
      session[:browsing_gender] = params[:gender].to_i
      redirect_to request.referer
    else      
      redirect_to root_path
    end
  end

end
