class DesignerOrdersController < ApplicationController

	before_filter :ensure_user_is_designer
  layout "dashboard_designers"

  def index
  	conditions = {designer_id: current_user.id}
    conditions['orders.status'] = Order::COMPLETED_STATUS
    @designer_orders = DesignerOrder.joins(:order).where(conditions).page(params[:page]).per(10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
  end

  def show
  	find_designer_order
  end

  def edit
  	find_designer_order
  end

  def update
    @designer_order = DesignerOrder.find(params[:id])

    notify_user = false
    if params[:designer_order].has_key? :ups_tracking and !params[:designer_order][:ups_tracking].empty?
      params[:designer_order][:shipping_status] = DesignerOrder::SHIPPING_STATUS[:shipped]
      notify_user = true
    end

    respond_to do |format|
      if @designer_order.update_attributes(params[:designer_order])
        notify_customer_designer_order_shipped(@designer_order) if notify_user and @designer_order.shipping_status == DesignerOrder::SHIPPING_STATUS[:shipped]
        
        format.html { redirect_to @designer_order }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @designer_order.errors, status: :unprocessable_entity }
      end
    end

  end

  protected

  def find_designer_order
    @designer_order = DesignerOrder.where(id: params[:id]).includes(:order => [:shipping_country, :billing_country], :order_items => {:item => :product}, :designer => :designer_country).first
  end

end
