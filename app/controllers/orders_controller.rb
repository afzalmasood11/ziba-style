class OrdersController < ApplicationController

  before_filter :ensure_user_can_access
  #before_filter :force_ssl, only: [:checkout, :validate_checkout]

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.where(status: Order::COMPLETED_STATUS).includes(:designer_orders => :order_items).order("created_at DESC").page(params[:page]).per(10)
    @orders = @orders.where(customer_id: current_user.id) unless params[:user_type] == "admin" and current_user.is_admin?

    respond_to do |format|
      
      #pick correct layout    
      layout = (params[:user_type] == "admin" and current_user_is_admin?) ? "dashboard_admin" :  "dashboard_users"

      format.html { render :index, layout: layout }
      format.json { render json: @orders }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find(params[:id], include: [:customer, :designer_orders => [:order_items => [:item => [:size, :product]], :designer => :designer_country]]) || not_found
    not_found unless @order.customer_id == current_user.id or current_user.is_admin? #security: only customer or admin can access the order

    if !session[:just_ordered].blank?
      @display_thank_you = true  #to display thank you message, TODO: this is not the perfect way to do it 
      session.delete :just_ordered
    end

    respond_to do |format|
      format.html { render "show", layout: params.include?(:print) ? "print" : "dashboard_users" }
      format.json { render json: @order }
    end
  end

  # GET /orders/1/edit
  def checkout
    user_can_checkout?
    if request.post?
      #cleaning old checkout
      current_user.cancel_checkout  
      session.delete :checkout_step
      session.delete :order_apply_credit
      #creating new checkout
      order = Order.find_or_create_by_customer_id_and_status(current_user.id, Order::STATUS[:pending_checkout])
      items = Item.find(current_user.cart.keys)
      items.each do |item|
        designer_order = DesignerOrder.find_or_create_by_order_id_and_designer_id(order.id, item.product.designer_id)
        order_item = OrderItem.find_or_create_by_designer_order_id_and_item_id(designer_order.id, item.id)
        order_item.update_attributes({quantity: current_user.cart["#{item.id}"], price: item.product.current_price, shipping_cost: item.product.shipping_cost})
      end
    end
    @order = Order.find_by_customer_id_and_status(current_user.id, Order::STATUS[:pending_checkout])      
    #prefilled whatever we can - we do that here because we don't want to save those info whitout having the user confirm those
    %w{ fname lname address1 address2 zipcode city state country_id phone }.each{|field| @order["shipping_#{field}"] = current_user[field] if @order["shipping_#{field}"].blank? }
    @checkout_step = session[:checkout_step] = params[:step] || session[:checkout_step] || "shipping"    
    @unused_credits = current_user.unused_credits
    @order.apply_credit = session[:order_apply_credit] || true
   end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find_by_customer_id_and_status(current_user.id, Order::STATUS[:pending_checkout])

    #we always want to validate the shipping info (but we need the validation to be conditionnal because we create the order the first time we load the form)
    params[:order][:validate_shipping] = true
    #we want to validate the billing info only when the user is on the billing part of the form and they're not using paypal
    params[:order][:validate_billing] = (params[:checkout_step] == "billing" and params[:order][:paypal_payment] != "1") #@order is supposed to have cc info to validate and store
    #we use the credit card first and last name as billing first and last name
    params[:order][:billing_fname] = params[:order][:credit_card_first_name]
    params[:order][:billing_lname] = params[:order][:credit_card_last_name]

    [:shipping_state_id, :billing_state_id].each do ||
      params[:order]
    end

    params[:order][:shipping_state_id] = "" if params[:checkout_step] == "shipping" and !params[:order][:shipping_state_id].present?
    params[:order][:billing_state_id]  = "" if params[:checkout_step] == "billing"  and !params[:order][:billing_state_id].present?

    #if the billing info are the same as the shipping info then we populate the billing info with the shipping one 
    #%w{address1 address2 city zipcode state phone country_id}.each{ |field| params[:order]["billing_#{field}"] = @order["shipping_#{field}"] } if params[:order].has_key? :billing_as_shipping and params[:order][:billing_as_shipping] == "1"
    
    if params[:order][:apply_credit] and params[:order][:apply_credit] == "1"
      Credit.find_all_by_id(params[:order][:credits_attributes].collect{|credit_attr| credit_attr[1][:id].to_i }).collect{|c| c.update_attributes({order_id:@order.id}) }
      session[:order_apply_credit] =  true
    else
      current_user.cancel_credits_for_checkout(@order.id)
      session[:order_apply_credit] =  false
    end
    params[:order].delete :credits_attributes

    if @order.update_attributes(params[:order])
      if params[:checkout_step] == "billing" and params[:order][:paypal_payment] == "1"
        response = PPGATEWAY.setup_purchase(1000,
          :ip                => request.remote_ip,
          :return_url        => validate_checkout_url,
          :cancel_return_url => validate_checkout_url
        )
        redirect_to PPGATEWAY.redirect_url_for(response.token)
      else
        respond_to do |format|
          session[:checkout_step] = get_next_step(params[:checkout_step])
          format.html { redirect_to params[:checkout_step] == "billing" ? validate_checkout_path : checkout_path, notice: 'Order was successfully updated.' }
          format.json { head :no_content }
        end
      end
    else
      respond_to do |format|
        @checkout_step = session[:checkout_step] || get_next_step(session[:checkout_step]) #TODO: maybe not, maybe it should be more clever and show the step that has errors
        format.html { render :checkout }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end


  def validate_checkout
    @order = Order.find_by_customer_id_and_status(current_user.id, Order::STATUS[:pending_checkout]) || not_found
    @order.apply_credit = session[:order_apply_credit]
    unless request.post?
      @submit_checkout = true
      @order.paypal_setup(params[:token]) if params.include? :token
    else
      params[:ip_address] = request.remote_ip
      if @order.process_checkout(params)
        current_user.empty_cart
        notify_customer_and_designers(@order)
        session[:just_ordered] = true

        #check if purchase is first purchase, if yes credit the person who invited that user
        if current_user.completed_orders.length == 1
          credit = current_user.credit_invitation_sender
          notify_customer("new_credit", credit) if credit
        end
          
        redirect_to order_path(@order), notice: "Thank you! Your Order has been submitted. You will receive a confirmation email."
      else
        @checkout_step = session[:checkout_step] = "billing"
        render :checkout
      end
    end
  end

  def order_complete
    render :order_complete, layout: request.xhr? ? false : "application"
  end

  def cancel_checkout
    respond_to do |format|
      if current_user.cancel_checkout
        format.html { redirect_to cart_index_path, notice: 'Checkout was successfully cancel.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit", layout: "application" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  private 

  def user_can_checkout?
    unless !current_user.pending_checkout.blank? || request.post? || current_user.cart.present?
      flash[:notice] = "Cannot access this page, you need to checkout <a href=\"/cart\">your current cart</a>.".html_safe
      redirect_to request.referer || '/'  
    end
  end

  def get_next_step(current_step)
    next_step = "shipping"
    case current_step
      when "shipping"; next_step = "billing"        
    end
    next_step
  end

  private

  def ensure_user_can_access
    redirect_to '/' if params.include?(:user_type) and params[:user_type] == "admin" and !current_user_is_admin?
  end

  #def force_ssl
    #redirect_to "https://#{request.host}#{request.fullpath}" unless env['HTTP_X_FORWARDED_PROTO'] == 'https' or Rails.env == 'development'
  #end

end
