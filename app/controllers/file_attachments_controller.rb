class FileAttachmentsController < ApplicationController

  before_filter :ensure_user_can_edit_attachment

  def show
    @file_attachment = FileAttachment.find params[:id]

    respond_to do |format|
      format.html {render "show", layout: (current_user.is_admin? and params.include?(:user_type) and params[:user_type] == "admin") ? "dashboard_admin" : "dashboard_designers"}
    end
  end

  def crop_studio
    @file_attachment = FileAttachment.find params[:id]
    @style = params[:style].to_sym
    respond_to do |format|
      format.html {render "crop_studio", layout: (current_user.is_admin? and params.include?(:user_type) and params[:user_type] == "admin") ? "dashboard_admin" : "dashboard_designers"}
    end
  end
  
  def update
    @file_attachment = FileAttachment.find params[:id]


    # persist the crop rules for this particular style
    # so that when each style is reprocessed, the cropping coordinates can be 
    # applied appropriately
    crop_rules = @file_attachment.crop_rules || {}

    crop_rules[params[:file_attachment][:style_to_crop].to_sym] = {
      crop_x: params[:file_attachment][:crop_x].to_i,
      crop_y: params[:file_attachment][:crop_y].to_i,
      crop_w: params[:file_attachment][:crop_w].to_i,
      crop_h: params[:file_attachment][:crop_h].to_i,
    }

    logger.debug "here are the crop rules I'd like to save"
    logger.debug crop_rules 

    #@file_attachment.update_column :crop_rules, nil 
    #@file_attachment.update_column :crop_rules, crop_rules

    @file_attachment.crop_rules = crop_rules

    if @file_attachment.update_attributes(params[:file_attachment])

      #redirect_to request.referrer, notice: "attachment updated"
      # occasionally this page would need to be refreshed, putting the user in a confusing state
      # explicitly set a random get variable to obviate the need for this refresh 
      redirect_to file_attachment_path(@file_attachment.id) + "?=#{SecureRandom.hex(10)}", notice: "attachment updated"
      #redirect_to @file_attachment, notice: "attachment updated", version: 
    else
      redirect_to request.referrer, notice: "unable to update attachment"
    end
  end

  private

  def ensure_user_can_edit_attachment
    # if the user isn't an admin or the owner of the product, 
    #
    @file_attachment = FileAttachment.find params[:id]

    unless current_user.is_admin? or current_user.products.include? @file_attachment.entity
      redirect_to root_path, alert: "Permission denied"
    end

  end

end
