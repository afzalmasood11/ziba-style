class FeedbacksController < ApplicationController

  before_filter :ensure_user_is_designer, only: [:index]


  def index
    @feedbacks = current_user.is_admin? ? Feedback.all : DesignerOrder.joins(:feedback).select('`feedbacks`.*').where(designer_id: current_user.id).page(params[:page]).per(10)

    respond_to do |format|
      format.html { render :index, layout: "dashboard_designers" }
      format.json { render json: @designer_orders }
    end
  end

  def customer_index
    @designer_orders = DesignerOrder.includes(:order, :feedback, :order_items => {:item => :product}).joins(:order).where{order.customer_id == my{current_user.id}}.order("designer_orders.created_at DESC").page(params[:page]).per(8)

    respond_to do |format|
      format.html { render :customer_index, layout: "dashboard_users" }
    end
  end

  def new
    @feedback = Feedback.new

    respond_to do |format|
      format.html { render :new, layout: "dashboard_users" }
      format.json { render json: @feedback }
    end
  end

  def create
    @feedback = Feedback.new(params[:feedback])

    respond_to do |format|
      if @feedback.save
        format.html { redirect_to review_orders_path, notice: 'Feedback was successfully created.' }
        format.json { render json: @feedback, status: :created, location: @feedback }
      else
        format.html { render action: "new", layout: "dashboard_users" }
        format.json { render json: @feedback.errors, status: :unprocessable_entity }
      end
    end
  end

end
