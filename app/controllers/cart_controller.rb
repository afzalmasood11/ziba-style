class CartController < ApplicationController
  
  def index
    @cart = current_user.cart
    @items = !@cart.blank? ? Item.find_all_by_id(@cart.keys) : []

    respond_to do |format|
      format.html { render request.xhr? ? :xhr_index : :index, layout: request.xhr? ? false : "application" }
      format.json { render json: @items }
    end
  end

  def create
    cart_updated = false
    if cart_is_valid?
      cart = params[:cart]
      unless cart[:quantity].to_i == 0
        item = Item.find(cart[:item_id])
        in_stock = (item.in_stock? and item.quantity >= (cart[:quantity].to_i + (current_user.cart[cart[:item_id].to_i] || 0)))
        if in_stock
          current_user.cart = {} if current_user.cart.blank?
          current_user.cart[cart[:item_id]] = current_user.cart[cart[:item_id]].blank? ? cart[:quantity].to_i : current_user.cart[cart[:item_id]].to_i + cart[:quantity].to_i
          current_user.save
        end
        cart_updated = in_stock
      else
        current_user.cart.delete cart[:item_id]
        current_user.save
        cart_updated = true
      end
    end

    respond_to do |format|
      if cart_updated
        format.html { redirect_to '/shopping_bag', notice: 'Product added to cart.' }
        format.json { head :no_content }
      else
        error = cart_is_valid? ? 'Product could not be added to the cart. The product is out of stock or the quantity selected is higher the number of available items.' : 'The quantity is not a real quantity'
        format.html { redirect_to request.referrer, alert: error }
        format.json { render json: {error: error}, status: :unprocessable_entity }
      end
    end

  end

  def update
    cart_updated = false
    cart_can_be_updated = true
    if cart_is_valid_for_update? #check data into cart object

      params[:cart].each do |item_id, quantity|
        if params[:remove][item_id] == "1"
          params[:cart].delete item_id 
        else
          item = Item.find(item_id)
          cart_can_be_updated = false if !item.in_stock? or item.quantity < quantity.to_i
        end
      end

      if cart_can_be_updated
        current_user.cart = params[:cart]
        current_user.save
        cart_updated = true
      end
    end
    respond_to do |format|
      if cart_updated
        format.html { redirect_to cart_index_path, notice: 'The cart was updated successfully' }
      else
        format.html { redirect_to cart_index_path, alert: 'Cart could not be updated. A product is out of stock or the quantity selected is higher the number of available items.' }
      end
    end
  end

  def destroy
    current_user.cart.delete params[:id] unless current_user.cart.blank?
    current_user.save

    respond_to do |format|
      format.html { redirect_to cart_index_path, notice: 'The cart was updated successfully' }
      #format.json { head :no_content }
    end
  end

  private

  #custom validation rules for the cart
  def cart_is_valid?
    !params[:cart].blank? and !params[:cart][:quantity].blank? and !params[:cart][:item_id].blank? and params[:cart][:quantity].to_i.to_s == params[:cart][:quantity] and params[:cart][:item_id].to_i.to_s == params[:cart][:item_id]
  end

  def cart_is_valid_for_update? 
    valid = !params[:cart].blank? and !params[:cart][:quantity].blank?
    if valid
      params[:cart].keys.each{ |key| return false unless key.to_i.to_s == key }
      params[:cart].values.each{ |value| return false unless value.to_i.to_s == value }
    end
    valid
  end

end
