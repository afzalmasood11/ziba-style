class CountriesController < ApplicationController

  before_filter :ensure_user_is_admin, except: [:index, :show, :history]
  cache_sweeper :country_sweeper, only: [:create, :update, :destroy]

  def index      
    @products = Product.with_celebrity_pictures.includes(:celebrity_pictures).sample(10)
    @editor_picks = EditorPick.active

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @countries }
    end
  end

  def admin_index
    @countries = Country.all

    respond_to do |format|
      format.html
      format.json { render json: @countries }
    end
  end

  def show
    @country = Country.find(params[:id]) || not_found
    
    category_ids = params[:category_ids].present? ? params[:category_ids] : Category.all.collect(&:id)
    params[:id] = @country.id
    @products = Product.best_seller.filter_by_country(params[:id]).page(params[:page]).per(9)
    @products = filter_products(@products, params)
#    @categories = @country.categories.uniq
    @categories = @products.collect{|pro| pro.categories }.flatten.uniq
    get_sizes
    @designers = User.designers(country_ids: [@country.id], category_ids: category_ids, designer_active: true).uniq #TODO: problem with this, some designer don't have available products

    @infinite_scrolling = params[:infinite_scrolling].present?

    @params = params
    @params.delete("page")
    @params.delete("infinite_scrolling")

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @country }
      format.js { render 'shared/filter_results'}
    end
  end

  def history
    @country = Country.find(params[:id])
    @history = true #switch for the template (using the same template as show)

    respond_to do |format|
      format.html { render :show }
      format.json { render json: @country }
    end
  end

  def mercury_update
    country = Country.find(params[:id])
    country.name = params[:content][:country_name][:value]
    country.description = params[:content][:country_description][:value]
    country.save!
    render text: ""
  end


  #ADMIN ONLY

  def new
    @country = Country.new

    respond_to do |format|
      format.html {render :new, layout: "dashboard_admin"}# new.html.erb
      format.json { render json: @country }
    end
  end

  def edit
    @country = Country.find(params[:id])
    #@country.build_banner if @country.banner.blank?
    #@country.build_flag if @country.flag.blank?

    (Country::NUMBER_OF_HISTORY_PICTURES-@country.history_pictures.count).times {@country.history_pictures.build} #initialize product pictures
    render :edit, layout: "dashboard_admin"
  end

  def create
    @country = Country.new(params[:country])

    respond_to do |format|
      if @country.save
        # format.html { redirect_to @country, notice: 'Country was successfully created.' }
        format.html { redirect_to mercury_editor_path("countries/#{@country.id}"), notice: 'Country was successfully created.' }
        format.json { render json: @country, status: :created, location: @country }
      else
        format.html { render action: "new", layout: "dashboard_admin" }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

 def update
    @country = Country.find(params[:id])

    # handle mercury editor submission
    if params[:content].present?
      @country.name           = params[:content][:country_name][:value]  if params[:content].include? :country_name
      @country.description    = params[:content][:country_description][:value] if params[:content].include? :country_description
      @country.history        = params[:content][:country_history][:value] if params[:content].include? :country_history
      @country.history_title  = params[:content][:country_history_title][:value] if params[:content].include? :country_history_title
      @country.save!
      render text: ""
      return
    end 

    params[:country][:is_shipping_destination] = params[:country][:is_shipping_destination].to_i if params.include? :country and params[:country].include? :is_shipping_destination
    params[:country][:is_shipping_destination] = params[:country][:is_shipping_destination].to_i if params.include? :country and params[:country].include? :is_shipping_destination
 
    respond_to do |format|
      if @country.update_attributes(params[:country])
        redirect = params[:country][:homepage_image_attributes].present? ? admin_home_page_index_url : @country
        format.html { redirect_to redirect, notice: 'Country was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit", layout: "dashboard_admin" }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @country = Country.find(params[:id])
    @country.destroy

    respond_to do |format|
      format.html { redirect_to countries_url }
      format.json { head :no_content }
    end
  end
end
