class ProductsController < ApplicationController
  
  before_filter :authenticate_user!, except: [:share, :show]
  before_filter :clean_product_params, only: [:create, :update]
  before_filter :ensure_user_is_designer_or_admin?, only: [:index]
  before_filter :ensure_user_is_designer, only: [:new, :create, :edit, :update, :sizes, :destroy]
  cache_sweeper :product_sweeper, only: [:create, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
    @products = Product.includes(:primary_categories).order("products.created_at DESC").page(params[:page]).per(10)
    @products = (current_user.is_admin? and params.include?(:user_type) and params[:user_type] == "admin") ? @products : @products.where{(products.designer_id == my{current_user.id}) & (products.status > my{Product::STATUS[:deleted]})}
    @products = filter_products(@products, params)

    @products = @products.search params[:query] if params.include? :query

    respond_to do |format|
      format.html {render "index", layout: (current_user.is_admin? and params.include?(:user_type) and params[:user_type] == "admin") ? "dashboard_admin" : "dashboard_designers"}
      format.json { render json: @products }
      format.xls
    end
  end

  def export_products
    @products = Product.all

    respond_to do |format|
      format.html
      format.xls { send_data(@products.to_xls(:only => [:name, :designer_name, :price, :categories_name, :is_custom, :no_of_items]))
        return
      }
    end

#    respond_to do |format|
#      format.xls
#    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id]) || not_found
    #security - make sure the product is approved or  belongs to the designer or that current_user is an admin
    not_found unless @product.status == Product::STATUS[:approved] or (@product.designer_id == current_user.id and @product.status > Product::STATUS[:deleted]) or current_user.is_admin?

    unless @product.status == Product::STATUS[:approved] or current_user.is_admin?
      flash_msg = "This product is #{product_status_to_string(@product.status)}."
      flash_msg += " Click <a href=\"#{edit_product_path(@product)}\">here to edit</a>" unless @product.status == Product::STATUS[:pending_approval]
      flash[:notice] = flash_msg.html_safe  
    end

    @available_items = @product.available_items
    
    @quantity_values = {}
    @available_items.each{|a_i| @quantity_values[a_i.id] = a_i.quantity }
    @quantity_values = @quantity_values.to_json

    if request.path != product_path(@product)
      redirect_to @product, status: :moved_permanently
    else
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @product }
      end
    end
  end

  def size_chart
    @product = Product.find(params[:id]) || not_found

    respond_to do |format|
      format.html { render "size_chart", layout: request.xhr? ? false : "application" }
      format.json { render json: @product }
    end
  end

  def share_by_email
    @product = Product.find(params[:id]) || not_found
    not_found unless @product.status == Product::STATUS[:approved]
    @invitation = Invitation.new
    @invitation.product_id = @product.id
    respond_to do |format|
      format.html { render :share_by_email, layout: request.xhr? ? false : "application" }
    end
  end

  def share
    @product = Product.find(params[:id]) || not_found
    not_found unless @product.status == Product::STATUS[:approved]
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new
    @product.active = 1
    
    initialize_pictures_for_form
    @valid_product = false
    
    respond_to do |format|
      format.html {render "new", layout: "dashboard_designers"}
      format.json { render json: @product }
    end
  end

  def edit 
    @product = Product.find(params[:id])    
    not_found unless @product.designer_id == current_user.id or current_user.is_admin? #security - make sure the product belongs to the designer
    @product.product_excluded_countries.build

    initialize_pictures_for_form
    @disable = @product.status == Product::STATUS[:pending] ? false: true 

    # allow the admin to edit the product, no matter what
    if current_user.is_admin?
      @disable = false
    end

    # weird little checking going on here - we just want to know if both form (edit + shipping) are valid to show the submit for approval button
    @product.submitting_shipping_info = "1"
    @valid_product = @product.valid?
    @product.submitting_shipping_info = false
    @product.valid?
    #end of weird thing going on

    # default return policy
    @product.return_days ||= 7

    respond_to do |format|
      format.html {render ((params.include?(:action_type) and params[:action_type] == "shipping") ? "shipping" : "edit"), layout: "dashboard_designers"}
      format.json { render json: @product }
    end
  end

  def crop_studio
    @product = Product.find(params[:id])

    respond_to do |format|
      format.html { render "crop_studio", layout: "dashboard_designers" }
    end
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(params[:product])
    @product.designer_id = current_user.id

    respond_to do |format|
      if @product.save
        format.html { redirect_to products_sizes_path(@product), notice: 'Product was successfully created.' }
        format.json { render json: @product, status: :created, location: products_sizes_path(@product) }
      else
        initialize_pictures_for_form
        format.html { render action: "new", layout: "dashboard_designers" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find(params[:id])
    not_found unless @product.designer_id == current_user.id or current_user.is_admin? #security - make sure the product belongs to the designer or that the user is an admin

    # is product is custom and quantity == 1, change quantity to 10
    if @product.is_custom and params[:product].include? :items_attributes
      params[:product][:items_attributes].each do |i_a|
        i_a[1]["quantity"] = "10" if i_a[1]["quantity"] == "1"
        i_a[1]["quantity"] = nil if i_a[1]["quantity"] == "0"
      end
    end

    if (params[:product].include? :product_request_approval and @product.status == Product::STATUS[:pending]) or (params[:product].include? :product_back_to_pending and @product.status == Product::STATUS[:approved])
 
   
      if params[:product].include?(:product_request_approval) #PRODUCT WILL BE PENDING FOR APPROVAL
        @product.status = Product::STATUS[:pending_approval]
        notify_admin("new_product_designer_to_approve", @product) if @product.save
        redirect_to product_path(@product), notice: 'Your product is now pending for approval by an admin.'
      else  #PRODUCT WILL BE BACK TO PENDING
        @product.status = Product::STATUS[:pending] 
        @product.save
        redirect_to edit_product_path(@product), notice: 'Your product is editable.'
      end
    else

      #SECURITY: if the product is approved or pending for approval, only the item quantity or the active field is updatable unless current_user is admin
      unless @product.status == Product::STATUS[:pending] or current_user.is_admin?
        not_found unless params[:product].include? :items_attributes or params[:product].include? :active
        items_attributes = params[:product][:items_attributes] if params[:product].include? :items_attributes
        active = params[:product][:active] if params[:product].include? :active
        params.delete :product
        params[:product] = {}
        params[:product][:items_attributes] = items_attributes if defined? items_attributes and items_attributes and items_attributes != nil
        params[:product][:active] = active if defined? active and active and active != nil
      end

      if [0, 7].include? @product.return_days_option
        @product.return_days = @product.return_days_option
      end

      respond_to do |format|
        if process_sale_params and @product.update_attributes(params[:product])  # process_sale_params because only admin can change the sale status of the product
          redirect_path = edit_product_path(@product)
          if params.include? :view
       
            redirect_path = case params[:view]
              when "edit"; products_sizes_path(@product)
              when "sizes"; products_shipping_path(@product)
              else; edit_product_path(@product)
            end
       
            redirect = params[:product][:homepage_image_attributes].present? ? admin_home_page_index_url : redirect_path
            format.html { redirect_to redirect, notice: 'Product was successfully updated.' }
            format.json { head :no_content }
          elsif params[:product][:homepage_image_attributes].present?
            format.html { redirect_to admin_home_page_index_url, notice: 'Product was successfully updated.' }
          else
            format.html { render action: :edit, layout: "dashboard_designers" }
            format.json { render json: { status: @product.active } }
          end
        else
     
          if params[:product][:homepage_image_attributes].present?
            format.html { redirect_to admin_home_page_index_url }            
          else
            view = (params.include? :view) ? params[:view] : "edit"
            format.html { render action: view, layout: "dashboard_designers" }
          end
          format.json { render json: @product.errors }
        end
      end
    end 
  end

  def destroy
    @product = Product.find(params[:id])
    not_found unless @product.designer_id == current_user.id or current_user.is_admin? #security
    @product.status = Product::STATUS[:deleted]
    @product.save

    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end

  def sizes 
    @product = Product.find(params[:id])
    
    respond_to do |format|
      format.html {render "sizes", layout: "dashboard_designers"}
      format.json { render json: @product }
    end
  end
  
  private
  
  def initialize_pictures_for_form
    @product.build_primary_image unless @product.primary_image
    (Product::NUMBER_OF_PICTURES-@product.pictures.count).times { @product.pictures.build } #initialize product pictures      
    (Product::NUMBER_OF_CELEBRITY_PICTURES-@product.celebrity_pictures.count).times {@product.celebrity_pictures.build} #initialize celebrity pictures
  end

  # before_filter for :create and :update
  def clean_product_params
    # always store weight in KG in the database. weight_type is just for front-end purposes
    # TODO: (nice to have?) note: uses alchemist gem for conversion    
    params[:product][:weight] = params[:product][:weight].to_f.pounds.to.kilograms.to_s if (params[:product][:weight] and params[:product][:weight_type] and params[:product][:weight_type] == 'lb')
    params[:product][:return_days] = params[:product][:return_days_option]              if (params[:product][:return_days_option] and params[:product][:return_days_option] != 'other')
    params[:product].delete(:return_days_option)

    if params[:product].include? :celebrity_pictures_attributes
      params[:product][:celebrity_pictures_attributes].each do |key, value|
        params[:product][:celebrity_pictures_attributes].delete(key) if value[:attachment].blank? and value[:meta].blank?
      end
    end

  end

  def process_sale_params
    (params[:product].key?(:sale) and current_user.is_admin?) or params[:product][:sale].blank?
  end

end
