class PagesController < ApplicationController

  before_filter :ensure_user_is_admin, except: [:show, :blog]
  cache_sweeper :page_sweeper, onyl: [:create, :update, :destroy]
  skip_before_filter :designer_needs_password
  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pages }
    end
  end

  def blog 
    #@rss = Feedzirra::Feed.fetch_and_parse(::BLOG_FEED_URL)
    respond_to do |format|
      format.html { render :blog }
    end
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    @page = Page.find_by_name(params[:id]) || not_found
    
    # redirect for pages marked privata, aka requiring authentication to view
    if @page.private and not current_user
      redirect_to new_user_session_path, alert: "viewing this page requires authentication"
      return
    end

    respond_to do |format|
      format.html { render :show, layout: request.xhr? ? false : "application" }
      format.json { render json: @page }
    end
  end

  def mercury_update
    page = Page.find(params[:id])
    page.title = params[:content][:page_title][:value]
    page.text = params[:content][:page_text][:value]
    page.save!
    render text: ""
  end

  # GET /pages/new
  # GET /pages/new.json
  def new
    @page = Page.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @page }
    end
  end

  # GET /pages/1/edit
  def edit
    @page = Page.find(params[:id])
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(params[:page])

    respond_to do |format|
      if @page.save
        format.html { redirect_to @page, notice: 'Page was successfully created.' }
        format.json { render json: @page, status: :created, location: @page }
      else
        format.html { render action: "new" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pages/1
  # PUT /pages/1.json
  def update

    # handle updates from mercury editor
    if params[:content].present?
      page = Page.find_by_name params[:id]
      page.title = params[:content][:page_title][:value]
      page.text = params[:content][:page_text][:value]
      page.save!
      render text: ""
      return
    end

    @page = Page.find(params[:id])

    respond_to do |format|
      if @page.update_attributes(params[:page])
        format.html { redirect_to @page, notice: 'Page was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page = Page.find(params[:id])
    @page.destroy

    respond_to do |format|
      format.html { redirect_to pages_url }
      format.json { head :no_content }
    end
  end
end
