class SearchController < ApplicationController

	def index
		# just displays search box
	end

	def create

    @query = params[:query] || '';

    unless params[:query].blank?
			
      @products = Product.search @query, field_weights: {
        name: 10,
        short_description: 5,
        history: 3,
        inspiration: 3,
        materials: 1
      }

      @designers = User.search @query, field_weights: {
        designer_name: 3,
        designer_biography: 1
      }

      @categories = Category.search @query
			@countries  = Country.search  @query
    end		
	end
end