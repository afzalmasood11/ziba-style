class AdminController < ApplicationController

  layout "dashboard_admin"
  before_filter :ensure_user_is_admin, only: [:approve, :switch_to_designer]
  before_filter :ensure_user_is_designer, only: :switch_to_admin

  def approve
    action = params[:action_name]
    unless request.post?
      case action
        when "approve_designers"
          # if action == "authorize_designers"; status = User::STATUS[:pending_auth]
          # elsif action == "approve_designers"; status = User::STATUS[:pending_approval]; end
          status = User::STATUS[:pending_approval]
          @title = "Approve Boutiques" 
          @data = { columns: {:id => "ID", :email => "Email", :fname => "First Name", :lname => "Last Name", :action => "Action"}, 
                    entities: User.find_all_by_designer_status(status), url: request.fullpath }
        when  "authorize_designers"
          @title = "Approve Designers" 
          # pending_promotion_to_designer => users asking to go from regular user to designer, can still login and use site will pending
          # pending_auth => users just signing up and waiting to be approved to come onto the platform
          @data = { columns: {:id => "ID", :email => "Email", :fname => "First Name", :lname => "Last Name", :portfolio => 'Portfolio', :action => "Action"}, 
                    entities: User.where(designer_status: [User::STATUS[:pending_auth], User::STATUS[:pending_promotion_to_designer]]), url: request.fullpath }
        when "approve_products"
          @title = "Approve Products"
          @xls = "Export all Products"
          @data = { columns: { :id => "ID", :name => "Name", :short_description => "Short Description", :action => "Action" }, 
                    entities: Product.find_all_by_status(Product::STATUS[:pending_approval]), url: request.fullpath }
        when "approve_videos"
          @title = "Approve Designer Videos"
          @data = { columns: {:id => "ID", :name => "Name", :url => "Url", :action => "Action"}, 
                    entities: DesignerVideo.find_all_by_status(DesignerVideo::STATUS[:pending]), url: request.fullpath }
        else
          @data = []
      end
    else
      entity_id = params[:entity_id]
      case action
        when "approve_designers", "authorize_designers"
          u = User.find(entity_id)

          if action == "authorize_designers"  
            if(u.designer_status == User::STATUS[:pending_promotion_to_designer])
              status = User::STATUS[:approved]
            else
              status = User::STATUS[:approved_needs_password]
              u.confirm!
              u.authentication_token = Invitation.generate_token
            end
          elsif action == "approve_designers"
            status = User::STATUS[:approved]
          end

          u.designer_status = status
          u.admin_update_status = true
          status = u.save ? true : false
          notify_designer(action, u) if status
        when "approve_products"
          p = Product.find(entity_id)
          p.status = Product::STATUS[:approved]
          status = p.save ? true : false
          notify_designer(action, p) if status
        when "approve_videos"
          p = DesignerVideo.find(entity_id)
          p.status = DesignerVideo::STATUS[:approved]
          status = p.save ? true : false
          notify_designer(action, p) if status
        else
          #nothing to do here
      end
      render :json => {status: status}
    end
  end

  def switch_to_designer
    
    user_id = params[:admin][:designer_id] if params.include? :admin and params[:admin].include? :designer_id
    if user_id

      admin_user = current_user
      designer = User.find(user_id) || not_found

      sign_out
      sign_in designer

      session[:admin_id] = admin_user.id   if admin_user
      session[:admin_switch_at] = Time.now if admin_user

    end

    redirect_to request.referer || "/designers/boutique"

  end
  
  def switch_to_admin
    if session.include? :admin_id
      admin = User.find(session[:admin_id]) || not_found

      sign_out
      sign_in admin
    end
    redirect_to request.referer || "/admin/approve_designers"
  end

end
