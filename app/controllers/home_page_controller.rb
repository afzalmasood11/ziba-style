class HomePageController < ApplicationController

  before_filter :ensure_user_is_admin, except: :index

  def index      
  	#@entities  = HomePageEntity.order(:order).collect{|e| e.entity ? e.entity : e }.flatten.uniq
    @entities  = HomePageEntity.order(:order).flatten.uniq
    @need_log_in = params[:need_log_in] ? true : false

    @signed_out = params[:signed_out] ? true : false
    respond_to do |format|
      format.html
      format.json { render json: @entities }
    end
  end

  def admin

  	if request.post?
  		if params[:entity].present? and params[:entity][:id].present? and params[:entity][:type].present?
				HomePageEntity.find_or_create_by_entity_id_and_entity_type(params[:entity][:id], params[:entity][:type])

        @entities  = HomePageEntity.order(:order)
        @entities.each{ |e| e.entity.build_homepage_image if e.entity and e.entity.homepage_image.blank? }

        respond_to do |format|
          format.html
        end
  		end
      if params[:homepage_entities_order].present?
        params[:homepage_entities_order].each do |val|
          hpe = HomePageEntity.find(val[0])
          hpe.order = val[1]
          hpe.save
        end
        respond_to do |format|
          format.json { render json: @entities }
        end
      end
    else
      @entities  = HomePageEntity.order(:order)
      @entities.each{ |e| e.entity.build_homepage_image if e.entity and e.entity.homepage_image.blank? }
  	
      respond_to do |format|
        format.html
        format.json { render json: @entities }
      end
    end
  end

end
