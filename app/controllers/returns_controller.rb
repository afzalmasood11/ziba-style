class ReturnsController < ApplicationController
  
  before_filter :ensure_user_is_designer, only: :designer_returns
  before_filter :ensure_user_is_admin, only: :restock

  def index
    @returns = Return.includes({:order_item => {:designer_order => {:order => :customer}}})
                     .joins({:order_item => {:designer_order => {:order => :customer}}})
                     .page(params[:page]).per(10)
    @returns = @returns.where{users.id == my{current_user.id}} unless current_user.is_admin? and params[:user_type] == "admin"
    @admin_view = true if current_user.is_admin? and params[:user_type] == "admin"
    not_found if !current_user.is_admin? and params[:user_type] == "admin"

    respond_to do |format|
      format.html { render :index, layout: (current_user.is_admin? and params[:user_type] == "admin") ? "dashboard_admin" : "dashboard_users" }
      format.json { render json: @returns }
    end
  end

  def designer_returns
    @returns = Return.includes({:order_item => {:designer_order => {:order => :customer}}})
                     .joins({:order_item => {:designer_order => {:order => :customer}}})
                     .where{designer_orders.designer_id == my{current_user.id}}
                     .page(params[:page]).per(10)

    respond_to do |format|
      format.html { render :index_designer, layout: "dashboard_designers" }
      format.json { render json: @returns }
    end
  end

  def show
    @return = Return.find(params[:id], :include => {:order_item => [{:designer_order => [:order, :designer]}, {:item => :product}] }) || not_found
    not_found unless @return.order_item.designer_order.order.customer_id == current_user.id or @return.order_item.designer_order.designer_id == current_user.id or current_user.is_admin? #security

    respond_to do |format|
      format.html { render :show, layout: (params.include? :print) ? "print" : "dashboard_users" }
      format.json { render json: @return }
    end
  end

  def new
    @order = Order.where({id: params[:order_id], customer_id: current_user.id}).first || not_found
    @order_item = OrderItem.find_by_order_id_and_item_id(params[:order_id], params[:item_id])

    #check if there are still some items to return
    already_made_returns = Return.find_all_by_order_item_id(@order_item.id)
    if already_made_returns.empty? or @order_item.quantity > already_made_returns.collect{|r| r.quantity }.sum
      #check if customer can still return the item
      if @order_item.item.product.lead_days > @order.return_days_left
        @return = Return.new
        @return.order_item_id = @order_item.id

        respond_to do |format|
          format.html { render :new, layout: "dashboard_users" }
          format.json { render json: @return }
        end
      else #customer cannot return the item anymore
        redirect_to @order, alert: "Return period expired"
      end
    else
      redirect_to @order, alert: "No more items to return"
    end
  end

  def edit
    @return = Return.find(params[:id], :include => {:order_item => [{:designer_order => [:order, :designer]}, {:item => :product}] }) || not_found
    not_found unless @return.order_item.designer_order.order.customer_id == current_user.id or @return.order_item.designer_order.designer_id == current_user.id or current_user.is_admin? #security
    not_found if !current_user.is_admin? and params[:user_type] == "admin"
    
    @admin_view = true if current_user.is_admin? and params[:user_type] == "admin"

    respond_to do |format|
      format.html { render :edit, layout: "dashboard_users" }
    end
  end

  def create
    #security - check that order_item belongs to an order of the current_user
    @order_item = OrderItem.where{ (id == my{params[:return][:order_item_id]}) & (orders.customer_id == my{current_user.id}) }.joins(:designer_order => :order).first
    not_found if @order_item.blank?

    #security - check if there are still some items to return
    already_made_returns = Return.find_all_by_order_item_id(@order_item.id)
    if !already_made_returns.empty? and @order_item.quantity <= already_made_returns.collect{|r| r.quantity }.sum + params[:return][:quantity].to_i
      redirect_to order_item.designer_order.order, alert: 'No more items to return'
    else
      @return = Return.new(params[:return])

      respond_to do |format|
        if @return.save
          format.html { redirect_to @return, notice: 'Return was successfully created.' }
          format.json { render json: @return, status: :created, location: @return }
        else
          format.html { render action: "new", layout: "dashboard_users" }
          format.json { render json: @return.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def update
    @return = Return.find(params[:id], :include => {:order_item => [{:designer_order => [:order, :designer]}, {:item => :product}] }) || not_found
    not_found unless @return.order_item.designer_order.order.customer_id == current_user.id or @return.order_item.designer_order.designer_id == current_user.id or current_user.is_admin? #security

    respond_to do |format|
      if @return.update_attributes(params[:return])
        format.html { redirect_to returns_path, notice: 'Return was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit", layout: "dashboard_users" }
        format.json { render json: @return.errors, status: :unprocessable_entity }
      end
    end
  end

  def restock
    @return = Return.find(params[:id]) || not_found
    unless @return.restocked or @return.status != Return::STATUS[:arrived]
      @return.order_item.item.quantity = @return.order_item.item.quantity + @return.quantity
      if @return.order_item.item.save
        @return.restocked = true
        @return.status = Return::STATUS[:restocked]
        if @return.save
          if @return.order_item.designer_order.order.transaction_platform == Order::PLATFORM[:paypal]

            #TODO: handle refund via PayPal
            #transaction_result = PPGATEWAY.transfer(refund_price_in_cents, paypal_email_to_refund_to, :currency => currency.name, :subject => "Withdrawal from My Site", :note => "A personal message if you want")
          
            flash[:alert] = "The Item has been restocked! The customer could not be refunded because PayPal was used as a payment gateway. Please refund manually."

          elsif @return.order_item.designer_order.order.transaction_platform == Order::PLATFORM[:wepay]

            # transaction_result = BTGATEWAY.credit(refund_price_in_cents, @return.order_item.designer_order.order.braintree_vault_id)

            gateway = WepayRails::Payments::Gateway.new
            transaction_result = gateway.call_api('/checkout/refund', {
                checkout_id:   @return.order_item.designer_order.order.transaction_id,
                refund_reason: @return.reason,
                amount:        @return.return_price
            })
            @return.gateway_message = transaction_result["state"]
            @return.gateway_authorization_id = transaction_result["checkout_id"]
            if transaction_result["state"] == "captured"
              @return.status = Return::STATUS[:refunded]
              flash[:notice] = "The Item has been restocked and the customer has been refunded."
            else
              flash[:alert] = "The Item has been restocked but there was an error while refunding the customer."
            end
            @return.save

          end
        end
      end
    else
      flash[:alert] = @return.status != Return::STATUS[:arrived] ? "The status of the items has to be \"arrived\" to restock it" : "The Item has already been restocked."
    end
    redirect_to request.referrer
  end

end
