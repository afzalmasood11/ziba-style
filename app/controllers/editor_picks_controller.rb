class EditorPicksController < ApplicationController
  
  before_filter :ensure_user_is_admin, except: [:index, :show]
  
  # clean up home page with map if EditorPicks are modified
  cache_sweeper :editor_pick_sweeper, only: [:create, :update, :destroy]

  def index
    @editor_picks = EditorPick.active

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @editor_picks }
    end
  end

  def show
    if params[:id].nil?
      redirect_to EditorPick.first
    else
      @editor_pick = EditorPick.find(params[:id])
      if (@editor_pick.product.active and @editor_pick.product.status == Product::STATUS[:approved]) or (current_user and current_user.is_admin?)
        @editor_picks = EditorPick.active
        respond_to do |format|
          format.html
          format.json { render json: @editor_pick }
        end
      else
        redirect_to EditorPick.first
      end
    end
  end

  def new
    @editor_pick = EditorPick.new
    @editor_pick.product_id = params[:product_id] 

    respond_to do |format|
      format.html { render :new, layout: "dashboard_admin" }
      format.json { render json: @editor_pick }
    end
  end

  def edit
    @editor_pick = EditorPick.find(params[:id])
    render :edit, layout: "dashboard_admin"
  end

  def create
    @editor_pick = EditorPick.new(params[:editor_pick])

    respond_to do |format|
      if @editor_pick.save
        format.html { redirect_to @editor_pick, notice: 'Editor pick was successfully created.' }
        format.json { render json: @editor_pick, status: :created, location: @editor_pick }
      else
        format.html { render action: "new", layout: "dashboard_admin" }
        format.json { render json: @editor_pick.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @editor_pick = EditorPick.find(params[:id])

    respond_to do |format|
      if @editor_pick.update_attributes(params[:editor_pick])
        format.html { redirect_to @editor_pick, notice: 'Editor pick was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit", layout: "dashboard_admin" }
        format.json { render json: @editor_pick.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @editor_pick = EditorPick.find(params[:id])
    @editor_pick.destroy

    respond_to do |format|
      format.html { redirect_to editor_picks_url }
      format.json { head :no_content }
    end
  end
end
