class SalesController < ApplicationController
  
  before_filter :ensure_user_is_admin, except: :index
  before_filter :find_sale_page

  cache_sweeper :sales_sweeper, only: :update

  def index
    category_ids = params[:category_ids].present? ? params[:category_ids] : Category.all.collect(&:id)
    
    @products  = Product.approved_and_active.on_sale.filter_by_category(category_ids).page(params[:page]).per(9)  
    @products = filter_products(@products, params)
    @categories = Category.where{products.sale == true}.joins(:products)
    get_sizes
    
    @designers = User.designers(category_ids: category_ids)
      .joins(:products, :country)
      .where('products.sale' => true, 'products.active' => true, 'products.status' => Product::STATUS[:approved], 'users.designer_status' => User::STATUS[:approved])
      

    @countries = @designers.collect(&:designer_country)  
    @infinite_scrolling = params[:infinite_scrolling].present?

    @params = params
    @params.delete("page")
    @params.delete("infinite_scrolling")
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @products }
      format.js { render 'shared/filter_results' }
    end
  end

  def edit
    render :edit, layout: "dashboard_admin"
  end

  # PUT /sales/1
  # PUT /sales/1.json
  def update
    @sale = Sale.first

    respond_to do |format|
      if @sale.update_attributes(params[:sale])
        format.html { redirect_to sales_path, notice: 'Sale was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def find_sale_page
    #@sale = Sale.first || not_found
    @sale = Sale.first
    if @sale.blank?
      redirect_to root_path, notice: 'There are no sales at this time'
    end
  end

end

