class DesignerSweeper < AbstractSweeper
  observe User

  # this sweeper is reponsible for expiring the home page (the one with the map)

  def after_save(entity)
    expire_designers_page(entity)
  end

  def after_delete(entity)
    expire_designers_page(entity)
  end

  protected
  def expire_designers_page(entity)
    expire_designer(entity.id) if entity.is_designer?
  end

end