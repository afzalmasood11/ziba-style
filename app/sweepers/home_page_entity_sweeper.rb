class HomePageEntitySweeper < ActionController::Caching::Sweeper
  observe HomePageEntity, Product, Category, Country, User

  def after_save(entity)
    clear_cache_for_home_page(entity)
  end  

  def after_update(entity)
    clear_cache_for_home_page(entity)
  end  
  def after_create(entity)
    clear_cache_for_home_page(entity)
  end  

  def after_destroy(entity)
    clear_cache_for_home_page(entity)
  end  

  private
    def clear_cache_for_home_page(entity)     
      Rails.cache.delete("views/views_home_page_entities")
    end

end
