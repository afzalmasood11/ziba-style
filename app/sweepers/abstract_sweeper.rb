class AbstractSweeper < ActionController::Caching::Sweeper
  
  def expire_category(category_id)
    Rails.cache.clear
  end

  def expire_country(country_id)
    Rails.cache.clear
  end

  def expire_editor_pick(editor_pick_id)
    Rails.cache.delete("views/views_editor_picks_view_#{editor_pick_id}")
    expire_home_page
  end

  def expire_designer(designer_id)
    Rails.cache.delete("views/views_designers_show_#{designer_id}")
    #Rails.cache.delete("views/views_designers_visitor_show_#{designer_id}")
    expire_designers
  end

  def expire_product(product_id)
    Rails.cache.delete("views/views_products_show_#{product_id}")
  end

  def expire_designers
    Rails.cache.delete("views/views_designers_index")
    Rails.cache.delete("views/views_designers_style_directory")
  end

  def reset_navigation
    Rails.cache.delete("views/layout_navigation")
  end

  def expire_home_page
    Rails.cache.delete("views/views_countries_index")
  end

  def expire_countries
    expire_home_page
    expire_designers
    Rails.cache.delete("views/devise_sessions_new")
    Rails.cache.delete("views/views_devise_registrations_new_designer")
    Rails.cache.delete("views/views_devise_registrations_new_customer")
  end

  def expire_sales
    Rails.cache.delete("views/views_sales_index")
  end

end