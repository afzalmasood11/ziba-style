class CountrySweeper < AbstractSweeper
  observe Country

  def after_save(entity)
    expire_countries(entity)
  end

  def after_delete(entity)
    expire_countries(entity)
  end

  def expire_countries(entity)
    expire_country(entity.id)
  end

end
