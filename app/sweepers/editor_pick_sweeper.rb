class EditorPickSweeper < AbstractSweeper
  observe EditorPick

  def after_save(editor_pick)
    expire_cache_for(editor_pick)
  end

  def after_delete(editor_pick)
    expire_cache_for(editor_pick)
  end

  protected
  def expire_cache_for(editor_pick)
    expire_editor_pick(editor_pick.id)
  end

end