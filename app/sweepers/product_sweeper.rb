class ProductSweeper < AbstractSweeper
  observe Product 

  def after_save(product)
    clear_cache_for_product(product)
  end

  def after_update(product)
    clear_cache_for_product(product)
  end
  
  def after_destroy(product)
    clear_cache_for_product(product)
  end

  def clear_cache_for_product(product)     
    # let's be clever later
    Rails.cache.clear


    # expire_product(product.id)

    # #clear product's categories cache
    # product.categories.each do |category|
    #   expire_category(category.id)
    # end

    # #clear product's country cache
    # expire_country(product.designer.designer_country.id) if product.designer.designer_country

    # #clear sales page
    # expire_sales
  end
  
end
