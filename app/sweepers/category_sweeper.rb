class CategorySweeper < AbstractSweeper
  observe Category

  # this sweeper is reponsible for expiring the home page (the one with the map)

  def after_save(entity)
    expire_page(entity)
  end

  def after_delete(entity)
    expire_page(entity)
  end

  protected
  def expire_page(entity)
    expire_category(entity.id)
  end

end