class PageSweeper < AbstractSweeper
  observe Page

  def after_save(page)
    expire_cache_for(page)
  end

  def after_delete(page)
    expire_cache_for(page)
  end

  private
  def expire_cache_for(page)
    Rails.cache.delete("views/views_pages_show_#{page.name}")
  end

end