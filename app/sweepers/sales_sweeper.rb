class SalesSweeper < AbstractSweeper

  def after_save(entity)
    expire_sales
  end

end