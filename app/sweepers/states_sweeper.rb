class StatesSweeper < ActionController::Caching::Sweeper
  observe State

  def after_save
	expire_fragment("all_articles_json")
	Rails.cache.delete("views/all_articles_json")
  end  
end
