class Notifier < ActionMailer::Base
  default :from => 'ZIBA Style <support@ziba-style.com>',
          :return_path => 'contact@ziba-style.com',
          'Content-Transfer-Encoding' => '7bit'

  layout 'email'

  #DESIGNER NOTIFICATIONS

  def send_designer_account_is_pending_notification(designer)
    @designer = designer
    mail(:to => designer.email, :subject => "Your request to be a featured designer on ZIBA Style has been received");
  end


  def send_designer_new_order_notification(designer_order, designer)
    @designer_order = designer_order
    @designer = designer
    mail(:to => designer.email, :subject => "Order Notification: an order has been placed from your boutique")
  end

  def send_designer_account_has_been_approved_notification(designer)
    @designer = designer
    mail(:to => designer.email, :subject => "Welcome to ZIBA Style. Your designer account has been approved.")
  end

  def send_designer_boutique_has_been_approved_notification(designer)
    @designer = designer
    mail(:to => designer.email, :subject => "Your ZIBA Style boutique has been approved.")  do |format|
     format.html { render :send_designer_boutique_has_been_approved_notification, layout: false }
    end
  end

  def send_designer_product_has_been_approved_notification(designer, product)
    @designer = designer
    @product  = product
    mail(:to => designer.email, :subject => "Your product has been approved for display on ZIBA Style.")  do |format|
     format.html { render :send_designer_product_has_been_approved_notification, layout: false }
    end
  end

  def send_designer_account_has_become_editable(designer)
    @designer = designer
    mail(:to => designer.email, :subject => "Your boutique profile is ready to be edited.") do |format|
      format.html { render :send_designer_account_has_become_editable, layout: false }
    end
  end

  #CUSTOMER NOTIFICATIONS

  def send_customer_welcome_notification(customer)
    @customer = customer
    mail(:to => customer.email, :subject => "Welcome to ZIBA Style - the home of luxury goods from around the world.") do |format|
     format.html { render :send_customer_welcome_notification, layout: false }
    end
  end

  def send_invitation(invitation)
    @invitation = invitation
    mail(:to => invitation.email, :subject => "#{invitation.from_user.fname} #{invitation.from_user.lname} has invited you to join ZIBA Style - the online destination for global luxury shopping.")  do |format|
     format.html { render :send_invitation, layout: false }
    end
  end

  def send_product_invitation(invitation)
    @invitation = invitation
    @product = Product.find(invitation.product_id)
    mail(:to => invitation.email, :subject => "#{invitation.from_user.fname} #{invitation.from_user.lname} has shared a style with you on ZIBA Style.")
  end

  def send_product(invitation)
    @invitation = invitation
    @product = Product.find(invitation.product_id)
    mail(:to => invitation.email, :subject => "#{invitation.from_user.fname} #{invitation.from_user.lname} has shared a style with you on ZIBA Style.")
  end 

  def send_customer_new_order_notification(order, customer)
    @order = order
    @customer = customer
    mail(:to => customer.email, :subject => "ZIBA Style Order Confirmation (Order # #{order.id}).")
  end

  def send_customer_new_credit_notification(credit)
    @credit = credit
    @customer = credit.user
    mail(:to => @customer.email, :subject => "Your friend is in. You have received a $20 credit on ZIBA Style!") do |format|
     format.html { render :send_customer_new_credit_notification, layout: false }
    end
  end

  def notify_customer_designer_order_shipped(designer_order)
  	@designer_order = designer_order
    mail(:to => designer_order.order.customer.email, :subject => "ZIBA Style Shipment Notification (Order # #{designer_order.order.id}).")  	
  end

  #ADMIN NOTIFICATIONS

  def send_admin_new_designer_signup_notification(designer, admin)
    @designer = designer
    @admin = admin
    mail(:to => admin.email, :subject => "A designer has submitted an application to join ZIBA Style.")
  end

  def send_admin_new_boutique_designer_to_approve_notification(designer, admin)
    @designer = designer
    @admin = admin
    mail(:to => admin.email, :subject => "A designer has submitted a boutique for approval.")
  end

  def send_admin_new_product_designer_to_approve_notification(product, admin)
    @designer = product.designer
    @product  = product
    @admin = admin
    mail(:to => admin.email, :subject => "A designer has submitted a product for approval.")
  end

  def send_admin_new_order_notification(order, admin)
    @order = order
    @admin = admin
    mail(:to => admin.email, :subject => "Order Notification: a new order has been placed.")
  end

  #ADMIN - CONTACT - NOTIFICATIONS

  def send_admin_new_message_contact_us_notification(contact, admin)
    @contact = contact
    @admin = admin
    mail(:to => admin.email, :subject => "Someone has submitted a Contact Us form.")
  end

  def send_admin_new_message_engage_with_us_notification(contact, admin)
    @contact = contact
    @admin = admin
    mail(:to => admin.email, :subject => "Someone has submitted an Engage with Us form.")
  end

  def send_admin_new_message_request_invitation_notification(contact, admin)
    @contact = contact
    @admin = admin
    mail(:to => admin.email, :subject => "Someone has requested an invitation to join ZIBA Style.") do |format|
     format.html { render :send_admin_new_message_request_invitation_notification, layout: false }
    end
  end

  def send_admin_new_message_request_deactivate_boutique_notification(designer, admin)
    @designer = designer
    @admin = admin
    mail(:to => admin.email, :subject => "A designer has requested to deactivate a boutique.")
  end  
  
  def send_admin_new_message_request_activate_boutique_notification(designer, admin)
    @designer = designer
    @admin = admin
    mail(:to => admin.email)
  end

end
