class Contact < ActiveRecord::Base
  TYPE = {contact_us: 1, request_invitation: 2, engage_with_us: 3}
  DISCOVER = [["Online", 1], ["In a boutique", 2], ["In a magazine", 3], ["I know the designer", 4], ["Other", 5]]  

  attr_accessible :email, :contact_type, :fname, :lname, :phone, :country_id, :order_number, :message, :discover_product, :link

  belongs_to :country

  validates_presence_of :contact_type
  validates_presence_of :fname, :lname, :country_id, :message, :if => :should_validate_contact_us_or_engage_with_us?
  validates_presence_of :phone, :order_number, :if => :should_validate_contact_us?
  validates :email, :presence => true, :email => true,                                :if => :should_validate_contact_us_or_request_invitation?

  def should_validate_contact_us?
    self.contact_type == TYPE[:contact_us]
  end

  def should_validate_request_invitation?
    self.contact_type == TYPE[:request_invitation]
  end

  def should_validate_engage_with_us?
    self.contact_type == TYPE[:engage_with_us]
  end

  def should_validate_contact_us_or_engage_with_us?
    should_validate_contact_us? || should_validate_engage_with_us?
  end

  def should_validate_contact_us_or_request_invitation?
    should_validate_contact_us? || should_validate_request_invitation?
  end

end
