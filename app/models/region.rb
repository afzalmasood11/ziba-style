class Region < ActiveRecord::Base

  has_many :countries

  # should return all regions that have [countries that have designers [ that are active that have [products that are approved]]]
  def self.with_designers

    Region.where{countries.id.in( 
      User.designers({designer_active: true}).joins(:products, :designer_country)
        .where{products.status == my{Product::STATUS[:approved]}}
        .group(:designer_country_id)
        .select{users.designer_country_id}
        .collect( &:designer_country_id ))}.order(:name)
        .includes(countries: :active_designers)

  end
end
