class Size < ActiveRecord::Base
	belongs_to :category
  has_many :items
	serialize :other_values # list of international sizes, e.g. {IT: 34, UK: 2, FR: 32...}
end
