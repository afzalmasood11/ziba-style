class FileAttachment < ActiveRecord::Base
  # CONSTANTS
  ENTITY_TYPE = {user: 1, product: 2, country: 3}
  TYPE        = {banner: 1, profile: 2, portfolio: 3, product_picture: 4, homepage_picture: 7, celebrity_picture: 5, designer_index: 10, country_history_picture: 6}
  STYLES      = {
    product_picture:
        { crop_template: "960x960", mini: "40x50#", cart_popup: "70x80#", small: "150x150#", medium: "310x325#", catalog: "260x330#", popup: "960x700>" },
    celebrity_picture:
        #{ crop_template: "960x960", mini: "40x40#", small: "150x150#", as_seen_on: "142x82", medium: "320x400#", catalog: "260x330#", popup: "800x500#" },
        { crop_template: "960x960", mini: "40x40#", small: "150x150#", as_seen_on: "165x115#", medium: "320x400#", catalog: "260x330#", popup: "800x500#" },
    default:
        { crop_template: "960x960", small: "150x150#", profile: "150x150#", banner: "960x180#" }
  }

#  attr_accessible :attachment_processing, :attachment, :file_attachments, :attachment_type, :attachment_primary, :meta, :crop_x, :crop_y, :crop_w, :crop_h, :style_to_crop, :crop_rules
  attr_accessible :attachment, :file_attachments, :attachment_type, :attachment_primary, :meta, :crop_x, :crop_y, :crop_w, :crop_h, :style_to_crop, :crop_rules

  serialize :crop_rules, Hash

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h, :style_to_crop

  after_save :reprocess_attachment
  # the above reprocess_attachment __should__ result in cropping the attachment correctly
  # if not, the next line should be adapted to do the job
  #after_update :reprocess_avatar, :if => :cropping?

  belongs_to :entity, polymorphic: true

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end

  # used by the custom cropper.rb so that custom cropping logic is only applied to 1 style at a time
  def crop_target_geometry
    if self.style_to_crop.blank?
      logger.debug "style_to_crop was blank"
      return ""
    end
    if self.attachment.nil? or self.attachment.instance.attachment_type.nil?
      logger.debug "attachment or attachment_type was blank:("
      return ""
    end

    # get the name (a symbol) for the this attachment's type
    type = TYPE.invert[self.attachment.instance.attachment_type]
    # get the string of the geometry rule for the style that is undergoing cropping
    geometry = STYLES[type][self.style_to_crop.to_sym]

    geometry
  end

  #only to used by for crop_studio for cropping product_picture and celebrity_picture styles
  def attachment_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(attachment.path(style))
  end

  # Paperclip, see: https://github.com/thoughtbot/paperclip
  # has_attached_file :attachment , :styles => { :mini => "40x56>", :small => "150x150>", :medium => "300x300>", :banner => "950x120" } #need ImageMagick
  has_attached_file :attachment ,
    storage: :s3,
    s3_protocol: Rails.env.production? ? :https : :http,
    bucket: ZIBAStyle::Application.config.s3_bucket,
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    :processors => [:cropper],

    # :att_type is set up in config/initializers/extend_paperclip.rb
    # adds support for the :att_type interpolation which looks at the FileAttachment's attachment_type
    # so that this can be added to generated path
    # this creates unique URIs for celebrity_picture 1 :banner, and picture 1 :banner
    :path => "/#{Rails.env}/:att_type/:id/:style/:filename",

    :styles => lambda { |attachment|

      # because paperclip processes attachments before the model gets saved,
      # we are unable to access attachment.instance.attachment_type if we are saving the attachment for the first time
      # i.e., if we're saving pic1 for product1 for the first time, at the time this lambda is executed
      # we will not yet have access to attachment.instance.attachment_type
      # the attachment_type is nil, in this case: so we return an empty hash
      # this has the effect of telling paperclip "do no image processing at this point"
      # after save we call reprocess_attachment, which tells paperclip to redo the image generation logic
      # this time, the attachment will exist and the correct :styles hash will be return by the lambda
      unless attachment.instance.attachment_type.nil?
        if TYPE[:product_picture].to_s == attachment.instance.attachment_type.to_s
          STYLES[:product_picture]
        elsif TYPE[:celebrity_picture].to_s == attachment.instance.attachment_type.to_s
          STYLES[:celebrity_picture]
        else
          STYLES[:default]
        end
      else
        {}
      end
  }

  #validates_attachment_size :attachment,  :in => 0..4.megabytes

  # See: http://stackoverflow.com/questions/1573085/how-can-i-resize-a-paperclip-image-after-it-has-been-added-to-the-database
  # note this adds some slowness, e.g. when seeding or creating fake products. might want to turn off for that.
  def reprocess_attachment
    self.attachment.reprocess! unless self.attachment.to_s.include?('missing')
  end

  def product
    entity
  end

  def get_height
    geo = Paperclip::Geometry.from_file(attachment.to_file(:original))
    geo.height
#    height = 200 # just for testing

    #for setting min height/width
#    ratio = geo.width/geo.height
#
#    min_width  = 150
#    min_height = 119
#
#    if ratio > 1
#      # Horizontal Image
#      final_height = min_height
#      final_width  = final_height * ratio
#      "#{final_width.round}x#{final_height.round}!"
#    else
#      # Vertical Image
#      final_width  = min_width
#      final_height = final_width * ratio
#      "#{final_height.round}x#{final_width.round}!"
#    end
#    final_width
  end

end
