class Invitation < ActiveRecord::Base
	belongs_to :from_user, :class_name => 'User'
	belongs_to :to_user,   :class_name => 'User'

	validates_uniqueness_of :token
	validates_presence_of :email
	validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }
	# OLD - an email can receive several invitations
	#validate :ensure_unknown_email, on: :create 
	# def ensure_unknown_email
	# 	unless User.find_by_email(self.email).blank?
	# 		errors.add :email, "Email address already in the system"	
	# 	end
	# end

	def self.generate_token
		o = [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten;
		(0..50).map{ o[rand(o.length)]  }.join;
	end

end
