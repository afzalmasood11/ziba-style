class HomePageEntity < ActiveRecord::Base
	belongs_to :entity, polymorphic: true

  has_attached_file :homepage_image, 
    storage: :s3,
    s3_protocol: Rails.env.production? ? :https : :http,
    bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    path: "/#{Rails.env}/homepageentity/:id/homepage/:filename"

	before_save :set_order

  HOMEPAGE_PHRASES = [ 
    "READY TO EXPLORE?",
    "LAUNCH YOUR JOURNEY",
    "SHOP THE WORLD",
    "DISCOVER LOCAL LUXURY",
    "EXCLUSIVE ACCESS",
    "GLOBETROT IN STYLE",
    "DESTINATION SHOPPING",
    "GLOBAL FINDS",
  ]

	def set_order
		unless self.order
			last_entity = HomePageEntity.order(:order).last
			self.order = last_entity ? last_entity.order + 1 : 1
		end    
	end

  def get_height
    geo = Paperclip::Geometry.from_file(homepage_image.to_file(:original))
    geo.height
#    300 #just for test
  end
#
#  def img_height
#    self.height
#  end

end
