class Feedback < ActiveRecord::Base
	belongs_to :designer_order

	validates_presence_of :rating, :comment 

end
