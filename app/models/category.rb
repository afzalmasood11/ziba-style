class Category < ActiveRecord::Base
  extend FriendlyId

  belongs_to :category # for parent relationship
	has_many :subcategories, class_name: :Category # for child relationship
	has_many :categories # for child relationship
  has_many :products, through: :product_categories
	has_many :product_categories 
	has_many :sizes
  
  #friendly_id :name, use:[:slugged, :history]
  friendly_id :name, :use => :scoped, :scope => :category_id
  
  GENDER_TYPES = { women: 1, men: 2, both: 3 }

	# Paperclip, see: https://github.com/thoughtbot/paperclip    
  has_attached_file :banner, :styles => { :small => "150x150>", :banner => "960x180#" },
    storage: :s3,
    s3_protocol: Rails.env.production? ? :https : :http,
    bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    :path => "/#{Rails.env}/category/banner/:style/:filename"
  
  has_attached_file :style_directory_image, :styles => { :small => "150x150>", :default => "220x270#" },
    storage: :s3,
    bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_protocol: Rails.env.production? ? :https : :http,
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    :path => "/#{Rails.env}/category/style_dir/:style/:filename"    

  has_attached_file :size_chart_man, 
    storage: :s3,
    s3_protocol: Rails.env.production? ? :https : :http,
    bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    :path => "/#{Rails.env}/category/:id/size_chart/man/:filename"

  has_attached_file :size_chart_woman, 
    storage: :s3,
    s3_protocol: Rails.env.production? ? :https : :http,
    bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    :path => "/#{Rails.env}/category/:id/size_chart/woman/:filename"

  has_attached_file :size_chart_unisex, 
    storage: :s3,
    s3_protocol: Rails.env.production? ? :https : :http,
    bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    :path => "/#{Rails.env}/category/:id/size_chart/unisex/:filename"

  has_attached_file :homepage_image, 
    storage: :s3,
    s3_protocol: Rails.env.production? ? :https : :http,
    bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    path: "/#{Rails.env}/category/:id/homepage/:filename"

  # Sphinx Index
  define_index do
    indexes :name, :sortable => true
  end

  # -- No longer used --
  # def self.subcategories(gender = :women)
  #   unless gender
  #     gender = is_numeric? gender ? gender : GENDER_TYPES[gender]
  #     where{(category_id != nil) & (gender_type.in([gender, GENDER_TYPES[:both]]))}
  #   else
  #     where{(category_id != nil) & (gender_type.in([GENDER_TYPES[:women], GENDER_TYPES[:both], GENDER_TYPES[:men]]))}    
  #   end
  # end 

  # alias for parent relationship (no gender required, since only one parent exists)
	def parent_category
		category
	end

  # TODO: duplicated from app controller, should be refactored into a helper and then included in this model and in app controller
  def is_numeric?(s)
    !!Float(s) rescue false
  end
  def self.is_numeric?(s)
    !!Float(s) rescue false
  end

  def gender_type
    self.sizes.select("gender_type").group("gender_type").uniq().collect{|s| s.gender_type }
  end

	def relevant_sizes(gender = :women)
    if is_numeric? gender
      cond = {gender_type: gender}
    else
      cond = {gender_type: GENDER_TYPES[gender]}
    end

    # cond = {gender_type: GENDER_TYPES[gender]}
		sizes.empty? ? parent_category.sizes.where(cond) : sizes.where(cond)
	end

  def self.top_levels(gender = :women)
    gender_cond = Category.is_numeric?(gender) ? [GENDER_TYPES[:both], gender] : [GENDER_TYPES[:both], GENDER_TYPES[gender]]
    Category.where(id: Category.with_products(gender_cond).select{categories.category_id}.collect( &:category_id ).uniq ).where{ (category_id == nil) }
  end

  def self.with_products(gender)
    where(id: Category.active_category_ids(gender).uniq )
  end

  def self.active_category_ids(gender)
    User.designers({designer_active: true})
        .joins(products: [items: [:size]])
        .joins(products: :product_categories)
        .where{(products.active == true) & (products.status == my{Product::STATUS[:approved]}) & (items.quantity > 0) & (sizes.gender_type.in( my{ gender } ))}
        .group{product_categories.category_id}
        .select{product_categories.category_id}
        .collect( &:category_id )
  end

  def subcategories_with_product(gender)
    Category.joins{products.items.size}.where{ (sizes.gender_type.in(my{ [gender, GENDER_TYPES[:both]] })) & (items.quantity > 0) & (categories.category_id == my{self.id}) & (products.active == true) & (products.status == my{Product::STATUS[:approved]}) }.uniq
  end


  def self.featured_in_style_directory
    where{ featured_in_style_directory == true }
  end

end
