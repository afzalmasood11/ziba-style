class DesignerOrder < ActiveRecord::Base

  SHIPPING_STATUS = {pending: 0, shipped: 1}

	belongs_to :designer, :class_name => 'User'
	belongs_to :order

	has_many :order_items, :dependent => :destroy
	accepts_nested_attributes_for :order_items 

	has_one :feedback	

  def total
    self.order_items.collect{|o_i| ((o_i.price||=0)+(o_i.shipping_cost||=0))*(o_i.quantity||=0) }.flatten.sum
  end

  def tax_rate
    # if national shipping
    if self.order.shipping_country_id == self.designer.designer_country_id 
      #if country is USA
      if self.designer.designer_country_id == ::USA_COUNTRY_ID
        # and states are the same apply local taxes
        if self.designer.designer_state_id and self.order.shipping_state.downcase == self.designer.designer_state.code.downcase
          self.designer.designer_tax_rate
        else # else: no tax
          0
        end
      else #if country is not US, apply specified tax
        self.designer.designer_tax_rate
      end
    else #international shipping
      self.designer.designer_int_tax_rate
    end
  end  

  def total_tax
    self.order_items.collect{|o_i| (o_i.price||=0)*(o_i.quantity||=0) }.flatten.sum*tax_rate
  end

  def total_with_tax
    total+total_tax
  end

  def number_of_items
    self.order_items.count
  end

  def products
    self.order_items.collect{|o_i| o_i.item.product }.flatten.uniq
  end

  def max_lead_days
    self.products.collect{|p| p.lead_days }.sort.last || 0
  end

end
