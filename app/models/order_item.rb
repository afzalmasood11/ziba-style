class OrderItem < ActiveRecord::Base
	belongs_to :designer_order
	belongs_to :item
  has_many :returns
	
  validates_numericality_of :quantity, :only_integer =>true, :greater_than_or_equal_to =>1, :message => "quantity has to be at least one"

	def self.find_by_order_id_and_item_id(order_id, var_item_id)
		order = Order.find(order_id)
 		OrderItem.where{(designer_order_id.in(order.designer_orders.collect{|d_o| d_o.id })) & (item_id == var_item_id)}.first
	end

  def check_stock
    self.item.quantity >= self.quantity
  end

  def remove_from_stock
    if self.check_stock
      unless self.item.product.is_custom  
        self.item.update_attributes(quantity: self.item.quantity-self.quantity)
      else
        true # if item.product is custom made, we don't decrease the stock (there is physically no stock)
      end
    else
      false
    end
  end

  def rollback_stock
    self.item.update_attributes(quantity: self.item.quantity+self.quantity)
  end

  def total
    (self.quantity||=0)*((self.price||=0)+(self.shipping_cost||=0))
  end

  def tax_rate
    # if national shipping
    if self.designer_order.order.shipping_country_id == self.designer_order.designer.designer_country_id 
      #if country is USA
      if self.designer_order.designer.designer_country_id == ::USA_COUNTRY_ID
        # and states are the same apply local taxes
        if self.designer_order.designer.designer_state_id and self.designer_order.order.shipping_state.downcase == self.designer_order.designer.designer_state.code.downcase 
          self.designer_order.designer.designer_tax_rate
        else # else: no tax
          0
        end
      else #if country is not US, apply specified tax
        self.designer_order.designer.designer_tax_rate
      end
    else #international shipping
      self.designer_order.designer.designer_int_tax_rate
    end
  end

  def total_tax
    (self.quantity*self.price)*tax_rate
  end

  def total_with_tax
    total+total_tax
  end

  def number_of_returnable_items
    self.quantity - self.returns.collect{|r| r.quantity }.sum
  end 

end