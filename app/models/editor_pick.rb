class EditorPick < ActiveRecord::Base

  belongs_to :product
	
  has_attached_file :image, :styles => { :large => "960x608" },
    storage: :s3,
    bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    s3_protocol: Rails.env.production? ? :https : :http,
    :path => "/#{Rails.env}/editor_pick/image/:style/:filename"    

  has_attached_file :thumbnail,  :styles => { :thumb => "165x115#" },
    storage: :s3,
    bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    s3_protocol: Rails.env.production? ? :https : :http,
    :path => "/#{Rails.env}/editor_pick/thumbnail/:style/:filename"    

  validates_presence_of :product_id, message: "Product ID cannot be blank"

  after_destroy :touch_all

  def touch_all
    # for busting cache
    EditorPick.all.each {|ep| ep.touch }
  end
	
	def previous(offset = 0)    
    prev_pick = self.class.joins(:product).first(
        :conditions => ['editor_picks.id < ? AND products.active = ? AND products.status = ?', self.id, true, Product::STATUS[:approved]], 
        :limit => 1, 
        :offset => offset, 
        :order => "id DESC"
      )
    # prev_pick.nil? ? self.class.last : prev_pick
  end

  def next(offset = 0)
    next_pick = self.class.joins(:product).first(
        :conditions => ['editor_picks.id > ? AND products.active = ? AND products.status = ?', self.id, true, Product::STATUS[:approved]], 
        :limit => 1, 
        :offset => offset, 
        :order => "id ASC"
      )
    # next_pick.nil? ? self.class.first : next_pick 
  end

  def self.active
    self.joins(:product).where{(products.active == true) & (products.status == Product::STATUS[:approved])}
  end
	
end
