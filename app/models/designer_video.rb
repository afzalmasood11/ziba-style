class DesignerVideo < ActiveRecord::Base

  STATUS = { pending: 0, approved: 1 }

  validates_presence_of :name, :url, :designer_id, :status

  # this allows us to attach the oembed info to the model
  attr_accessor :oembed_resource
end
