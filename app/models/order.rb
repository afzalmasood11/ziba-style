class Order < ActiveRecord::Base

	STATUS = {cart: 0, pending_checkout: 1, complete: 2}
  COMPLETED_STATUS = [STATUS[:complete]]
  PLATFORM = {braintree: 1, paypal: 2, wepay: 3}

	validates_presence_of :shipping_fname, :shipping_lname, :shipping_address1, :shipping_zipcode, :shipping_city,   :shipping_country, :shipping_phone, if: :validate_shipping
	validates_presence_of :billing_fname,  :billing_lname,  :billing_address1,  :billing_zipcode,  :billing_country, :billing_city,     :billing_phone,  if: :validate_billing
	validate :validate_card, :validate_stock, :wepay_validate_card, if: :validate_billing
	validate :validate_ships_to_country, if: :validate_shipping

	validate :validate_shipping_state, if: :validate_shipping
	validate :validate_billing_state,  if: :validate_billing

	belongs_to :customer, :class_name => 'User'
	belongs_to :shipping_country, :class_name => 'Country'
	belongs_to :billing_country, :class_name => 'Country'

	has_many :designer_orders, :dependent => :destroy
	has_many :credits
	has_many :unused_credits, class_name: :Credit, conditions: { used: false }


	accepts_nested_attributes_for :designer_orders 
	accepts_nested_attributes_for :credits
	
	attr_accessor :credit_card_type, :credit_card_number, :credit_card_verification_number, :credit_card_first_name, :credit_card_last_name, :billing_as_shipping, :validate_shipping, :validate_billing, :paypal_payment, :validate_shipping, :apply_credit

	def items
	  # Item.joins(:order_items => :designer_order)
	  # 		.select('`items`.*, `designer_orders`.`id` as designer_order_id, `designer_orders`.`shipping_status` as designer_order_shipping_status, `designer_orders`.`ups_tracking` as designer_order_ups_tracking, `order_items`.`price` as order_item_price, `order_items`.`quantity` as order_item_quantity' )
	  # 		.where('designer_orders.order_id' => self.id)

	  Item.joins(:order_items => :designer_order)
	  		.select('`items`.*, `designer_orders`.`id` as designer_order_id, `designer_orders`.`shipping_status` as designer_order_shipping_status, `designer_orders`.`ups_tracking` as designer_order_ups_tracking, `order_items`.`price` as order_item_price, `order_items`.`quantity` as order_item_quantity' )
	  		.where('designer_orders.order_id' => self.id)
	end

  def return_days_left
   a=Time.now.strftime("%Y-%m-%d")
   b=self.processed_at.strftime("%Y-%m-%d")
   a=Date.parse(a)
   b=Date.parse(b)
   (a-b).to_i
  end

	def designers
		User.joins(:designer_orders).where('designer_orders.order_id' => self.id)
	end

	def short_description
		description = []
		self.designer_orders.collect{|d_o| d_o.order_items }.flatten.each do |o_i|
			description << "#{o_i.item.product.name} (size: #{o_i.item.size.us_value} | qte: #{o_i.quantity})"
		end
		description.join("\n")
	end

	def process_checkout(params)
		stock = self.check_stock
		if stock and !stock.kind_of?(Hash)
			stock_errors = []
			self.designer_orders.collect{|d_o| d_o.order_items }.flatten.each do |o_i|
				 stock_errors << o_i.item_id unless o_i.remove_from_stock
			end
			if stock_errors.empty?


				# BRAINTREE payement
				# payment_result = self.process_payment(params)
				# if payment_result.success?
				#   puts "success!: #{payment_result.message}"
				#   platform = (params.include? :express_payer_id and params.include? :express_token) ? Order::PLATFORM[:paypal] : Order::PLATFORM[:braintree]
				# 	self.update_attributes({status: Order::STATUS[:complete], processed_at: Time.now, braintree_transaction_id: payment_result.authorization, transaction_platform: platform, braintree_vault_id: self.customer.braintree_customer_id}) 				
				# 	self.unused_credits.each{ |c| c.mark_has_used  }
				# else
				# 	errors.add :credit_card, payment_result.message
				#   logger.debug payment_result.message
				# end
				
				payment_result = self.process_wepay_payment(params)
				if payment_result[:state] == "authorized"
					puts "success!: #{payment_result.inspect}"
					platform = (params.include? :express_payer_id and params.include? :express_token) ? Order::PLATFORM[:paypal] : Order::PLATFORM[:wepay]
					self.update_attributes({status: Order::STATUS[:complete], processed_at: Time.now, transaction_id: payment_result[:checkout_id], transaction_platform: platform, transaction_cc: self.customer.wepay_credit_card_id}) 				
					self.unused_credits.each{ |c| c.mark_has_used  }
				else
					errors.add :credit_card, payment_result[:state]
					logger.debug payment_result[:state]
				end
			else 
				stock_errors
			end
		else
			errors.add "Items", "Some items are not available"
			false
		end
	end

	def process_payment(params)
		if params.include? :express_payer_id and params.include? :express_token and params.include? :ip_address
			self.customer.express_payer_id = params[:express_payer_id]
			self.customer.express_token 	 = params[:express_token]
			self.customer.ip_address 			 = params[:ip_address]
			PPGATEWAY.purchase(price_in_cents, self.customer.paypal_purchase_options)
		else
			BTGATEWAY.purchase(price_in_cents, self.customer.braintree_customer_id)
		end
	end	

	def process_wepay_payment(params)
		wepay_conf = YAML::load(File.open("#{Rails.root}/config/wepay.yml"))
		wepay_params = {
		  account_id:wepay_conf[Rails.env]["account_id"],
		  short_description: self.short_description,
		  type:"GOODS",
		  fee_payer: "payee",
		  amount: self.price_in_dollars,
		  payment_method_id: self.customer.wepay_credit_card_id,
		  payment_method_type: "credit_card"
		}
		gateway = WepayRails::Payments::Gateway.new
		gateway.call_api('/checkout/create', wepay_params)
	end

	def check_stock
		return_items = {}
		self.designer_orders.collect{|d_o| d_o.order_items }.flatten.each do |o_i| 
			return_items[o_i.item_id] = o_i.item.quantity unless o_i.check_stock
		end
		return_items.empty? || return_items
	end

	def validate_stock
		self.errors.add :order, "Some items are not in stock" if self.check_stock.kind_of?(Hash)
	end

	def number_of_items
		self.designer_orders.collect{|d_o| d_o.order_items.collect{|o_i| o_i.quantity.to_i } }.flatten.sum
	end

	def total	#total - no tax
		self.designer_orders.collect{|d_o| d_o.total }.sum
	end
	
	def total_tax #total of the tax
		self.designer_orders.collect{|d_o| d_o.total_tax }.sum
	end

	def total_with_tax #total + tax
		self.designer_orders.collect{|d_o| d_o.total_with_tax }.sum
	end

	def ziba_rate
		7.00/100.00
	end

	#PAYPAL STUFF

	def paypal_setup(token)
	  details = PPGATEWAY.details_for(token)
	  self.customer.express_payer_id = details.payer_id
	  self.customer.express_token = token
	  self.billing_fname = details.params["first_name"]
	  self.billing_lname = details.params["last_name"]
	  self.billing_address1 = details.params["street1"]
	  self.billing_address2 = details.params["street2"]
	  self.billing_zipcode = details.params["postal_code"]
	  self.billing_state = details.params["state_or_province"]
	  self.billing_city = details.params["city_name"]
	  self.billing_country = Country.find_or_create_by_name(details.params["country_name"])
	  self.billing_phone = details.params["phone"]
	  self.save
	end

	def price_in_dollars
		credit = self.unused_credits.collect{|c| c.amount }.sum || 0
		total_with_tax - credit
	end

  private

		def price_in_cents
			credit = self.unused_credits.collect{|c| c.amount }.sum || 0
			(total_with_tax*100).round - credit
		end

		def validate_card
			activemerchant_field_to_our_field_mapping = {
			  :number 							=> :credit_card_number,
			  :type 								=> :credit_card_type,
			  :first_name 					=> :credit_card_first_name,
			  :last_name 						=> :credit_card_last_name,
			  :month 								=> :credit_card_expiration_date,
			  :year 								=> :credit_card_expiration_date,
			  :issue_number 				=> nil,
			  :verification_value 	=> :credit_card_verification_number
			}
			unless credit_card.valid?
			  credit_card.errors.each do |field, message|
			    errors.add activemerchant_field_to_our_field_mapping[field.to_sym], message if activemerchant_field_to_our_field_mapping[field.to_sym].present?
			  end
			end
			true #important!
		end

		def credit_card
			@credit_card ||= ActiveMerchant::Billing::CreditCard.new(
			  :type               => credit_card_type,
			  :number             => credit_card_number,
			  :verification_value => credit_card_verification_number,
			  :month              => credit_card_expiration_date.month,
			  :year               => credit_card_expiration_date.year,
			  :first_name         => credit_card_first_name,
			  :last_name          => credit_card_last_name
			)
		end

		def wepay_credit_card
			wepay_conf = YAML::load(File.open("#{Rails.root}/config/wepay.yml"))
			{
			  client_id: wepay_conf[Rails.env]["client_id"],
			  user_name: "#{credit_card_first_name} #{credit_card_last_name}",
			  email: self.customer.email,
			  cc_number: credit_card_number,
			  cvv: credit_card_verification_number,
			  expiration_month: credit_card_expiration_date.month,
			  expiration_year: credit_card_expiration_date.year,
			  address:
			  {
			    address1: self.billing_address1,
			    address2: self.billing_address2,
			    city: self.billing_city,
			    state: self.billing_state,
			    country: "US",
			    zip: self.billing_zipcode
			  }
			}
		end

		def wepay_validate_card

			gateway = WepayRails::Payments::Gateway.new
			begin
				result = gateway.call_api('/credit_card/create', wepay_credit_card)
			rescue Exception=>e
				result = nil
		    errors.add :credit_card,  e.to_s
			end

			if result
				self.customer.wepay_credit_card_id = result[:credit_card_id]
				self.customer.save
			end

			true #important!
		end

		def validate_card_and_connect_to_gateway
			if self.validate_billing
				gateway = WepayRails::Payments::Gateway.new
				begin
					result = gateway.call_api('/credit_card/create', wepay_credit_card)
				rescue
					result = nil
				end
				if result
					self.customer.wepay_credit_card_id = result[:credit_card_id]
					self.customer.save
				end
			end
		end

		def validate_ships_to_country

			invalid_products = Product.joins(:product_excluded_countries, items: [:order_items => :designer_order])				
				.where(designer_orders: { order_id: self.id }, product_excluded_countries: { excluded_country_id: self.shipping_country_id })

			# TODO: optimize start from Designer
			
			unless invalid_products.empty?
				errors.add "Items", "#{invalid_products.collect(&:name).join(', ')} cannot ship to your country"
				return false
			end
			return true			
		end


		def validate_shipping_state
			states = State.find_all_by_country_id(self.shipping_country_id)
			if states and self.shipping_state_id
				unless states.collect(&:id).include?(self.shipping_state_id)
					errors.add "State", "Please select a state for this country (shipping)"
					return false
				else
					self.shipping_state = states[states.index{|x| x.id == self.shipping_state_id }].code
				end
			end
			return true
		end

		def validate_billing_state
			states = State.find_all_by_country_id(self.billing_country_id)

			if states and self.billing_state_id
				unless states.collect(&:id).include?(self.billing_state_id.to_i)
					errors.add "State", "Please select a state for this country (billing)"
					return false
				else
					self.billing_state = states[states.index{|x| x.id==self.billing_state_id }].code
				end
			end
			return true
		end
end