class Credit < ActiveRecord::Base
  belongs_to :user
  belongs_to :from_user, class_name: :User , foreign_key: :referring_user_id

  validates_uniqueness_of :code

  before_create :generate_code
  before_create :default_value

  def mark_has_used
    self.used = true
    self.save
  end

  private

  def default_value
    self.amount = 20 unless self.amount
    self.used = 0 unless self.used
  end

  def generate_code
    self.code  =  Credit::generate_code_key
    while Credit.find_by_code(self.code) != nil
      self.code = Credit::generate_code_key
    end
  end

  def self.generate_code_key
    o = [('a'..'z'),('A'..'Z'),(1..9)].map{|i| i.to_a}.flatten;
    (0..10).map{ o[rand(o.length)]  }.join;
  end

end
