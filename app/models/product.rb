class Product < ActiveRecord::Base
  extend FriendlyId
  
  NUMBER_OF_PICTURES = 4
  NUMBER_OF_CELEBRITY_PICTURES = 3

  before_create :default_values
  after_commit  :init_sizes

  STATUS = { deleted: 0, pending: 1, pending_approval: 2, approved: 3 }
  PRICE_MAX = 9999999
  PRICES = [{min: 0, 	 max: 100, label: '$0-100'}, 
    {min: 100, max: 200, label: '$100-200'},
    {min: 200, max: 300, label: '$200-300'},
    {min: 300, max: PRICE_MAX, label: '$300+'},
  ]

  has_many :categories, through: :product_categories
  has_many :primary_categories, source: :category, through: :product_categories, conditions: { 'product_categories.primary' => true }
  has_many :product_categories
  has_many :excluded_countries, through: :product_excluded_countries, class_name: :Country
  has_many :product_excluded_countries
  has_many :items	
  has_many :pictures, class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:product_picture], attachment_primary: false }, as: :entity, dependent: :destroy
  has_many :celebrity_pictures, class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:celebrity_picture], attachment_primary: false }, as: :entity, dependent: :destroy
  has_one  :primary_image,  class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:product_picture], attachment_primary: true }, as: :entity, dependent: :destroy
  has_one  :homepage_image, class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:homepage_picture] }, as: :entity, dependent: :destroy

  has_many :editor_picks, dependent: :destroy

  belongs_to :designer, class_name: :User

  accepts_nested_attributes_for :categories, :pictures, :celebrity_pictures, :items, :primary_image, :homepage_image

  attr_accessor :product_request_approval, :product_back_to_pending, :submitting_shipping_info, :return_days_option

  friendly_id :name, use: [:slugged, :history]

  # Sphinx Indexing
  define_index do
    indexes :name, :sortable => true
    indexes :product_measurements
    indexes :short_description
    indexes :inspiration
    indexes :materials

    has designer_id, created_at, updated_at
  end

  validates_presence_of :name, :gender_type, :product_measurements, :inspiration, :materials, :short_description
  validates_numericality_of :price

  # conditional validation applied only to shipping data
  validates_presence_of     :weight, :weight_type, :return_days, :lead_days, :if => :should_validate_shipping_values?
  validates_numericality_of :weight, :lead_days, :if => :should_validate_shipping_values?

  def relevant_sizes 
    return nil if primary_category.nil?
    #product should always be linked to a subcat; but we check if the sizing chart applies to the parent cat (e.g. "Apparel")		
    sizes = Size.where(category_id: primary_category.id, gender_type: gender_type)
    sizes = Size.where(category_id: primary_category.category.id, gender_type: gender_type) if sizes.blank? and primary_category.category
    sizes
  end

  def init_sizes
    if self.relevant_sizes and (self.items.blank? or self.categories.first.id != self.items.first.size.category.id)
      self.items.delete_all
      self.relevant_sizes.each do |size|
        items_options =  {product_id: self.id, size_id: size.id}
        items_options[:quantity] = 10 if self.is_custom
        Item.create(items_options)
      end
    end
  end

  def get_size_chart
    category = self.categories.first

    case self.gender_type
    when Category::GENDER_TYPES[:men]; 		return !category.size_chart_man.url.include?('missing') ? category.size_chart_man    : Category.find(category.category_id).size_chart_man
    when Category::GENDER_TYPES[:women]; 	return !category.size_chart_woman.url.include?('missing') ? category.size_chart_woman  : Category.find(category.category_id).size_chart_woman
    when Category::GENDER_TYPES[:both]; 	return !category.size_chart_unisex.url.include?('missing') ? category.size_chart_unisex : Category.find(category.category_id).size_chart_unisex
    end
    
  end

  def default_values
    self.status = STATUS[:pending]
    self.return_days = 7
  end

  def current_price
    self.sale == true ? self.sale_price : self.price
  end

  def original_price_with_shipping
    price_val = self.price || 0
    shipping_cost_val = self.shipping_cost  || 0
    price_val + shipping_cost_val
  end

  def original_price_with_shipping_to_currency
    ApplicationController.helpers.number_to_currency original_price_with_shipping, separator: '.'
  end

  def sale_price_with_shipping
    shipping_cost_val = self.shipping_cost || 0
    sale_price_val = self.sale_price || 0
    shipping_cost_val+sale_price_val
  end

  def sale_price_with_shipping_to_currency
    # hacktacular -- MUCH better solution is to implement a handlebars helper that mirrors number_to_currency
    ApplicationController.helpers.number_to_currency sale_price_with_shipping, separator: '.'
  end

  def price_with_shipping
    shipping_cost_val = self.shipping_cost || 0
    current_price_val = self.current_price || 0
    current_price_val + shipping_cost_val
  end

  def price_with_shipping_to_currency
    # spechackular -- MUCH better solution is to implement a handlebars helper that mirrors number_to_currency
    ApplicationController.helpers.number_to_currency price_with_shipping, separator: '.'
  end


  # -- Category related functions -- 
  def primary_category
    self.primary_categories.first
  end

  def primary_category_id
    primary_category ? primary_category.id : nil 
  end

  def primary_category_id=(category_id)
    ProductCategory.delete_all({primary: true, product_id: id}) if id
    product_categories.build({category_id: category_id, primary: true}) unless category_id.blank?
  end

  def other_categories
    Category.joins(:product_categories).where{(product_categories.primary == false) & (product_categories.product_id == my{id})}
  end

  def other_category_ids
    other_categories ? other_categories.collect(&:id) : nil
  end

  def other_category_ids=(category_ids)
    ProductCategory.delete_all({primary: false, product_id: id}) if id
    category_ids.each {|category_id| product_categories.build({category_id: category_id, primary: false}) unless category_id.blank? }
  end
  # -- /end category related functions -- 

  def available_items
    self.items.where{quantity != nil}
  end

  #QUERY HELPERS => @products = Product.best_seller or  @products = Product.on_sale

  def self.approved_and_active
    where{(products.status == Product::STATUS[:approved]) & (products.active == true)}
  end

  #to use as a scope like Product.where(my conditions).available | this function adds a filter to return the products that have available items only
  def self.available
    joins(:items).where{(items.quantity != nil) & (items.quantity > 0)}.uniq
  end

  def self.designers_approve_and_active
    joins(:designer).where{(users.designer_active == true) & (users.designer_status == User::STATUS[:approved])}
  end

  #to use as a scope like Product.where(my conditions).best_seller | this function adds a filter to return the products that have available items only sorted by by best seller
  def self.best_seller
    select{['products.*', count(order_items.id).as('number_of_sells')]}.
      joins{items.outer.order_items.outer}.
      uniq.
      group{products.id}.
      order{['number_of_sells DESC', created_at.desc]}.
      approved_and_active.designers_approve_and_active.available
  end

  def self.with_celebrity_pictures
    joins(:celebrity_pictures).approved_and_active
  end

  def self.featured
    where(featured: true).approved_and_active
  end

  def self.on_sale
    where(sale: true).approved_and_active
  end

  #FILTER HELPERS => @products = Product.best_seller.filter_by_gender(:man) or  @products = Product.best_seller.filter_by_country(34)


  def self.filter_by_gender(gender)		
    where(gender_type: [gender, Category::GENDER_TYPES[:both]])
  end

  def self.filter_by_country(country_ids)
    joins(:designer).where{users.designer_country_id.in(country_ids)}
  end

  def self.filter_by_designer(designer_ids)
    joins(:designer).where{users.id.in(designer_ids)}
  end

  def self.filter_by_category(category_ids)
    joins{product_categories.category}.where{categories.id.in(category_ids)}
  end

  def self.filter_by_price_range(price_min, price_max)
    #where('"products".price >= ? AND "products".price <= ?', price_min, price_max)

    #TODO compare to price_with_sipping instead

    where{(price >= price_min.to_i) & (price <= price_max.to_i)}
  end

  def self.filter_by_price_ranges(prices)
    #where('"products".price >= ? AND "products".price <= ?', price_min, price_max)

    #TODO compare to price_with_sipping instead
    prices = prices.collect{|price| [price[:min], price[:max]]}
    where{prices.map{|price_min,price_max| ((price + shipping_cost) >= price_min.to_i) & ((price + shipping_cost) <= price_max.to_i) }.reduce(&:|)}
  end

  def self.filter_by_size(size_ids)
    joins{items.size}.where{(sizes.id.in(size_ids)) & (items.quantity > 0)}
  end

  # END HELPERS

  def converted_weight
    return (weight_type == 'lb') ? weight.kilograms.to.pounds.round(3) : weight 
  end

  def display_weight
    "#{converted_weight} #{weight_type}"
  end

  def should_validate_shipping_values?
    submitting_shipping_info == '1'
  end

  # image accessor for to_json
  def catalog_image
    self.primary_image.attachment.url(:catalog) unless self.primary_image.blank?
  end

  def one_size_is_only_size?
    self.relevant_sizes.where(us_value: "One Size") and self.relevant_sizes.count == 1
  end

  def no_of_items
    if self.is_custom
      return self.items.count
    end
    return 0
  end

  def designer_name
    self.designer.name
  end

  def categories_name
    self.categories.map(&:name).join(', ')
  end

end
