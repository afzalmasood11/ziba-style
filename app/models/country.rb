include ActionView::Helpers::SanitizeHelper

class Country < ActiveRecord::Base
	extend FriendlyId
  
  belongs_to :region
    
  NUMBER_OF_HISTORY_PICTURES = 3
    
  # Paperclip, see: https://github.com/thoughtbot/paperclip    
  has_attached_file :banner, :styles => { :small => "150x150>", :banner => "960x180#" },
    storage: :s3, bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_protocol: Rails.env.production? ? :https : :http,
    s3_credentials: ZIBAStyle::Application.config.s3_credentials, #need ImageMagick
    :path => "/#{Rails.env}/country/banner/:style/:filename"

  has_attached_file :flag, :styles => { :small => "16x11>" }, #need ImageMagick
    storage: :s3, bucket: ZIBAStyle::Application.config.s3_bucket, 
    s3_credentials: ZIBAStyle::Application.config.s3_credentials,
    s3_protocol: Rails.env.production? ? :https : :http,
    :path => "/#{Rails.env}/country/flag/:style/:filename"
    
  has_one  :homepage_image,   class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:homepage_picture] }, as: :entity, dependent: :destroy
  has_many :history_pictures, class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:country_history_picture] }, as: :entity
  has_many :users

  attr_accessible :banner, :flag, :name, :region_id, :homepage_image_attributes

  # same functionality exposed below with self.with_designers
  has_many :active_designers, class_name: :User, conditions: { role: User::ROLE[:designer], designer_active: true, designer_status: User::STATUS[:approved] }, foreign_key: :designer_country_id

  accepts_nested_attributes_for :history_pictures, :homepage_image

  friendly_id :name, use: [:slugged, :history]

  def myname
    self.class
  end

  def name
    ActionView::Helpers::SanitizeHelper::strip_tags read_attribute(:name)
  end

  def name_with_tags
    read_attribute(:name)
  end

  # Sphinx Index
  define_index do
    indexes :name, :sortable => true
  end

  def flag_url(size = :small)
    self.flag ? self.flag.url(size) : "/flags/small/missing.png"
  end
  
  def categories
    Category.where{users.designer_country_id == my{self.id}}.joins(:products => :designer)
  end

  def has_designers?
    not self.users.where(designer_active: 1).empty?
  end

  def self.with_designers(with_designers=false)
    countries = Country.where(id: User.designers({designer_active: true}).joins(:designer_country).joins(products: :items)
                  .where{products.status == my{Product::STATUS[:approved]}} # 
                  .group(:designer_country_id)
                  .select{users.designer_country_id}
                  .collect( &:designer_country_id ))
    countries = countries.includes(:active_designers) if with_designers

    # country names may countain markup (from mercury)
    # sorting by overridden name method ignores mercury markup when sorting
    countries.sort_by(&:name)
  end

  def self.shipping_destinations
    where{is_shipping_destination == true}
  end
end
