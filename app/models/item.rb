class Item < ActiveRecord::Base
	belongs_to :product
	belongs_to :size 
	has_many :order_items
	
	def in_stock?
	  !self.quantity.blank? and self.quantity > 0 and self.product.status == Product::STATUS[:approved] and self.product.active == true
	end
	
	def is_out_of_stock?
	  !self.quantity.blank? and self.quantity == 0
	end
	
end
