class Page < ActiveRecord::Base
	# Paperclip, see: https://github.com/thoughtbot/paperclip    
  has_attached_file :banner, styles: { :small => "330x450>" },
                           storage: :s3, 
                           s3_protocol: Rails.env.production? ? :https : :http,
                           bucket: ZIBAStyle::Application.config.s3_bucket, 
                           s3_credentials: ZIBAStyle::Application.config.s3_credentials,
                           path: "/#{Rails.env}/page/:style/:filename"

end
