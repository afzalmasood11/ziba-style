class ProductExcludedCountry < ActiveRecord::Base
	belongs_to :product
	belongs_to :excluded_country, class_name: :Country
end
