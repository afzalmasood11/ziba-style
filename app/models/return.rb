class Return < ActiveRecord::Base

  REASON = ["Doesn't fit.", "Don't like this item after seeing it.", "Other"]
  STATUS = {:pending => 0, :shipped => 1, :arrived => 2, :restocked => 3, :refunded => 4}

	belongs_to :order_item

	before_create :generate_return_code
  before_save :manage_status

  attr_accessor :status_changed

  validates_numericality_of :quantity, :only_integer =>true, :greater_than_or_equal_to =>1, :message => "quantity has to be at least 1"
  validates_presence_of :reason

	def generate_return_code
    code = Return::generate_return_code
    while Return.find_by_code(code) != nil
      code = Return::generate_return_code
    end
    self.code = code
    self.status = STATUS[:pending]
	end

	def self.generate_return_code
		o = [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten;
		(0..50).map{ o[rand(o.length)]  }.join;
	end

  def manage_status
    self.status = STATUS[:shipped] if self.status == STATUS[:pending] and !self.certified.blank?
  end

  def return_price
    (self.quantity * self.order_item.price) + (self.quantity * self.order_item.price)*self.order_item.tax_rate
  end

  def return_price_in_cents
    self.return_price*100
  end

end
