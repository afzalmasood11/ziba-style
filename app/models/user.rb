class User < ActiveRecord::Base
  extend FriendlyId

  before_create :default_values
  after_create :assign_invitation

  before_save :international_taxes

  serialize :cart
  serialize :email_preferences

  # CONSTANTS
  ROLE   = {customer: 1, designer: 2, admin: 100}
  STATUS = {pending_auth: 0, authorized: 1, pending_approval: 2, approved: 3, pending_promotion_to_designer: 4, approved_needs_password: 5 }
  GENDER = { female: 1, male: 2 }
  EMAIL_PREFERENCES = {newsletter: "Newsletter", designer_news: "Designer News"}

  # MODEL RELATIONS
  has_many :portfolio_docs,   class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:portfolio] },        as: :entity, dependent: :destroy
  has_one  :banner_pic,       class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:banner] },           as: :entity, dependent: :destroy
  has_one  :profile_pic,      class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:profile] },          as: :entity, dependent: :destroy
  has_one  :homepage_image,   class_name: :FileAttachment, conditions: { attachment_type: FileAttachment::TYPE[:homepage_picture] }, as: :entity, dependent: :destroy

  # has_one  :country
  belongs_to  :country
  has_many :credits
  has_many :unused_credits,    class_name: :Credit, conditions: { used: false }
  has_many :orders, foreign_key: :customer_id
  has_many :completed_orders, class_name: :Order, foreign_key: :customer_id, conditions: { status: Order::STATUS[:complete] }
  has_many :products, foreign_key: :designer_id, dependent: :destroy
  has_many :designer_orders, foreign_key: :designer_id
  has_many :designer_videos, foreign_key: :designer_id, conditions: { status: DesignerVideo::STATUS[:approved] }

  belongs_to :designer_country, class_name: :Country, foreign_key: :designer_country_id
  belongs_to :designer_state,   class_name: :State,   foreign_key: :designer_state_id

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :token_authenticatable, :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable

  # Setup accessible (or protected) attributes for your model
  # SECURITY: "ROLE" FIELD MUST NOT BE IN THOSE LISTS
  attr_accessible :email, :password, :password_confirmation, :remember_me, :designer_name, :designer_biography, :gender, :invitation_limit, :current_password
  attr_accessible :id, :fname, :lname, :address1, :address2, :zipcode, :city, :country_id, :phone, :designer_active, :state, :invitation_token, :terms_of_service
  attr_accessible :portfolio_docs, :portfolio_docs_attributes, :banner_pic, :banner_pic_attributes, :profile_pic, :profile_pic_attributes, :dob, :homepage_image_attributes
  attr_accessible :designer_city, :designer_country_id, :designer_paypal, :designer_portfolio_url,  :email_preferences, :designer_tax_rate, :designer_state_id, :designer_int_tax_rate, :designer_ships_from

  attr_accessor :invitation_token, :terms_and_conditions, :current_password, :designer_request_approval, :admin_update_status
  attr_accessor :ip_address, :express_token, :express_payer_id #PAYPAL fields
    
  accepts_nested_attributes_for :portfolio_docs, :banner_pic, :profile_pic, :homepage_image
  
  validates_presence_of  :email #, :city, :address1, :zipcode, :state, :phone - fields removed from customer sign up page
  # validates_presence_of :country_id, if: :is_customer?
  validates_presence_of :designer_name, :designer_city,  :designer_country_id, if: :is_designer? #TODO: designer has to enter designer_tax_rate

  validate :customer_can_register,      if: :is_customer?

  validate :validate_designer_country, if: :is_designer?
  validate :has_portfolio_url_or_attachment, if: :is_designer?

  validate :password_requirement, on: :create

  friendly_id :name, use: [:slugged, :history]

  def password_requirement
    reg = /^(?=.*\d)(?=.*([a-z]|[A-Z]))([\x20-\x7E]){6,40}$/
    unless reg.match(password)
      errors.add :password, " should contain at least one integer, at least one alphabet character (either in downcase or upcase) and should be minimum of 6 and maximum of 40 characters long."
    end
  end

  # validates_presence_of :banner_pic, :profile_pic, :designer_tax_rate, :on => :update, if: :is_designer? #TODO: only if updating from boutique form - otherwise admin cannot approve him
  validates_uniqueness_of :email
  # validates_acceptance_of :terms_of_service, on: :create, if: :is_customer? #no need to have a :terms_of_service checkbox on user registration pages

  # Sphinx Indexing (only index designer users)
  define_index do
    indexes :designer_name
    indexes :designer_biography
    
    where "role = '#{ROLE[:designer]}'"
    has created_at, updated_at
  end

  def customer_can_register
    if self.new_record?
      invitation = Invitation.find_by_token(self.invitation_token)
      unless invitation.blank?
        errors.add :invitation_token, "Invitation code already used" unless invitation.to_user_id.blank?
      else
        errors.add :invitation_token, "Invitation code not found"    
      end
    end
  end

  def validate_designer_country
    errors.add :designer_state_id, "State cannot be blank" if !self.new_record? and !self.admin_update_status and self.designer_country_id == ::USA_COUNTRY_ID and (self.designer_state_id == nil or self.designer_state_id == "" or self.designer_state_id == 0)
  end

  def international_taxes
    if self.designer_country_id == ::USA_COUNTRY_ID
      self.designer_int_tax_rate = 0 
    else
      self.designer_state_id = nil
    end
  end

  def name #used for home page only
    self.designer_name
  end

  def assign_invitation  
    if self.role == ROLE[:customer]
      used_invitation = Invitation.find_by_token(self.invitation_token)
      used_invitation.update_attributes(to_user_id: self.id) 
      Invitation.find_all_by_email(used_invitation.email).each do |invitation|
        invitation.update_attributes(to_user_id: self.id)
      end
    end
  end

  def confirm_invitation_email
    if self.role == ROLE[:customer]
      invitation = Invitation.find_by_token(self.invitation_token)
      if invitation.email == self.email
        self.confirmation_token = nil
        self.confirmed_at = DateTime.now
      end
    end
  end

  def confirmation_required?
    self.role == User::ROLE[:designer] and self.designer_status == User::STATUS[:pending_auth] ? false : true
  end
  
  def active_for_authentication?
    confirmed? || confirmation_period_valid?
  end

  # designers must provide either a link or file for their portfolio
  def has_portfolio_url_or_attachment 
    if self.designer_portfolio_url.present? or self.portfolio_docs.present? 
      return true
    else
      self.errors.add :base, 'Must provide a portfolio URL or attachment'
      return false
    end
  end

  def is_admin?
    role == ROLE[:admin]
  end  

  def is_designer?
    role == ROLE[:designer]
  end

  def is_customer?
    role == ROLE[:customer]
  end

  def male?
    self.gender == GENDER[:male]
  end

  def female?
    self.gender == GENDER[:female]
  end
  
  def default_values
    if self.role == ROLE[:designer]
      self.designer_status = STATUS[:pending_auth]
      self.designer_active = true
    end
    self.invitation_limit = 15
    self.email_preferences = {}
    self.designer_tax_rate = 0
    self.designer_int_tax_rate = 0
    self.cart = {}   
    self.role ||= ROLE[:customer] # just in case, default role == customer
  end

  def pending_checkout
    Order.find_by_customer_id_and_status(self.id, Order::STATUS[:pending_checkout])
  end

  def cancel_checkout
    orders = Order.find_all_by_customer_id_and_status(self.id, Order::STATUS[:pending_checkout])
    orders.each do |order|
      self.cancel_credits_for_checkout(order.id)
      Order.destroy_all({id:order.id})
    end
  end

  def cancel_credits_for_checkout(order_id)
    Credit.find_all_by_order_id(order_id).collect{|c| c.update_attributes(order_id:nil) }
  end

  def designer_orders
    DesignerOrder.joins(:order).where('orders.customer_id' => self.id)
  end

  def average_feedback_rating
#    feedback = Feedback.joins(:designer_order).where('"designer_orders".designer_id = ?', self.id).select("SUM(feedbacks.rating)/COUNT(feedbacks.rating) as average_rating").first
    Feedback.joins(:designer_order).where{designer_orders.designer_id == my{id}}.
      select{sum(feedbacks.rating).op('/',count(feedbacks.rating)).as('average_rating')}.
      first.average_rating
  end

  # get the categories that this designer's products fall into
  def designer_categories(session_gender)
    Category.joins{product_categories.product.items.size}.where{(products.active == true) & (products.designer_id == my{id}) & (items.quantity > 0) & (sizes.gender_type.in(my{ [session_gender, Category::GENDER_TYPES[:both]] }))}.uniq
  end

  def self.designers(options = {})
    options[:status] ||= STATUS[:approved] # so that User.designers will return approved ones by default
    designers = User.where(role: ROLE[:designer], designer_status: options[:status])
    designers = designers.where(designer_active: options[:designer_active]) if options[:designer_active]
    designers = designers.where(designer_country_id: options[:country_ids]) if options[:country_ids]
    designers = designers.where{designer_name =~ (options[:letter] + '%')} if options[:letter]
    designers = designers.joins{products.product_categories}.where{product_categories.category_id.in(options[:category_ids])} if options[:category_ids]
    designers = designers.joins{products.items}.where{(items.quantity > 0) & (products.status == Product::STATUS[:approved])} if options[:with_products]
    designers.uniq
    designers.joins(:designer_country).order{countries.name}
  end

  def count_cart
    self.cart = [] if self.cart == ""
    self.cart.collect{|key, value| value.to_i }.sum
  end

  def empty_cart
    self.cart = []
    self.save
  end

  def profile_pic_url(size = :small)
    profile_pic ? profile_pic.attachment.url(size) : "/assets/css/bio1.jpg"
  end

  def designer_flag_url(size = :small)
    return designer_country.flag(size) if designer_country
    nil
  end

  # bypasses Devise's requirement to re-enter current password to edit
  def update_with_password(params={}) 
    if params[:password].blank? 
      params.delete(:password) 
      params.delete(:password_confirmation) if params[:password_confirmation].blank? 
    end 
    update_attributes(params) 
  end  

  def paypal_purchase_options
    {
      :ip => self.ip_address,
      :token => self.express_token,
      :payer_id => self.express_payer_id
    }
  end

  # CREDIT INVITATION SENDER METHODS -- START

  def credit_invitation_sender
    invitation = Invitation.find_by_to_user_id(self.id)
    (invitation and invitation.from_user) ? invitation.from_user.get_credit_from_first_invited_user_purchase(self.id) : nil
  end

  def get_credit_from_first_invited_user_purchase(referring_user_id=nil)
    #attach users to credit
    credit = Credit.new
    credit.user_id = self.id
    credit.referring_user_id = referring_user_id if referring_user_id
    credit.save
    #increase number of invitation by 1
    self.invitation_limit = self.invitation_limit + 1
    self.save
    #return credit
    credit
  end

  # CREDIT INVITATION SENDER METHODS -- END

  def user_role
    ROLE.key(self.role).to_s    
  end

  def registration_date
    self.created_at.to_date
  end

end
