require 'test_helper'

class DesignerVideosControllerTest < ActionController::TestCase
  setup do
    @designer_video = designer_videos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:designer_videos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create designer_video" do
    assert_difference('DesignerVideo.count') do
      post :create, designer_video: @designer_video.attributes
    end

    assert_redirected_to designer_video_path(assigns(:designer_video))
  end

  test "should show designer_video" do
    get :show, id: @designer_video
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @designer_video
    assert_response :success
  end

  test "should update designer_video" do
    put :update, id: @designer_video, designer_video: @designer_video.attributes
    assert_redirected_to designer_video_path(assigns(:designer_video))
  end

  test "should destroy designer_video" do
    assert_difference('DesignerVideo.count', -1) do
      delete :destroy, id: @designer_video
    end

    assert_redirected_to designer_videos_path
  end
end
