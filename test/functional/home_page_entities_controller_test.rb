require 'test_helper'

class HomePageEntitiesControllerTest < ActionController::TestCase
  setup do
    @home_page_entity = home_page_entities(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:home_page_entities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create home_page_entity" do
    assert_difference('HomePageEntity.count') do
      post :create, home_page_entity: @home_page_entity.attributes
    end

    assert_redirected_to home_page_entity_path(assigns(:home_page_entity))
  end

  test "should show home_page_entity" do
    get :show, id: @home_page_entity
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @home_page_entity
    assert_response :success
  end

  test "should update home_page_entity" do
    put :update, id: @home_page_entity, home_page_entity: @home_page_entity.attributes
    assert_redirected_to home_page_entity_path(assigns(:home_page_entity))
  end

  test "should destroy home_page_entity" do
    assert_difference('HomePageEntity.count', -1) do
      delete :destroy, id: @home_page_entity
    end

    assert_redirected_to home_page_entities_path
  end
end
